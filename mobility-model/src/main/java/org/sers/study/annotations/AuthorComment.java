
/**
 *Jan 9, 2017
 *org.web.system.annotations
 *yodime-model
 *INSPIRON
 *
  *
 * 
 */
package org.sers.study.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface AuthorComment {
	public String author() default "solomon";

	public String role() default "";

	public String email() default "snsumba@gmail.com";

	public String description() default "This file is an original composition of the above @author";
}
