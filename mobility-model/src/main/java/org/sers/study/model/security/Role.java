package org.sers.study.model.security;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.BaseEntity;

@AuthorComment(role = "Represents the user roles in the system")
@Entity
@Table(name = "roles")
public class Role extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3702739490857371750L;

	/**
	 * default administrator role
	 */
	public static final String DEFAULT_ADMIN_ROLE = "ROLE_ADMINISTRATOR";
	public static final String NORMAL_USER_ROLE = "NORMAL_USER";
	private String name;
	private String description;
	private Set<Permission> permissions;
	private Set<User> users;

	/**
	 * default constructor
	 */
	public Role() {

	}

	/**
	 * constructor with initial specified value
	 * 
	 * @param name
	 * @param description
	 * @param permissions
	 * @param users
	 */
	public Role(String name, String description, Set<Permission> permissions, Set<User> users) {
		super();
		this.name = name;
		this.description = description;
		this.permissions = permissions;
		this.users = users;
	}

	/**
	 * @return the name
	 */
	@Column(name = "role_name", nullable = false)
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	@Column(name = "description", nullable = false)
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the permissions
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "role_permissions", joinColumns = @JoinColumn(name = "role_id") , inverseJoinColumns = @JoinColumn(name = "permission_id") )
	public Set<Permission> getPermissions() {
		return permissions;
	}

	/**
	 * @param permissions
	 *            the permissions to set
	 */
	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	/**
	 * adds a permission to the role
	 * 
	 * @param permission
	 */
	public void addPermission(Permission permission) {
		this.getPermissions().add(permission);
	}

	/**
	 * removes a given permission to the role
	 * 
	 * @param permission
	 */
	public void removePermission(Permission permission) {
		if (this.getPermissions().contains(permission)) {
			this.getPermissions().remove(permission);
		}
	}

	/**
	 * @return the users
	 */
	@ManyToMany(mappedBy = "roles")
	public Set<User> getUsers() {
		return users;
	}

	/**
	 * @param users
	 *            the users to set
	 */
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	/**
	 * adds a given user to the role
	 * 
	 * @param user
	 */
	public void addUser(User user) {
		if (users == null) {
			users = new HashSet<User>();
		}

		if (user != null) {
			if (!this.users.contains(user)) {
				users.add(user);
				user.addRole(this);
			}
		}
	}

	/**
	 * removes a given user from the role
	 * 
	 * @param user
	 */
	public void removeUser(User user) {
		if (user == null || users == null || users.size() == 0) {
			return;
		}

		if (this.getUsers().contains(user)) {
			getUsers().remove(user);
			user.removeRole(this);
		}
	}

	/**
	 * Checks if this role is the default administrator <code>Role</code> that
	 * ships with the system
	 * 
	 * @return True only and only if
	 *         <code>role.getName().equals(Role.DEFAULT_ADMIN_ROLE);</code>
	 */
	public boolean checkIfDefaultAdminRole() {
		return this.getName().equals(Role.DEFAULT_ADMIN_ROLE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (this.getName() != null && this.getName().trim().length() > 0)
			return this.getName();

		return super.toString();
	}

	/**
	 * checks whether this role has the given permission.
	 * 
	 * @param perm
	 * @return
	 */
	public boolean hasPermission(String perm) {
		if (this.permissions != null) {
			for (Permission permission : this.permissions) {
				if (permission.getName().equalsIgnoreCase(perm)) {
					return true;
				}
			}
		}
		return false;
	}
}
