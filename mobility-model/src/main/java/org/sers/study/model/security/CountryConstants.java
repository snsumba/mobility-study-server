package org.sers.study.model.security;

import org.sers.study.annotations.AuthorComment;


@AuthorComment(role="This class contains security related constants like default countries")
public final class CountryConstants {

	/**
	 * default constructor (Note: it is private because this class can't be
	 * instantiated)
	 */
	private CountryConstants() {

	}

	@CountryAnnotation(name = "Uganda", postalCode = 256)
	public static final String UGANDA = "Uganda";
	@CountryAnnotation(name = "Argentina", postalCode = 54)
	public static final String ARGENTINA = "Argentina";
	@CountryAnnotation(name = "Antigua", postalCode = 1268)
	public static final String ANTIGUA = "Antigua";
	@CountryAnnotation(name = "Antarctica", postalCode = 672)
	public static final String ANTARCTICA = "Antarctica";
	@CountryAnnotation(name = "Anguilla", postalCode = 1264)
	public static final String ANGUILLA = "Anguilla";
	@CountryAnnotation(name = "Angola", postalCode = 244)
	public static final String ANGOLA = "Angola";
	@CountryAnnotation(name = "Andorra", postalCode = 376)
	public static final String ANDORRA = "Andorra";
	@CountryAnnotation(name = "American Samoa", postalCode = 1684)
	public static final String AMERICAN_SAMOA = "American Samoa";
	@CountryAnnotation(name = "Algeria", postalCode = 213)
	public static final String ALGERIA = "Algeria";
	@CountryAnnotation(name = "Albania", postalCode = 355)
	public static final String ALBANIA = "Albania";
	@CountryAnnotation(name = "Afghanistan", postalCode = 93)
	public static final String AFGHANISTAN = "Afghanistan";
	@CountryAnnotation(name = "Armenia", postalCode = 374)
	public static final String ARMENIA = "Armenia";
	@CountryAnnotation(name = "Aruba", postalCode = 297)
	public static final String ARUBA = "Aruba";
	@CountryAnnotation(name = "Ascension", postalCode = 247)
	public static final String ASCENSION = "Ascension";
	@CountryAnnotation(name = "Australia", postalCode = 61)
	public static final String AUSTRALIA = "Australia";
	@CountryAnnotation(name = "Austria", postalCode = 43)
	public static final String AUSTRIA = "Austria";
	@CountryAnnotation(name = "Azerbaijan", postalCode = 994)
	public static final String AZERBAIJAN = "Azerbaijan";
	@CountryAnnotation(name = "Bahamas", postalCode = 1242)
	public static final String BAHAMAS = "Bahamas";
	@CountryAnnotation(name = "Bahrain", postalCode = 973)
	public static final String BAHRAIN = "Bahrain";
	@CountryAnnotation(name = "Bangladesh", postalCode = 880)
	public static final String BANGLADESH = "Bangladesh";
	@CountryAnnotation(name = "Barbados", postalCode = 1246)
	public static final String BARBADOS = "Barbados";
	@CountryAnnotation(name = "Belarus", postalCode = 375)
	public static final String BELARUS = "Belarus";
	@CountryAnnotation(name = "Belgium", postalCode = 32)
	public static final String BELGIUM = "Belgium";
	@CountryAnnotation(name = "Belize", postalCode = 501)
	public static final String BELIZE = "Belize";
	@CountryAnnotation(name = "Benin", postalCode = 229)
	public static final String BENIN = "Benin";
	@CountryAnnotation(name = "Bermuda", postalCode = 1441)
	public static final String BERMUDA = "Bermuda";
	@CountryAnnotation(name = "Bhutan", postalCode = 975)
	public static final String BHUTAN = "Bhutan";
	@CountryAnnotation(name = "Bolivia", postalCode = 591)
	public static final String BOLIVIA = "Bolivia";
	@CountryAnnotation(name = "Bosnia & Herzegovina", postalCode = 387)
	public static final String BOSNIA_AND_HERZEGOVINA = "Bosnia & Herzegovina";
	@CountryAnnotation(name = "Botswana", postalCode = 267)
	public static final String BOTSWANA = "Botswana";
	@CountryAnnotation(name = "Brazil", postalCode = 55)
	public static final String BRAZIL = "Brazil";
	@CountryAnnotation(name = "British Virgin Islands", postalCode = 1284)
	public static final String BRITISH_VIRGIN_ISLANDS = "British Virgin Islands";
	@CountryAnnotation(name = "Brunei Darussalam", postalCode = 673)
	public static final String BRUNEL_DARUSSALAM = "Brunei Darussalam";
	@CountryAnnotation(name = "Bulgaria", postalCode = 359)
	public static final String BULGARIA = "Bulgaria";
	@CountryAnnotation(name = "Burkina Faso", postalCode = 226)
	public static final String BURKINA_FASO = "Burkina Faso";
	@CountryAnnotation(name = "Burundi", postalCode = 257)
	public static final String BURUNDI = "Burundi";
	@CountryAnnotation(name = "Cambodia", postalCode = 855)
	public static final String CAMBODIA = "Cambodia";
	@CountryAnnotation(name = "Cameroon", postalCode = 237)
	public static final String CAMEROON = "Cameroon";
	@CountryAnnotation(name = "Canada", postalCode = 1)
	public static final String CANADA = "Canada";
	@CountryAnnotation(name = "Cape Verde Islands", postalCode = 238)
	public static final String CAPE_VERDE_ISLANDS = "Cape Verde Islands";
	@CountryAnnotation(name = "Cayman Islands", postalCode = 1345)
	public static final String CAYMAN_ISLANDS = "Cayman Islands";
	@CountryAnnotation(name = "Central African Republic", postalCode = 236)
	public static final String CENTRAL_AFRICAN_REPUBLIC = "Central African Republic";
	@CountryAnnotation(name = "Chad", postalCode = 235)
	public static final String CHAD = "Chad";
	@CountryAnnotation(name = "Chatham Island (New Zealand)", postalCode = 64)
	public static final String CHATHAM_ISLAND_NEW_ZEALAND = "Chatham Island (New Zealand)";
	@CountryAnnotation(name = "Chile", postalCode = 56)
	public static final String CHILE = "Chile";
	@CountryAnnotation(name = "China (PRC)", postalCode = 86)
	public static final String CHINA_PRC = "China (PRC)";
	@CountryAnnotation(name = "Christmas Island", postalCode = 618)
	public static final String CHRISTMAS_ISLAND = "Christmas Island";
	@CountryAnnotation(name = "Cocos-Keeling Islands", postalCode = 61)
	public static final String COCS_KEELING_ISLANDS = "Cocos-Keeling Islands";
	@CountryAnnotation(name = "Colombia", postalCode = 57)
	public static final String COLOMBIA = "Colombia";
	@CountryAnnotation(name = "Comoros", postalCode = 269)
	public static final String COMOROS = "Comoros";
	@CountryAnnotation(name = "Congo", postalCode = 242)
	public static final String CONGO = "Congo";
	@CountryAnnotation(name = "Dr Congo", postalCode = 243)
	public static final String DR_CONGO = "Dr Congo";
	@CountryAnnotation(name = "Cook Islands", postalCode = 682)
	public static final String COOK_ISLANDS = "Cook Islands";
	@CountryAnnotation(name = "Costa Rica", postalCode = 506)
	public static final String COSTA_RICA = "Costa Rica";
	@CountryAnnotation(name = "C�te d'Ivoire (Ivory Coast)", postalCode = 225)
	public static final String IVORY_COAST = "C�te d'Ivoire (Ivory Coast)";
	@CountryAnnotation(name = "Croatia", postalCode = 385)
	public static final String CROATIA = "Croatia";
	@CountryAnnotation(name = "Cuba", postalCode = 53)
	public static final String CUBA = "Cuba";
	@CountryAnnotation(name = "Cuba (Guantanamo Bay)", postalCode = 5399)
	public static final String CUBA_GUANTANAMO_BAY = "Cuba (Guantanamo Bay)";
	@CountryAnnotation(name = "Cura�ao", postalCode = 599)
	public static final String CURACAO = "Cura�ao";
	@CountryAnnotation(name = "Cyprus", postalCode = 357)
	public static final String CYPRUS = "Cyprus";
	@CountryAnnotation(name = "Czech Republic", postalCode = 420)
	public static final String CZECH_REPUBLIC = "Czech Republic";
	@CountryAnnotation(name = "Denmark", postalCode = 45)
	public static final String DENMARK = "Denmark";
	@CountryAnnotation(name = "Diego Garcia", postalCode = 246)
	public static final String DIEGO_GARCIA = "Diego Garcia";
	@CountryAnnotation(name = "Djibouti", postalCode = 253)
	public static final String DJIBOUTI = "Djibouti";
	@CountryAnnotation(name = "Dominica", postalCode = 1767)
	public static final String DOMINICA = "Dominica";
	@CountryAnnotation(name = "Dominican Republic", postalCode = 1809)
	public static final String DOMINICAN_REPUBLIC = "Dominican Republic";
	@CountryAnnotation(name = "East Timor", postalCode = 670)
	public static final String EAST_TIMOR = "East Timor";
	@CountryAnnotation(name = "Easter Island", postalCode = 56)
	public static final String EASTER_INSLAND = "Easter Island";
	@CountryAnnotation(name = "Ecuador", postalCode = 593)
	public static final String ECUADOR = "Ecuador";
	@CountryAnnotation(name = "Egypt", postalCode = 20)
	public static final String EGYPT = "Egypt";
	@CountryAnnotation(name = "El Salvador", postalCode = 503)
	public static final String EL_SALVADOR = "El Salvador";
	@CountryAnnotation(name = "Equatorial Guinea", postalCode = 240)
	public static final String EQUATORIAL_GUINEA = "Equatorial Guinea";
	@CountryAnnotation(name = "Eritrea", postalCode = 291)
	public static final String ERITREA = "Eritrea";
	@CountryAnnotation(name = "Estonia", postalCode = 372)
	public static final String ESTONIA = "Estonia";
	@CountryAnnotation(name = "Ethiopia", postalCode = 251)
	public static final String ETHIOPIA = "Ethiopia";
	@CountryAnnotation(name = "Falkland Islands (Malvinas)", postalCode = 500)
	public static final String MALVINAS = "Falkland Islands (Malvinas)";
	@CountryAnnotation(name = "Faroe Islands", postalCode = 298)
	public static final String FAROE_ISLANDS = "Faroe Islands";
	@CountryAnnotation(name = "Fiji Islands", postalCode = 679)
	public static final String FIJI_ISLANDS = "Fiji Islands";
	@CountryAnnotation(name = "Finland", postalCode = 358)
	public static final String FINLAND = "Finland";
	@CountryAnnotation(name = "France", postalCode = 33)
	public static final String FRANCE = "France";
	@CountryAnnotation(name = "French Antilles", postalCode = 596)
	public static final String FRENCH_ENTILLES = "French Antilles";
	@CountryAnnotation(name = "French Guiana", postalCode = 594)
	public static final String FRENCH_GUIANA = "French Guiana";
	@CountryAnnotation(name = "French Polynesia", postalCode = 689)
	public static final String FRENCH_POLYNESIA = "French Polynesia";
	@CountryAnnotation(name = "Gabonese Republic", postalCode = 241)
	public static final String GABONESE_REPUBLIC = "Gabonese Republic";
	@CountryAnnotation(name = "Gambia", postalCode = 220)
	public static final String GAMBIA = "Gambia";
	@CountryAnnotation(name = "Georgia", postalCode = 995)
	public static final String GEORGIA = "Georgia";
	@CountryAnnotation(name = "Germany", postalCode = 49)
	public static final String GERMANY = "Germany";
	@CountryAnnotation(name = "Ghana", postalCode = 233)
	public static final String GHANA = "Ghana";
	@CountryAnnotation(name = "Gibraltar", postalCode = 350)
	public static final String GIBRALTAR = "Gibraltar";
	@CountryAnnotation(name = "Greece", postalCode = 30)
	public static final String GREECE = "Greece";
	@CountryAnnotation(name = "Greenland", postalCode = 299)
	public static final String GREENLAND = "Greenland";
	@CountryAnnotation(name = "Grenada", postalCode = 1473)
	public static final String GRENADA = "Grenada";
	@CountryAnnotation(name = "Guadeloupe", postalCode = 590)
	public static final String GUADELOUPE = "Guadeloupe";
	@CountryAnnotation(name = "Guam", postalCode = 1671)
	public static final String GUAM = "Guam";
	@CountryAnnotation(name = "Guantanamo Bay", postalCode = 5399)
	public static final String GUANTANAMO_BAY = "Guantanamo Bay";
	@CountryAnnotation(name = "Guatemala", postalCode = 502)
	public static final String GUATAMALA = "Guatemala";
	@CountryAnnotation(name = "Guinea-Bissau", postalCode = 245)
	public static final String GUINEA_BISSAU = "Guinea-Bissau";
	@CountryAnnotation(name = "Guinea", postalCode = 224)
	public static final String GUINEA = "Guinea";
	@CountryAnnotation(name = "Guyana", postalCode = 592)
	public static final String GUYANA = "Guyana";
	@CountryAnnotation(name = "Haiti", postalCode = 509)
	public static final String HAITI = "Haiti";
	@CountryAnnotation(name = "Honduras", postalCode = 504)
	public static final String HONDURAS = "Honduras";
	@CountryAnnotation(name = "Hong Kong", postalCode = 852)
	public static final String HONG_KONG = "Hong Kong";
	@CountryAnnotation(name = "Hungary", postalCode = 36)
	public static final String HUNGARY = "Hungary";
	@CountryAnnotation(name = "Iceland", postalCode = 354)
	public static final String ICELAND = "Iceland";
	@CountryAnnotation(name = "India", postalCode = 91)
	public static final String INDIA = "India";
	@CountryAnnotation(name = "Indonesia", postalCode = 62)
	public static final String INDONESIA = "Indonesia";
	
	
	@CountryAnnotation(name = "Iran", postalCode = 98)
	public static final String IRAN = "Iran";
	@CountryAnnotation(name = "Iraq", postalCode = 964)
	public static final String IRAG = "Iraq";
	@CountryAnnotation(name = "Ireland", postalCode = 353)
	public static final String IRELAND = "Ireland";
	@CountryAnnotation(name = "Israel", postalCode = 972)
	public static final String ISRAEL = "Israel";
	@CountryAnnotation(name = "Italy", postalCode = 39)
	public static final String ITALY = "Italy";
	@CountryAnnotation(name = "Jamaica", postalCode = 1876)
	public static final String JAMAICA = "Jamaica";
	@CountryAnnotation(name = "Japan", postalCode = 81)
	public static final String JAPAN = "Japan";
	@CountryAnnotation(name = "Jordan", postalCode = 962)
	public static final String JORDAN = "Jordan";
	@CountryAnnotation(name = "Kazakhstan", postalCode = 7)
	public static final String KAZAKHSTAN = "Kazakhstan";
	@CountryAnnotation(name = "Kenya", postalCode = 254)
	public static final String KENYA = "Kenya";
	@CountryAnnotation(name = "Kiribati", postalCode = 686)
	public static final String KIRIBATI = "Kiribati";
	@CountryAnnotation(name = "Korea (North)", postalCode = 850)
	public static final String NORTH_KOREA = "Korea (North)";
	@CountryAnnotation(name = "Korea (South)", postalCode = 82)
	public static final String SOUTH_KOREA = "Korea (South)";
	@CountryAnnotation(name = "Kuwait", postalCode = 965)
	public static final String KUWAIT = "Kuwait";
	@CountryAnnotation(name = "Kyrgyz Republic", postalCode = 996)
	public static final String KYRGYZ_REPUBLIC = "Kyrgyz Republic";
	@CountryAnnotation(name = "Laos", postalCode = 856)
	public static final String LAOS = "Laos";
	@CountryAnnotation(name = "Latvia", postalCode = 371)
	public static final String LATVIA = "Latvia";
	@CountryAnnotation(name = "Lebanon", postalCode = 961)
	public static final String LEBANON = "Lebanon";
	@CountryAnnotation(name = "Lesotho", postalCode = 266)
	public static final String LESOTHO = "Lesotho";
	@CountryAnnotation(name = "Liberia", postalCode = 231)
	public static final String LIBERIA = "Liberia";
	@CountryAnnotation(name = "Libya", postalCode = 218)
	public static final String LIBYA = "Libya";
	@CountryAnnotation(name = "Liechtenstein", postalCode = 423)
	public static final String LIECHTENSTEIN = "Liechtenstein";
	@CountryAnnotation(name = "Lithuania", postalCode = 370)
	public static final String LITHUANIA = "UgaLithuanianda";
	@CountryAnnotation(name = "Luxembourg", postalCode = 352)
	public static final String LUTEMBOURG = "Luxembourg";
	@CountryAnnotation(name = "Macao", postalCode = 853)
	public static final String MACAO = "Macao";
	@CountryAnnotation(name = "Macedonia (Former Yugoslav Rep)", postalCode = 389)
	public static final String MACEDONIA = "Macedonia (Former Yugoslav Rep)";
	@CountryAnnotation(name = "Madagascar", postalCode = 261)
	public static final String MADAGASCAR = "Madagascar";
	@CountryAnnotation(name = "Malawi", postalCode = 265)
	public static final String MALAWI = "Malawi";
	@CountryAnnotation(name = "Malaysia", postalCode = 60)
	public static final String MALAYSIA = "Malaysia";
	@CountryAnnotation(name = "Maldives", postalCode = 960)
	public static final String MALDIVES = "Maldives";
	@CountryAnnotation(name = "Mali Republic", postalCode = 223)
	public static final String MALI = "Mali Republic";
	@CountryAnnotation(name = "Malta", postalCode = 356)
	public static final String MALTA = "Malta";
	@CountryAnnotation(name = "Marshall Islands", postalCode = 692)
	public static final String MARSHALL_ISLANDS = "Marshall Islands";
	@CountryAnnotation(name = "Martinique", postalCode = 596)
	public static final String MARTINIQUE = "Martinique";
	@CountryAnnotation(name = "Mauritania", postalCode = 222)
	public static final String MAURITANIA = "Mauritania";
	@CountryAnnotation(name = "Mauritius", postalCode = 230)
	public static final String MAURITIUS = "Mauritius";
	@CountryAnnotation(name = "Mayotte Island", postalCode = 269)
	public static final String MAYOTTE_ISLAND = "Mayotte Island";
	@CountryAnnotation(name = "Mexico", postalCode = 52)
	public static final String MEXICO = "Mexico";
	@CountryAnnotation(name = "Federal States of Micronesia", postalCode = 691)
	public static final String MICRONESIA = "Federal States of Micronesia";
	@CountryAnnotation(name = "Midway Island", postalCode = 1808)
	public static final String MIDWAY_ISLAND = "Midway Island";
	@CountryAnnotation(name = "Moldova", postalCode = 373)
	public static final String MOLDOVA = "Moldova";
	@CountryAnnotation(name = "Monaco", postalCode = 976)
	public static final String MONACO = "Monaco";
	@CountryAnnotation(name = "Mongolia", postalCode = 976)
	public static final String MONGOLIA = "Mongolia";
	@CountryAnnotation(name = "Montenegro", postalCode = 382)
	public static final String MONTENEGRO = "Montenegro";
	@CountryAnnotation(name = "Montserrat", postalCode = 1664)
	public static final String MONTSERRAT = "Montserrat";
	@CountryAnnotation(name = "Morocco", postalCode = 212)
	public static final String MOROCCO = "Morocco";
	@CountryAnnotation(name = "Mozambique", postalCode = 258)
	public static final String MOZAMBIQUE = "Mozambique";
	@CountryAnnotation(name = "Myanmar", postalCode = 95)
	public static final String MYANMAR = "Myanmar";
	@CountryAnnotation(name = "Namibia", postalCode = 264)
	public static final String NAMIBIA = "Namibia";
	@CountryAnnotation(name = "Nauru", postalCode = 674)
	public static final String NAURU = "Nauru";
	@CountryAnnotation(name = "Nepal", postalCode = 977)
	public static final String NEPAL = "Nepal";
	@CountryAnnotation(name = "Netherlands", postalCode = 31)
	public static final String NETHERLANDS = "Netherlands";
	@CountryAnnotation(name = "Netherlands Antilles", postalCode = 599)
	public static final String NETHERLANDS_ANTILLES = "Netherlands Antilles";
	@CountryAnnotation(name = "Nevis", postalCode = 1869)
	public static final String NEVIS = "Nevis";
	@CountryAnnotation(name = "New Caledonia", postalCode = 687)
	public static final String NEW_CALEDONIA = "New Caledonia";
	@CountryAnnotation(name = "New Zealand", postalCode = 64)
	public static final String NEW_ZEALAND = "New Zealand";
	@CountryAnnotation(name = "Nicaragua", postalCode = 505)
	public static final String NICARAGUA = "Nicaragua";
	@CountryAnnotation(name = "Niger", postalCode = 227)
	public static final String NIGER = "Niger";
	@CountryAnnotation(name = "Nigeria", postalCode = 234)
	public static final String NIGERIA = "Nigeria";
	@CountryAnnotation(name = "Niue", postalCode = 683)
	public static final String NIUE = "Niue";
	@CountryAnnotation(name = "Norfolk Island", postalCode = 672)
	public static final String NORFOLK_ISLAND = "Norfolk Island";
//	@CountryAnnotation(name = "Northern Marianas Islands (Saipan, Rota, & Tinian)", postalCode = 1670)
//	public static final String NORTHERN_MARIANAS = "Northern Marianas Islands (Saipan, Rota, & Tinian)";
	@CountryAnnotation(name = "Norway", postalCode = 47)
	public static final String NORWAY = "Norway";
	@CountryAnnotation(name = "Oman", postalCode = 968)
	public static final String OMAN = "Oman";
	@CountryAnnotation(name = "Pakistan", postalCode = 92)
	public static final String PAKISTAN = "Pakistan";
	@CountryAnnotation(name = "Palau", postalCode = 680)
	public static final String PALAU = "Palau";
	@CountryAnnotation(name = "Palestinian Settlements", postalCode = 970)
	public static final String PALESTINIAN_SETTLEMENTS = "Palestinian Settlements";
	@CountryAnnotation(name = "Panama", postalCode = 507)
	public static final String PANAMA = "Panama";
	@CountryAnnotation(name = "Papua New Guinea", postalCode = 675)
	public static final String PAPUA_NEW_GUINEA = "Papua New Guinea";
	@CountryAnnotation(name = "Paraguay", postalCode = 595)
	public static final String PARAGUAY = "Paraguay";
	@CountryAnnotation(name = "Peru", postalCode = 51)
	public static final String PERU = "Peru";
	@CountryAnnotation(name = "Philippines", postalCode = 63)
	public static final String PHILIPPINES = "Philippines";
	@CountryAnnotation(name = "Poland", postalCode = 48)
	public static final String POLAND = "Poland";
	@CountryAnnotation(name = "Portugal", postalCode = 351)
	public static final String PORTUGAL = "Portugal";
	@CountryAnnotation(name = "Puerto Rico", postalCode = 1939)
	public static final String PUERTO_RICO = "Puerto Rico";
	@CountryAnnotation(name = "Qatar", postalCode = 974)
	public static final String QATAR = "Qatar";
	@CountryAnnotation(name = "R�union Island", postalCode = 262)
	public static final String REUNION_ISLAND = "R�union Island";
	@CountryAnnotation(name = "Romania", postalCode = 40)
	public static final String ROMANIA = "Romania";
	@CountryAnnotation(name = "Russia", postalCode = 7)
	public static final String RUSSIA = "Russia";
	@CountryAnnotation(name = "Rwandese Republic", postalCode = 250)
	public static final String RWANDA = "Rwandese Republic";
	@CountryAnnotation(name = "St. Helena", postalCode = 290)
	public static final String ST_HELENA = "St. Helena";
	@CountryAnnotation(name = "St. Kitts/Nevis", postalCode = 1869)
	public static final String ST_KITTS_NEVIS = "St. Kitts/Nevis";
	@CountryAnnotation(name = "St. Lucia", postalCode = 1758)
	public static final String ST_LUCIA = "St. Lucia";
	@CountryAnnotation(name = "St. Pierre & Miquelon", postalCode = 508)
	public static final String ST_PIERRE = "St. Pierre & Miquelon";
	@CountryAnnotation(name = "St. Vincent & Grenadines", postalCode = 1784)
	public static final String ST_VINCENT = "St. Vincent & Grenadines";
	@CountryAnnotation(name = "Samoa", postalCode = 685)
	public static final String SAMOA = "Samoa";
	@CountryAnnotation(name = "San Marino", postalCode = 378)
	public static final String SAN_MARINO = "San Marino";
	@CountryAnnotation(name = "S�o Tom� and Principe", postalCode = 239)
	public static final String SAO_TOME = "S�o Tom� and Principe";
	@CountryAnnotation(name = "Saudi Arabia", postalCode = 966)
	public static final String SAUDI_ARABIA = "Saudi Arabia";
	@CountryAnnotation(name = "Senegal", postalCode = 221)
	public static final String SENEGAL = "Senegal";
	@CountryAnnotation(name = "Serbia", postalCode = 381)
	public static final String SERBIA = "Serbia";
	@CountryAnnotation(name = "Seychelles Republic", postalCode = 248)
	public static final String SEYCHELLES = "Seychelles Republic";
	@CountryAnnotation(name = "Sierra Leone", postalCode = 232)
	public static final String SIERRA_LEONE = "Sierra Leone";
	@CountryAnnotation(name = "Singapore", postalCode = 65)
	public static final String SINGAPORE = "Singapore";
	@CountryAnnotation(name = "Slovak Republic", postalCode = 421)
	public static final String SLOVAK_REPUBLIC = "Slovak Republic";
	@CountryAnnotation(name = "Slovenia", postalCode = 386)
	public static final String SLOVENIA = "Slovenia";
	@CountryAnnotation(name = "Solomon Islands", postalCode = 677)
	public static final String SOLOMON_ISLANDS = "Solomon Islands";
	@CountryAnnotation(name = "Somali Democratic Republic", postalCode = 252)
	public static final String SOMALI_DR = "Somali Democratic Republic";
	@CountryAnnotation(name = "South Africa", postalCode = 27)
	public static final String SOUTH_AFRICA = "South Africa";
	@CountryAnnotation(name = "Spain", postalCode = 34)
	public static final String SPAIN = "Spain";
	@CountryAnnotation(name = "Sri Lanka", postalCode = 94)
	public static final String SRI_LANKA = "Sri Lanka";
	@CountryAnnotation(name = "Sudan", postalCode = 249)
	public static final String SUDAN = "Sudan";
	@CountryAnnotation(name = "Suriname", postalCode = 597)
	public static final String SURINAME = "Suriname";
	@CountryAnnotation(name = "Swaziland", postalCode = 268)
	public static final String SWAZILAND = "Swaziland";
	@CountryAnnotation(name = "Sweden", postalCode = 46)
	public static final String SWEDEN = "Sweden";
	@CountryAnnotation(name = "Switzerland", postalCode = 41)
	public static final String SWITZERLAND = "Switzerland";
	@CountryAnnotation(name = "Syria", postalCode = 963)
	public static final String SYRIA = "Syria";
	@CountryAnnotation(name = "Taiwan", postalCode = 886)
	public static final String TAIWAN = "Taiwan";
	@CountryAnnotation(name = "Tajikistan", postalCode = 992)
	public static final String TAJIKISTAN = "Tajikistan";
	@CountryAnnotation(name = "Tanzania", postalCode = 255)
	public static final String TANZANIA = "Tanzania";
	@CountryAnnotation(name = "Thailand", postalCode = 66)
	public static final String THAILAND = "Thailand";
	@CountryAnnotation(name = "Timor Leste", postalCode = 670)
	public static final String TIMOR_LESTE = "Timor Leste";
	@CountryAnnotation(name = "Togolese Republic", postalCode = 228)
	public static final String TOGOLOESE_REPUBLIC = "Togolese Republic";
	@CountryAnnotation(name = "Tokelau", postalCode = 690)
	public static final String TOKELAU = "Tokelau";
	@CountryAnnotation(name = "Tonga Islands", postalCode = 676)
	public static final String TONGA_ISLANDS = "Tonga Islands";
	@CountryAnnotation(name = "Trinidad & Tobago", postalCode = 1868)
	public static final String TRINIDAD_AND_TOBAGO = "Trinidad & Tobago";
	@CountryAnnotation(name = "Tunisia", postalCode = 216)
	public static final String TUNISIA = "Tunisia";
	@CountryAnnotation(name = "Turkey", postalCode = 90)
	public static final String TURKEY = "Turkey";
	@CountryAnnotation(name = "Turkmenistan", postalCode = 993)
	public static final String TURKMENISTAN = "Turkmenistan";
	@CountryAnnotation(name = "Turks and Caicos Islands", postalCode = 1649)
	public static final String TURKS_AND_CAICOS_ISLANDS = "Turks and Caicos Islands";
	@CountryAnnotation(name = "Tuvalu", postalCode = 688)
	public static final String TUVALU = "Tuvalu";
	@CountryAnnotation(name = "Ukraine", postalCode = 380)
	public static final String UKRAINE = "Ukraine";
	@CountryAnnotation(name = "United Arab Emirates", postalCode = 971)
	public static final String UNITED_ARAB_EMIRATES = "United Arab Emirates";
	@CountryAnnotation(name = "United Kingdom", postalCode = 44)
	public static final String UNITED_KINGDOM = "United Kingdom";
	@CountryAnnotation(name = "United States of America", postalCode = 1)
	public static final String USA = "United States of America";
	@CountryAnnotation(name = "US Virgin Islands", postalCode = 1340)
	public static final String US_VIRGIN_ISLANDS = "US Virgin Islands";
	@CountryAnnotation(name = "Uruguay", postalCode = 598)
	public static final String URUGUAY = "Uruguay";
	@CountryAnnotation(name = "Uzbekistan", postalCode = 998)
	public static final String UZBEKISTAN = "Uzbekistan";
	@CountryAnnotation(name = "Vanuatu", postalCode = 678)
	public static final String VANUATU = "Vanuatu";
	@CountryAnnotation(name = "Vatican City", postalCode = 39)
	public static final String VATICAN_CITY = "Vatican City";
	@CountryAnnotation(name = "Venezuela", postalCode = 58)
	public static final String VENEZUALA = "Venezuela";
	@CountryAnnotation(name = "Vietnam", postalCode = 84)
	public static final String VIETNAM = "Vietnam";
	@CountryAnnotation(name = "Wake Island", postalCode = 808)
	public static final String WAKE_ISLAND = "Wake Island";
	@CountryAnnotation(name = "Wallis and Futuna Islands", postalCode = 681)
	public static final String WALLIS_AND_FUTUNA_ISLANDS = "Wallis and Futuna Islands";
	@CountryAnnotation(name = "Yemen", postalCode = 967)
	public static final String YEMEN = "Yemen";
	@CountryAnnotation(name = "Zambia", postalCode = 260)
	public static final String ZAMBIA = "Zambia";
	@CountryAnnotation(name = "Zanzibar", postalCode = 255)
	public static final String ZANZIBAR = "Zanzibar";
	@CountryAnnotation(name = "Zimbabwe", postalCode = 263)
	public static final String ZIMBABWE = "Zimbabwe";
	
}