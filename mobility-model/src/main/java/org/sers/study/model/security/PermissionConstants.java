package org.sers.study.model.security;

import org.sers.study.annotations.AuthorComment;

@AuthorComment(role = "This class contains security related constants like default permissions")
public final class PermissionConstants {

	/**
	 * default constructor (Note: it is private because this class can't be
	 * instantiated)
	 */
	private PermissionConstants() {

	}

	@PermissionAnnotation(description = "access web resources if api is used in a web application", name = "perm_web_access")
	public static final String PERM_WEB_ACCESS = "perm_web_access";

	@PermissionAnnotation(description = "become a system adminitrator", name = "Administrator")
	public static final String PERM_ADMINISTRATOR = "Administrator";

}