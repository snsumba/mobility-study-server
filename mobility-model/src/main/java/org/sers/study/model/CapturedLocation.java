package org.sers.study.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.MethodType;
import org.sers.study.model.security.User;

@AuthorComment
@Entity
@Table(name = "captured_location")
public class CapturedLocation extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appRegistrationDate;
	private String placeName;
	private String longitude;
	private String latitude;
	private String accuracy;
	private String recordId;
	
	private MethodType methodType;
	private User user;

	/**
	 * 
	 */
	public CapturedLocation() {
		super();
	}

	/**
	 * @param appRegistrationDate
	 * @param placeName
	 * @param longitude
	 * @param latitude
	 * @param accuracy
	 * @param recordId
	 * @param methodType
	 * @param user
	 */
	public CapturedLocation(String appRegistrationDate, String placeName, String longitude, String latitude,
			String accuracy, String recordId, MethodType methodType, User user) {
		super();
		this.appRegistrationDate = appRegistrationDate;
		this.placeName = placeName;
		this.longitude = longitude;
		this.latitude = latitude;
		this.accuracy = accuracy;
		this.recordId = recordId;
		this.methodType = methodType;
		this.user = user;
	}

	/**
	 * This method is used to
	 * 
	 * @return the appRegistrationDate
	 */
	@Column(name = "app_registration_date")
	public String getAppRegistrationDate() {
		return appRegistrationDate;
	}

	/**
	 * This method is used to
	 * 
	 * @return the placeName
	 */
	@Column(name = "place_name", nullable = true)
	public String getPlaceName() {
		return placeName;
	}

	/**
	 * This method is used to
	 * 
	 * @return the longitude
	 */
	@Column(name = "longitude", nullable = true)
	public String getLongitude() {
		return longitude;
	}

	/**
	 * This method is used to
	 * 
	 * @return the latitude
	 */
	@Column(name = "latitude", nullable = true)
	public String getLatitude() {
		return latitude;
	}

	/**
	 * This method is used to
	 * 
	 * @return the accuracy
	 */
	@Column(name = "accuracy", nullable = true)
	public String getAccuracy() {
		return accuracy;
	}

	/**
	 * This method is used to
	 * 
	 * @return the recordId
	 */
	@Column(name = "record_id", nullable = true)
	public String getRecordId() {
		return recordId;
	}

	/**
	 * @param recordId
	 *            the recordId to set
	 */
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	/**
	 * This method is used to
	 * 
	 * @return the methodType
	 */
	@Column(name = "method_type", nullable = true)
	@Enumerated(EnumType.ORDINAL)
	public MethodType getMethodType() {
		return methodType;
	}

	/**
	 * This method is used to
	 * 
	 * @return the user
	 */
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return user;
	}

	/**
	 * @param placeName
	 *            the placeName to set
	 */
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	/**
	 * @param appRegistrationDate
	 *            the appRegistrationDate to set
	 */
	public void setAppRegistrationDate(String appRegistrationDate) {
		this.appRegistrationDate = appRegistrationDate;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @param accuracy
	 *            the accuracy to set
	 */
	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

	/**
	 * @param methodType
	 *            the methodType to set
	 */
	public void setMethodType(MethodType methodType) {
		this.methodType = methodType;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return this.user.toString() + " (" + latitude + "," + longitude + ")";
	}

	@Override
	public boolean equals(Object object) {
		return object instanceof CapturedLocation && (super.getId() != null)
				? super.getId().equals(((CapturedLocation) object).getId()) : (object == this);
	}

	@Override
	public int hashCode() {
		return super.getId() != null ? this.getClass().hashCode() + super.getId().hashCode() : super.hashCode();
	}

}
