package org.sers.study.model.security;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.sers.study.annotations.AuthorComment;


@AuthorComment(role="This is used for annotating country constants in the system")
@Retention(RetentionPolicy.RUNTIME)
public @interface CountryAnnotation {

	/**
	 * the name of a country
	 * 
	 * @return
	 */
	String name();

	/**
	 * the postal code of a country
	 * 
	 * @return
	 */
	int postalCode();
}