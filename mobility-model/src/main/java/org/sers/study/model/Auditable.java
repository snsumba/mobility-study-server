package org.sers.study.model;

import java.io.Serializable;
import java.util.Date;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.security.User;

@AuthorComment
public interface Auditable extends Serializable {

	/**
	 * gets the user who created the entity
	 * 
	 * @return
	 */
	User getCreatedBy();

	/**
	 * sets the user who created the entity
	 * 
	 * @param user
	 */
	void setCreatedBy(User user);

	/**
	 * gets the date the entity was created.
	 * 
	 * @return
	 */
	Date getDateCreated();

	/**
	 * sets the date the entity was created.
	 * 
	 * @param dateCreated
	 */
	void setDateCreated(Date dateCreated);

	/**
	 * gets the user who last changed the entity
	 * 
	 * @return
	 */
	User getChangedBy();

	/**
	 * sets the user who last changed the entity.
	 * 
	 * @param changedBy
	 */
	void setChangedBy(User changedBy);

	/**
	 * gets the date the entity was last changed.
	 * 
	 * @return
	 */
	Date getDateChanged();

	/**
	 * sets the date the entity was last changed.
	 * 
	 * @param dateChanged
	 */
	void setDateChanged(Date dateChanged);
}
