package org.sers.study.model.security;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.sers.study.annotations.AuthorComment;


@AuthorComment(role="This is used for annotating Permission constants in the system")
@Retention(RetentionPolicy.RUNTIME)
public @interface PermissionAnnotation {

	/**
	 * the description of the permission
	 * 
	 * @return
	 */
	String description();

	/**
	 * the name of the permission
	 * 
	 * @return
	 */
	String name();
}