/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sers.study.model.constants;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
public enum MethodType {
	GPS_DETERMINED("GPS Determined"), 
	MANUAL_INPUT("Manual Input"),
	UNKNOWN("Unknown");

	private String name;

	MethodType(String name) {
		this.name = name;
	}

	/**
	 * This method gets the name or title of the enumerator element
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
