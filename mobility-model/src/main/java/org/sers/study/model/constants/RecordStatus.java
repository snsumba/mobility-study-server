/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sers.study.model.constants;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
public enum RecordStatus {
	/*
	 * the status for an active row/record
	 */

	ACTIVE("Active"),
	/*
	 * the status for a deleted row/record
	 */
	DELETED("Deleted"),
	/**
	 * Status for a de activated row/record
	 */
	DE_ACTIVATED("De_Activate"),
	/**
	 * Status for a a signed out row/record
	 */
	SIGN_OUT("Signed_Out"),
	/**
	 * Status for a signed in row/record
	 */
	SIGN_IN("Signed_In");

	private String name;

	RecordStatus(String name) {
		this.name = name;
	}

	/**
	 * This method gets the name or title of the enumerator element
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
