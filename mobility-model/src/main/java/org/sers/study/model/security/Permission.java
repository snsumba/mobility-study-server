package org.sers.study.model.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.BaseEntity;


@AuthorComment(role="represents a permission in the system")
@Entity
@Table(name = "permissions")
public class Permission extends BaseEntity {

	/**
     * 
     */
	private static final long serialVersionUID = -6233936258089900467L;
	/**
	 * The name of a permission
	 */
	private String name;
	/**
	 * Text describing the permission
	 */
	private String description;

	/**
	 * default constructor
	 */
	public Permission() {
		super();
	}

	/**
	 * constructor with initial specified values
	 * 
	 * @param name
	 * @param description
	 */
	public Permission(String description) {
		super();
		this.description = description;
	}

	/**
	 * constructor that sets both name and description.
	 * 
	 * @param name
	 * @param description
	 */
	public Permission(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	/**
	 * @return the name
	 */
	@Column(name = "permission_name")
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		if (this.name != null && !this.name.isEmpty())
			return this.name;

		return super.toString();
	}

	/**
	 * 
	 * @return the name of Object that the permission is associated with
	 */
	public static String getObjectNameFromPermisionName(Permission p) {
		String subString = "";
		if (p.getName().indexOf('_') != -1)
			subString = p.getName().substring(p.getName().indexOf('_') + 1);
		else
			return p.getName();

		if (subString.indexOf('_') != -1)
			return subString.substring(subString.indexOf('_') + 1);
		else
			return subString;
	}
}
