package org.sers.study.model.constants;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
public enum BooleanEnum {
	True(true, "True"), False(false, "False");

	boolean booleanValue;
	String displayText;

	BooleanEnum(boolean boolValue, String dispText) {
		this.booleanValue = boolValue;
		this.displayText = dispText;
	}

	public final boolean toBoolean() {
		return this.booleanValue;
	}

	@Override
	public final String toString() {
		return this.displayText;
	}

	public static final BooleanEnum getValueOf(boolean value) {
		if (value)
			return BooleanEnum.True;
		return BooleanEnum.False;
	}

	/**
	 * This method is used to
	 * 
	 * @return the booleanValue
	 */
	public boolean isBooleanValue() {
		return booleanValue;
	}

	/**
	 * This method is used to
	 * 
	 * @return the displayText
	 */
	public String getDisplayText() {
		return displayText;
	}

	/**
	 * @param booleanValue
	 *            the booleanValue to set
	 */
	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

	/**
	 * @param displayText
	 *            the displayText to set
	 */
	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

}
