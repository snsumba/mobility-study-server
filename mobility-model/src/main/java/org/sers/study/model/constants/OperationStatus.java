/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sers.study.model.constants;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
public enum OperationStatus {
	/*
	 * the status for a permitted operation
	 */
	TRUE("true"),
	/*
	 * the status for a non permitted operation
	 */
	FALSE("false");

	private String name;

	OperationStatus(String name) {
		this.name = name;
	}

	/**
	 * This method gets the name or title of the enumerator element
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
