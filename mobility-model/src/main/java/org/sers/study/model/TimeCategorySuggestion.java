package org.sers.study.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.primefaces.push.annotation.OnClose;
import org.sers.study.annotations.AuthorComment;

@AuthorComment
@Entity
@Table(name = "category_suggestions")
public class TimeCategorySuggestion extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String suggestion;

	/**
	 * 
	 */
	public TimeCategorySuggestion() {
		super();
	}

	/**
	 * @param suggestion
	 * @param timeDiaryCategory
	 */
	public TimeCategorySuggestion(String suggestion) {
		super();
		this.suggestion = suggestion;
	}

	/**
	 * This method is used to
	 * 
	 * @return the suggestion
	 */
	public String getSuggestion() {
		return suggestion;
	}

	/**
	 * @param suggestion
	 *            the suggestion to set
	 */
	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}

	@Override
	public String toString() {
		return this.suggestion;
	}

	@Override
	public boolean equals(Object object) {
		return object instanceof TimeCategorySuggestion && (super.getId() != null)
				? super.getId().equals(((TimeCategorySuggestion) object).getId()) : (object == this);
	}

	@Override
	public int hashCode() {
		return super.getId() != null ? this.getClass().hashCode() + super.getId().hashCode() : super.hashCode();
	}

}
