package org.sers.study.model.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.BaseEntity;
import org.sers.study.model.constants.Gender;
import org.sers.study.model.constants.UserStatus;

@AuthorComment(role = "represents a user in the system")
@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.JOINED)
public class User extends BaseEntity {

	private static final long serialVersionUID = 1L;
	public static final String DEFAULT_ADMIN = "administrator";
	public static final String DEFAULT_ADMIN_EMAIL = "admin@email.com";
	public static final String DEFAULT_FIRST_NAME = "System";
	public static final String DEFAULT_LAST_NAME = "Administrator";
	public static final String DEFAULT_PHONE_NUMBER = "";

	@SuppressWarnings("unused")
	private String fullName;
	private Gender gender;
	private UserStatus status;
	private Set<Role> roles;
	private Date dateOfBirth;
	private Date dateOfLastPasswordChange;
	private String username;
	private String password;
	private String clearTextPassword;
	private String salt;
	private String lastName;
	private String userId;
	private String firstName;
	private String phoneNumber;
	private String location;
	private Country country;
	private boolean changePassword;
	private String apiToken;

	/**
	 * @param gender
	 * @param dateOfBirth
	 * @param username
	 * @param password
	 * @param clearTextPassword
	 * @param lastName
	 * @param userId
	 * @param firstName
	 * @param phoneNumber
	 * @param location
	 */
	public User(Gender gender, Date dateOfBirth, String username, String password, String clearTextPassword,
			String lastName, String userId, String firstName, String phoneNumber, String location) {
		super();
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.username = username;
		this.password = password;
		this.clearTextPassword = clearTextPassword;
		this.lastName = lastName;
		this.userId = userId;
		this.firstName = firstName;
		this.phoneNumber = phoneNumber;
		this.location = location;
	}

	private String transactionStatus;

	/**
	 * This method is used to
	 * 
	 * @return the transactionStatus
	 */
	@Transient
	public String getTransactionStatus() {
		return transactionStatus;
	}

	/**
	 * @param transactionStatus
	 *            the transactionStatus to set
	 */
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	/**
	 * default constructor
	 */
	public User() {
		super();
	}

	/**
	 * constructor with initial specified values
	 * 
	 * @param username
	 * @param password
	 * @param roles
	 * @param secretQuestion
	 * @param secretAnswer
	 * @param status
	 * @param clearTextPassword
	 */
	public User(String username, String password, Set<Role> roles, UserStatus status, String clearTextPassword,
			String salt) {
		super();
		this.username = username;
		this.password = password;
		this.roles = roles;
		this.status = status;
		this.clearTextPassword = clearTextPassword;
		this.salt = salt;
	}

	/**
	 * @return the location
	 */
	@Column(name = "location", nullable = true)
	public String getLocation() {
		return location;
	}

	/**
	 * gets the clear text password. This is the password that is set by the
	 * user possibly from the user interface
	 * 
	 * @return the clearTextPassword
	 */
	@Transient
	public String getClearTextPassword() {
		return clearTextPassword;
	}

	/**
	 * gets the username of the user
	 * 
	 * @return the username
	 */
	@Column(name = "username", nullable = true, unique = true)
	public String getUsername() {
		return username;
	}

	/**
	 * gets the password of the user. This is the hashed password
	 * 
	 * @return the password
	 */
	@Column(name = "password", nullable = false)
	public String getPassword() {
		return password;
	}

	/**
	 * gets the roles of the user
	 * 
	 * @return the roles
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "role_users", joinColumns = @JoinColumn(name = "user_id") , inverseJoinColumns = @JoinColumn(name = "role_id") )
	public Set<Role> getRoles() {
		return roles;
	}

	/**
	 * @return the country
	 */
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "country_id", nullable = true)
	public Country getCountry() {
		return country;
	}

	/**
	 * gets the status of the user i.e. whether the user is disabled, enabled
	 * 
	 * @return the status
	 */

	@Column(name = "user_status", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	public UserStatus getStatus() {
		return status;
	}

	/**
	 * gets the salt used in determine the hashed password of the user
	 * 
	 * @return the salt
	 */
	@Column(name = "salt", nullable = false)
	public String getSalt() {
		return salt;
	}

	/**
	 * gets the organisation unit that this user belongs to.
	 * 
	 * @return the unit
	 */

	/**
	 * This method is used to
	 * 
	 * @return the userId
	 */
	@Column(name = "user_id", nullable = true)
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * checks whether the user has a new password
	 * 
	 * @return
	 */
	public boolean hasNewPassword() {
		return (clearTextPassword != null && clearTextPassword.trim().length() > 0);
	}

	/**
	 * gets the last name of this user.
	 * 
	 * @return
	 */
	@Column(name = "last_name", nullable = true)
	public String getLastName() {
		return lastName;
	}

	/**
	 * gets the first name for this user.
	 * 
	 * @return
	 */
	@Column(name = "first_name", nullable = true)
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the gender
	 */
	@Column(name = "gender", nullable = true)
	@Enumerated(EnumType.ORDINAL)
	public Gender getGender() {
		return gender;
	}

	/**
	 * @return the phoneNumber
	 */
	@Column(name = "phone_number", nullable = true)
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * gets the date the password was last changed.
	 * 
	 * @return the dateOfLastPasswordChange
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_last_password_change", nullable = true)
	public Date getDateOfLastPasswordChange() {
		return dateOfLastPasswordChange;
	}

	/**
	 * This method is used to
	 * 
	 * @return the dateOfBirth
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth", nullable = true)
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * This method is used to
	 * 
	 * @return the apiToken
	 */
	@Column(name = "api_token", nullable = true)
	public String getApiToken() {
		return apiToken;
	}

	/**
	 * @param apiToken
	 *            the apiToken to set
	 */
	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * gets a value indicating whether the password should be changed. This is a
	 * transient fields whose value is not stored.
	 * 
	 * @return the changePassword
	 */
	@Transient
	public boolean isChangePassword() {
		return changePassword;
	}

	/**
	 * adds a Role to the list of roles mapped to this user
	 * 
	 * @param role
	 */
	public void addRole(Role role) {
		if (roles == null) {
			roles = new HashSet<Role>();
		}

		if (!this.roles.contains(role)) {
			roles.add(role);
		}
	}

	/**
	 * Removes a role from a set of roles mapped to this user *
	 * 
	 * @param role
	 */
	public void removeRole(Role role) {
		if (roles != null) {
			for (Role r : roles) {
				if (r.getName().equals(role.getName())) {
					roles.remove(role);
					break;
				}
			}
		}
	}

	/**
	 * sets the status of the user
	 * 
	 * @see #getStatus()
	 * @param status
	 *            the status to set
	 */
	public void setStatus(UserStatus status) {
		this.status = status;
	}

	/**
	 * @return the fullName
	 */
	@Transient
	public String getFullName() {
		return this.toString();
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param roles
	 *            the roles to set
	 */
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	/**
	 * @param clearTextPassword
	 *            the clearTextPassword to set
	 */
	public void setClearTextPassword(String clearTextPassword) {
		this.clearTextPassword = clearTextPassword;
	}

	/**
	 * @param salt
	 *            the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * @param dateOfLastPasswordChange
	 *            the dateOfLastPasswordChange to set
	 */
	public void setDateOfLastPasswordChange(Date dateOfLastPasswordChange) {
		this.dateOfLastPasswordChange = dateOfLastPasswordChange;
	}

	/**
	 * @param changePassword
	 *            the changePassword to set
	 */
	public void setChangePassword(boolean changePassword) {
		this.changePassword = changePassword;
	}

	/**
	 * @param fullName
	 *            the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * gets a list of permissions for this user
	 * 
	 * @return
	 */
	public List<Permission> findPermissions() {
		List<Permission> permissions = null;
		if (roles != null && roles.size() > 0) {
			permissions = new ArrayList<Permission>();
			for (Role role : roles) {
				if (role.getPermissions() != null && role.getPermissions().size() > 0) {
					for (Permission perm : role.getPermissions()) {
						permissions.add(perm);
					}
				}
			}
		}
		return permissions;
	}

	/**
	 * checks whether this user has the given permission.
	 * 
	 * @param perm
	 * @return
	 */
	public boolean hasPermission(String perm) {
		if (this.hasAdministrativePrivileges())
			return true;

		if (this.roles != null) {
			for (Role role : this.roles) {
				if (role.hasPermission(perm)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * determines if the user has the default administrative role
	 * 
	 * @return
	 */
	public boolean hasAdministrativePrivileges() {
		if (roles != null) {
			for (Role role : roles) {
				if (role.checkIfDefaultAdminRole()) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean hasRole(String roleName) {
		if (this.hasAdministrativePrivileges())
			return true;

		if (this.roles != null) {
			for (Role role : this.roles) {
				if (role.getName().equals(roleName)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {

		if (this.lastName != null && this.firstName != null)
			return this.firstName + " " + this.lastName;

		return super.toString();
	}

	/**
	 * checks whether this user has the given permission.
	 * 
	 * @param perm
	 * @return
	 */
	public boolean hasCreater(User user) {
		if (user != null) {
			if (user.getChangedBy() != null) {
				return true;
			}
		}
		return false;
	}

	// This must return true for another User object with same key/id.
	public boolean equals(Object other) {
		return other instanceof User && (id != null) ? id.equals(((User) other).id) : (other == this);
	}

	// This must return the same hashcode for every User object with the
	// same
	// key.
	public int hashCode() {
		return id != null ? this.getClass().hashCode() + id.hashCode() : super.hashCode();
	}

}