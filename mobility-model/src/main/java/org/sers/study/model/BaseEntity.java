package org.sers.study.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.security.User;


@AuthorComment(role="Base class for all Data Objects in the application.")
@MappedSuperclass
public class BaseEntity implements Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6095671201979163425L;
	/**
	 * unique ID for the objects.
	 */
	protected String id = null;
	private RecordStatus recordStatus = RecordStatus.ACTIVE;
	private User createdBy;
	private User changedBy;
	private Date dateCreated = new Date();
	private Date dateChanged = new Date();
	private String emailAddress;
	private String editStatus = OperationStatus.FALSE.getName();
	private String activateStatus = OperationStatus.FALSE.getName();
	private String deactivateStatus = OperationStatus.FALSE.getName();
	private String viewStatus = OperationStatus.FALSE.getName();

	/**
	 * default constructor
	 */
	public BaseEntity() {
		super();
	}

	public BaseEntity(User user) {
		super();
		this.createdBy = user;
		this.changedBy = user;
	}

	/**
	 * @return the editStatus
	 */
	@Column(name = "edit_status", nullable = true)
	public String getEditStatus() {
		return editStatus;
	}

	/**
	 * @return the activateStatus
	 */
	@Column(name = "activate_status", nullable = true)
	public String getActivateStatus() {
		return activateStatus;
	}

	/**
	 * @return the deactivateStatus
	 */
	@Column(name = "deactivate_status", nullable = true)
	public String getDeactivateStatus() {
		return deactivateStatus;
	}

	/**
	 * @return the viewStatus
	 */
	@Column(name = "view_status", nullable = true)
	public String getViewStatus() {
		return viewStatus;
	}

	/**
	 * @param viewStatus
	 *            the viewStatus to set
	 */
	public void setViewStatus(String viewStatus) {
		this.viewStatus = viewStatus;
	}
	/**
	 * @param editStatus
	 *            the editStatus to set
	 */
	public void setEditStatus(String editStatus) {
		this.editStatus = editStatus;
	}

	/**
	 * @param activateStatus
	 *            the activateStatus to set
	 */
	public void setActivateStatus(String activateStatus) {
		this.activateStatus = activateStatus;
	}

	/**
	 * @param deactivateStatus
	 *            the deactivateStatus to set
	 */
	public void setDeactivateStatus(String deactivateStatus) {
		this.deactivateStatus = deactivateStatus;
	}

	/**
	 * gets the id of the entity.
	 * 
	 * @return {@link #id}
	 */
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}

	/**
	 * sets the id of the entity.
	 * 
	 * @param id
	 *            {@link #id}
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * gets the record status of an entity
	 * 
	 * @return the recordStatus
	 */
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "record_status", nullable = false)
	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	/**
	 * gets or sets the email address of the user
	 * 
	 * @return the emailAddress
	 */
	@Column(name = "email_address", nullable = true)
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * sets the email address of the user
	 * 
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * sets the record status of the entity
	 * 
	 * @param recordStatus
	 *            the recordStatus to set
	 */
	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Override
	@OneToOne
	@JoinColumn(name = "created_by", nullable = true)
	public User getCreatedBy() {
		return this.createdBy;
	}

	@Override
	public void setCreatedBy(User user) {
		this.createdBy = user;
	}

	@Override
	@Temporal(TemporalType.DATE)
	@Column(name = "date_created", nullable = true)
	public Date getDateCreated() {
		return this.dateCreated;
	}

	@Override
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	@OneToOne
	@JoinColumn(name = "changed_by", nullable = true)
	public User getChangedBy() {
		return this.changedBy;
	}

	@Override
	public void setChangedBy(User changedBy) {
		this.changedBy = changedBy;
	}

	@Override
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_changed", nullable = true)
	public Date getDateChanged() {
		return this.dateChanged;
	}

	@Override
	public void setDateChanged(Date dateChanged) {
		this.dateChanged = dateChanged;
	}

	@Override
	public String toString() {
		return "Egosms Evaluation";
	}
}
