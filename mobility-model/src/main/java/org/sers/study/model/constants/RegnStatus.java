package org.sers.study.model.constants;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
public enum RegnStatus {
	ACTIVE("Active"), DE_ACTIVE("De-active"), SUSPENDED("Suspended");
	/**
	 * constructor with initial specified value
	 * 
	 * @param name
	 */
	RegnStatus(String name) {
		this.name = name;
	}

	private String name;

	/**
	 * gets the title of the enumerated value
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}
};