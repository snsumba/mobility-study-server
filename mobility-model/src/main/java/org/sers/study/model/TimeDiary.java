package org.sers.study.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.security.User;

@AuthorComment
@Entity
@Table(name = "times_diaries")
public class TimeDiary extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;
	private String recordId;
	private TimeCategorySuggestion categorySuggestion;
	private String registrationDate;
	private String startTime;
	private String endTime;

	/**
	 * 
	 */
	public TimeDiary() {
		super();
	}

	/**
	 * @param user
	 * @param recordId
	 * @param categorySuggestion
	 * @param registrationDate
	 * @param startTime
	 * @param endTime
	 */
	public TimeDiary(User user, String recordId, TimeCategorySuggestion categorySuggestion, String registrationDate,
			String startTime, String endTime) {
		super();
		this.user = user;
		this.recordId = recordId;
		this.categorySuggestion = categorySuggestion;
		this.registrationDate = registrationDate;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	/**
	 * This method is used to
	 * 
	 * @return the user
	 */
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return user;
	}

	/**
	 * This method is used to
	 * 
	 * @return the categorySuggestion
	 */
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "category_suggestion_id", nullable = true)
	public TimeCategorySuggestion getCategorySuggestion() {
		return categorySuggestion;
	}

	/**
	 * This method is used to
	 * 
	 * @return the recordId
	 */
	@Column(name = "record_id")
	public String getRecordId() {
		return recordId;
	}

	/**
	 * This method is used to
	 * 
	 * @return the registrationDate
	 */
	@Column(name = "registration_date")
	public String getRegistrationDate() {
		return registrationDate;
	}

	/**
	 * This method is used to
	 * 
	 * @return the startTime
	 */
	@Column(name = "start_time")
	public String getStartTime() {
		return startTime;
	}

	/**
	 * This method is used to
	 * 
	 * @return the endTime
	 */
	@Column(name = "end_time")
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @param recordId
	 *            the recordId to set
	 */
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	/**
	 * @param categorySuggestion
	 *            the categorySuggestion to set
	 */
	public void setCategorySuggestion(TimeCategorySuggestion categorySuggestion) {
		this.categorySuggestion = categorySuggestion;
	}

	/**
	 * @param registrationDate
	 *            the registrationDate to set
	 */
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return this.user.toString() + " " + categorySuggestion;
	}

	@Override
	public boolean equals(Object object) {
		return object instanceof CapturedLocation && (super.getId() != null)
				? super.getId().equals(((CapturedLocation) object).getId()) : (object == this);
	}

	@Override
	public int hashCode() {
		return super.getId() != null ? this.getClass().hashCode() + super.getId().hashCode() : super.hashCode();
	}
}
