package org.sers.study.model.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.BaseEntity;

@AuthorComment
@Entity
@Table(name = "countries")
@Inheritance(strategy = InheritanceType.JOINED)
public class Country extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private int postalCode;

	private String transactionStatus;
	/**
	 * This method is used to 
	 * @return the transactionStatus
	 */
	@Transient
	public String getTransactionStatus() {
		return transactionStatus;
	}

	/**
	 * @param transactionStatus the transactionStatus to set
	 */
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	/**
	 * 
	 */
	public Country() {
		super();
	}

	/**
	 * @param name
	 */
	public Country(String name) {
		super();
		this.name = name;
	}

	/**
	 * @return the name
	 */
	@Column(name = "name", nullable = false, unique = true)
	public String getName() {
		return name;
	}

	/**
	 * @return the postalCode
	 */
	@Column(name = "postal_code", nullable = false)
	public int getPostalCode() {
		return postalCode;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public boolean equals(Object object) {
		return object instanceof Country && (super.getId() != null) ? super.getId().equals(((Country) object).getId())
				: (object == this);
	}

	@Override
	public int hashCode() {
		return super.getId() != null ? this.getClass().hashCode() + super.getId().hashCode() : super.hashCode();
	}
}
