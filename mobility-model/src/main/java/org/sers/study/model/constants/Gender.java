package org.sers.study.model.constants;

import org.sers.study.annotations.AuthorComment;


@AuthorComment(role="Represents the gender of a person")
public enum Gender {
	/**
	 * Male sex
	 */
	MALE("Male"),

	/**
	 * Female sex
	 */
	FEMALE("Female"),
	/**
	 * Unkown sex
	 */
	UNKNOWN("Unknown");

	/**
	 * constructor with initial specified value
	 * 
	 * @param name
	 */
	Gender(String name) {
		this.name = name;
	}

	private String name;

	/**
	 * gets the title of the enumerated value
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return this.name;
	}
};