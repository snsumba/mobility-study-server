package org.sers.study.model.constants;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
public enum UserStatus {

	/**
	 * Enabled user
	 */
	ACTIVE("Enabled"),

	/**
	 * Deleted user
	 */
	DELETED("Deleted");

	/**
	 * constructor with initial specified value
	 * 
	 * @param status
	 */
	UserStatus(String status) {
		this.status = status;
	}

	private String status;

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		if (status != null && !status.isEmpty())
			return status;
		return "";
	}
};