package org.sers.study.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "duration_info")
public class DurationInfo extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long delayInMinutes;
	private long distanceApart;

	/**
	 * 
	 */
	public DurationInfo() {
		super();
	}

	/**
	 * @param delayInMinutes
	 * @param distanceApart
	 */
	public DurationInfo(long delayInMinutes, long distanceApart) {
		super();
		this.delayInMinutes = delayInMinutes;
		this.distanceApart = distanceApart;
	}

	/**
	 * This method is used to
	 * 
	 * @return the delayInMinutes
	 */
	@Column(name = "delay_in_minutes")
	public long getDelayInMinutes() {
		return delayInMinutes;
	}

	/**
	 * This method is used to
	 * 
	 * @return the distanceApart
	 */
	@Column(name = "distance_apart")
	public long getDistanceApart() {
		return distanceApart;
	}

	/**
	 * @param delayInMinutes
	 *            the delayInMinutes to set
	 */
	public void setDelayInMinutes(long delayInMinutes) {
		this.delayInMinutes = delayInMinutes;
	}

	/**
	 * @param distanceApart
	 *            the distanceApart to set
	 */
	public void setDistanceApart(long distanceApart) {
		this.distanceApart = distanceApart;
	}

}
