package org.sers.study.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.MethodType;
import org.sers.study.model.constants.OperationStatus;

@AuthorComment
@Entity
@Table(name = "app_type")
public class AppType extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MethodType methodType;
	private OperationStatus operationStatus;

	/**
	 * 
	 */
	public AppType() {
		super();
	}

	/**
	 * @param methodType
	 * @param operationStatus
	 */
	public AppType(MethodType methodType, OperationStatus operationStatus) {
		super();
		this.methodType = methodType;
		this.operationStatus = operationStatus;
	}

	/**
	 * This method is used to
	 * 
	 * @return the methodType
	 */
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "method_type")
	public MethodType getMethodType() {
		return methodType;
	}

	/**
	 * This method is used to
	 * 
	 * @return the operationStatus
	 */
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "operation_status")
	public OperationStatus getOperationStatus() {
		return operationStatus;
	}

	/**
	 * @param methodType
	 *            the methodType to set
	 */
	public void setMethodType(MethodType methodType) {
		this.methodType = methodType;
	}

	/**
	 * @param operationStatus
	 *            the operationStatus to set
	 */
	public void setOperationStatus(OperationStatus operationStatus) {
		this.operationStatus = operationStatus;
	}

	@Override
	public String toString() {
		return this.methodType.getName() + " " + operationStatus.getName();
	}

	@Override
	public boolean equals(Object object) {
		return object instanceof AppType && (super.getId() != null) ? super.getId().equals(((AppType) object).getId())
				: (object == this);
	}

	@Override
	public int hashCode() {
		return super.getId() != null ? this.getClass().hashCode() + super.getId().hashCode() : super.hashCode();
	}

}
