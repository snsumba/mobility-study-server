package org.sers.study.general;

import java.math.BigInteger;
import java.security.SecureRandom;


public class RandomStringGenerator {

	public RandomStringGenerator() {

	}

	public String generateRandomString() {
		return new BigInteger(130, new SecureRandom()).toString(32);
	}
}
