package org.sers.study.general;


public class RequiredContactNumber {

	public static String airtelPhoneNumber(String provided_number){
		String generated_contact = "";//0773669561
		if(provided_number.trim().length() == 16) generated_contact = provided_number.replaceAll(" ", "").substring(4, 13);
		else if(provided_number.trim().length() == 15) generated_contact = provided_number.replaceAll(" ", "").substring(4, 13);
		else if(provided_number.trim().length() == 13) generated_contact = provided_number.substring(4, 13);
		else if(provided_number.trim().length() == 12) generated_contact = provided_number.substring(3, 12);
		else if(provided_number.trim().length() == 11) generated_contact = provided_number.replaceAll(" ", "").substring(1, 10);
		else if(provided_number.trim().length() == 10) generated_contact = provided_number.substring(1, 10);
		
		System.out.println("generated_contact : "+generated_contact+" size : "+generated_contact.length());
		return generated_contact;
	}
	
	public static String phoneNumber(String provided_number){
		String generated_contact = "";
		if(provided_number.trim().length() == 16) generated_contact = "0"+provided_number.replaceAll(" ", "").substring(4, 13);
		else if(provided_number.trim().length() == 15) generated_contact = "0"+provided_number.replaceAll(" ", "").substring(4, 13);
		else if(provided_number.trim().length() == 13) generated_contact = "0"+provided_number.substring(4, 13);
		else if(provided_number.trim().length() == 12) generated_contact = "0"+provided_number.substring(3, 12);
		else if(provided_number.trim().length() == 11) generated_contact = provided_number.replaceAll(" ", "");
		else if(provided_number.trim().length() == 10) generated_contact = provided_number;
		
		System.out.println("generated_contact : "+generated_contact+" size : "+generated_contact.length());
		return generated_contact;
	}
	
	/*public static String phoneNumber256(String provided_number){//1503000337
		String generated_contact = "";
		if(provided_number.trim().length() == 16) generated_contact = provided_number.replaceAll(" ", "").substring(1, 13);
		else if(provided_number.trim().length() == 15) generated_contact = provided_number.replaceAll(" ", "").substring(1, 13);
		else if(provided_number.trim().length() == 13) generated_contact = provided_number.substring(1, 13);
		else if(provided_number.trim().length() == 12) generated_contact = provided_number;
		else if(provided_number.trim().length() == 11) generated_contact = "256"+provided_number.replaceAll(" ", "").substring(1, 10);
		else if(provided_number.trim().length() == 10) generated_contact = "256"+provided_number.substring(1, 10);
		
		System.out.println("generated_contact : "+generated_contact+" size : "+generated_contact.length());
		return generated_contact;
	}*/
	
	public static String phoneNumber256(String service_provider,String provided_number){//1503000337 
		String generated_contact = "";
		System.out.println("service_provider : "+service_provider);
		System.out.println("service_provider : "+provided_number);
		if(service_provider.equalsIgnoreCase("SMILECOMS") 
				|| service_provider.equalsIgnoreCase("SMILE_AIRTIME")
				|| service_provider.equalsIgnoreCase("Smile Data")
				|| service_provider.equalsIgnoreCase("Smile Airtime")){
			generated_contact = provided_number;
		}else{
			if(provided_number.contains("07") || provided_number.contains("256")){
				if(provided_number.trim().length() == 16) generated_contact = provided_number.replaceAll(" ", "").substring(1, 13);
				else if(provided_number.trim().length() == 15) generated_contact = provided_number.replaceAll(" ", "").substring(1, 13);
				else if(provided_number.trim().length() == 13) generated_contact = provided_number.substring(1, 13);
				else if(provided_number.trim().length() == 12) generated_contact = provided_number;
				else if(provided_number.trim().length() == 11) generated_contact = "256"+provided_number.replaceAll(" ", "").substring(1, 10);
				else if(provided_number.trim().length() == 10) generated_contact = "256"+provided_number.substring(1, 10);	
			}else if(provided_number.contains("03") || provided_number.contains("04")){
				generated_contact = provided_number;
			}
		}
		System.out.println("generated_contact : "+generated_contact+" size : "+generated_contact.length());
		return generated_contact;
	}
}
