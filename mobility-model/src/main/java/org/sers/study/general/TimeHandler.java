package org.sers.study.general;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeHandler {

	public String getCurrentYear() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public String getCurrentMonth() {
		DateFormat dateFormat = new SimpleDateFormat("MM");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public String getCurrentDay() {
		DateFormat dateFormat = new SimpleDateFormat("dd");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public String getTransactionYear(String inputDate) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Date date = new Date(inputDate);
		return dateFormat.format(date);
	}

	public String getTransactionMonth(String inputDate) {
		DateFormat dateFormat = new SimpleDateFormat("MM");
		Date date = new Date(inputDate);
		return dateFormat.format(date);
	}

	public String getTransactionDay(String inputDate) {
		DateFormat dateFormat = new SimpleDateFormat("dd");
		Date date = new Date(inputDate);
		return dateFormat.format(date);
	}

	public String getPaymentDate(String transactionDate) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(transactionDate.replace("-", "/"));
		return dateFormat.format(date);
	}

	public String getPaymentYear(String transactionDate) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Date date = new Date(transactionDate);
		return dateFormat.format(date);
	}

	public String getPaymentMonth(String transactionDate) {
		DateFormat dateFormat = new SimpleDateFormat("MM");
		Date date = new Date(transactionDate);
		return dateFormat.format(date);
	}

	public String getPaymentDay(String transactionDate) {
		DateFormat dateFormat = new SimpleDateFormat("dd");
		Date date = new Date(transactionDate);
		return dateFormat.format(date);
	}

	public String getPaymentMinutes(String transactionDate) {
		DateFormat dateFormat = new SimpleDateFormat("mm");
		Date date = new Date(transactionDate);
		return dateFormat.format(date);
	}

	public String getPaymentHour(String transactionDate) {
		DateFormat dateFormat = new SimpleDateFormat("HH");
		Date date = new Date(transactionDate);
		return dateFormat.format(date);
	}

	public String getDateNarative() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public String getHourNarative() {
		DateFormat dateFormat = new SimpleDateFormat("HH");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public String getMinuteNarative() {
		DateFormat dateFormat = new SimpleDateFormat("mm");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public String generateRandomString() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(20, random).toString(11);
	}

	public String getCurrentTimeAndDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public String getPaywayTimeAndDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String date_part = dateFormat.format(date);
		// HH:mm:ss.SSS
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");
		Date time = new Date();
		String time_part = timeFormat.format(time);
		time_part += "000+06:00";
		return date_part.trim() + "T" + time_part.trim();
	}

	public String getTimeOfTransaction() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd : HH:mm:ss");
		Date currentDate = new Date();
		String currentDateString = dateFormat.format(currentDate);
		return currentDateString;
	}

	public String getUsernameLastNumbers() {
		DateFormat dateFormat = new SimpleDateFormat("ddss");
		Date currentDate = new Date();
		String currentDateString = dateFormat.format(currentDate);
		return currentDateString;
	}

	public String getOtherNumbers() {
		DateFormat dateFormat = new SimpleDateFormat("MMmm");
		Date currentDate = new Date();
		String currentDateString = dateFormat.format(currentDate);
		return currentDateString;
	}

	public String getInterswitchRef() {
		DateFormat dateFormat = new SimpleDateFormat("ddssmm");
		Date currentDate = new Date();
		String currentDateString = dateFormat.format(currentDate);
		return currentDateString;
	}

	public String getCurrentTimeInMilliSeconds() {
		return "" + new Date().getTime();
	}

	public String getCurrentTimeInSeconds() {
		return "" + new Date().getSeconds();
	}

	public String getYearTimeStamp() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Date currentDate = new Date();
		String currentDateString = dateFormat.format(currentDate);
		return currentDateString;
	}

	public String formatStringIntoParsableDate(String date, String format) {
		String[] temp_array_of_strings = date.split(" ");
		String final_date_string_month = "";
		String final_date_string_date = "";
		String final_date_string_year = "";
		if (format.equalsIgnoreCase("dd/mm/yyyy")) {
			final_date_string_month = this.getMonthNumber(temp_array_of_strings[0].trim());
			final_date_string_date = temp_array_of_strings[1];
			final_date_string_year = temp_array_of_strings[2];
		}
		return final_date_string_date + "/" + final_date_string_month + "/" + final_date_string_year;
	}

	private String getMonthNumber(String month_name) {
		if (month_name.equalsIgnoreCase("January")) {
			return "01";
		} else if (month_name.equalsIgnoreCase("February")) {
			return "02";
		} else if (month_name.equalsIgnoreCase("March")) {
			return "03";
		} else if (month_name.equalsIgnoreCase("April")) {
			return "04";
		} else if (month_name.equalsIgnoreCase("May")) {
			return "05";
		} else if (month_name.equalsIgnoreCase("June")) {
			return "06";
		} else if (month_name.equalsIgnoreCase("July")) {
			return "07";
		} else if (month_name.equalsIgnoreCase("August")) {
			return "08";
		} else if (month_name.equalsIgnoreCase("September")) {
			return "09";
		} else if (month_name.equalsIgnoreCase("October")) {
			return "10";
		} else if (month_name.equalsIgnoreCase("November")) {
			return "11";
		} else if (month_name.equalsIgnoreCase("December")) {
			return "12";
		} else {
			return null;
		}
	}

	public Calendar getCurrentCalendarDate() {
		Calendar calendar = Calendar.getInstance();
		return calendar;
	}

	public Calendar getCalendarObjectFromStringDate(String date_string, String format) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(date_string);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("TimeHandler#getCalendarObjectFromStringDate exception : " + e.getMessage());
			return null;
		}
	}

	public String getDateStringFromCalendar(Calendar calendar_object) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		return dateFormat.format(calendar_object.getTime());
	}

	public String getCurrentDateString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		return dateFormat.format(Calendar.getInstance().getTime());
	}

	public String getPegasusCurrentDateString() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(Calendar.getInstance().getTime());
	}

	public String getDateStringAfterSetDays(int num_of_days) {
		return new SimpleDateFormat("yyyy/MM/dd").format(this.getCalendarAfterSetDays(num_of_days).getTime());
	}

	public Calendar getCalendarAfterSetDays(int number_of_days) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, number_of_days);
		return calendar;
	}

	public String getDateAndTimeFromCalendarObject(Calendar calendar_object) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date_object = calendar_object.getTime();
		return dateFormat.format(date_object);
	}

	public static Date getFirstDateOfThisMonth() {
		String[] dateTokens = new SimpleDateFormat("dd-MM-yyyy").format(new Date()).split("-");
		return new GregorianCalendar(Integer.parseInt(dateTokens[2]), Integer.parseInt(dateTokens[1]) - 1, 01)
				.getTime();
	}

	public static Date getLastDateOfThisMonth() {
		String[] dateTokens = new SimpleDateFormat("dd-MM-yyyy").format(new Date()).split("-");
		return new GregorianCalendar(Integer.parseInt(dateTokens[2]), Integer.parseInt(dateTokens[1]) - 1, 31)
				.getTime();
	}

	public String getAfricellTimeOfTransaction() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date currentDate = new Date();
		String currentDateString = dateFormat.format(currentDate);
		return currentDateString;
	}

	public String getAfricellMoneyTimeOfTransaction() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = new Date();
		String currentDateString = dateFormat.format(currentDate);
		return currentDateString;
	}

	public void testDateString() {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		System.out.println("Date 1 :" + format.format(getFirstDateOfThisMonth()));
		System.out.println("Date 2 :" + format.format(getLastDateOfThisMonth()));
	}

	public String generatePasswordString() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}
}
