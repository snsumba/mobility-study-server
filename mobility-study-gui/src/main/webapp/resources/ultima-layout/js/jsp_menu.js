$(function() {
			$('#menu-button').on('click', function(e) {
				var menu = $('#menu');
				if (menu.hasClass('lmenu-active')) {
					menu.addClass('fadeOutUp');

					setTimeout(function() {
						menu.removeClass('fadeOutUp fadeInDown lmenu-active');
					}, 500);
				} else {
					menu.addClass('lmenu-active fadeInDown');
				}
				e.preventDefault();
			});
		});