<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page
	import="org.sers.study.client.views.render.CopyrightDateRenderer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	String url = StringUtils.replace(request.getRequestURL().toString(), request.getRequestURI(),
			request.getContextPath());
	String baseUrl = url.replaceAll("http", "https");
	if (url.contains("www.yodime.com"))
		baseUrl = url.replaceAll("http", "https");
	else
		baseUrl = url;
	request.setAttribute("baseUrl", baseUrl);
	String copyright = new CopyrightDateRenderer().getCopyright();
%>
<html>
<head>
<title>Registration successful</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet"
	href="<%=baseUrl%>/static/css/system.css" />
<link type="text/css" rel="stylesheet"
	href="<%=baseUrl%>/static/css/bootstrap-theme.min.css" />
<link type="text/css" rel="stylesheet"
	href="<%=baseUrl%>/static/css/bootstrap-theme.css" />
<link type="text/css" rel="stylesheet"
	href="<%=baseUrl%>/static/css/bootstrap.min.css" />
<link type="text/css" rel="stylesheet"
	href="<%=baseUrl%>/static/css/bootstrap.css" />
<link type="text/css" rel="stylesheet"
	href="<%=baseUrl%>/static/css/style.css" />
<link href="<%=baseUrl%>/static/css/custom/custom.css" rel="stylesheet">
<style>
.login {
	background-image: url("<%=baseUrl%>/static/images/login.png");
}
</style>
</head>

<body>
	<div class="login-css-style login">
		<div style="height: 15%;"></div>
		<div class="account-wall account-wall-inc1">
			<p class="header-style"></p>
			<h6>Registration Successful</h6>
			<h2 class="header-style">Please Login</h2>
			<form class="form-signin"
				action="<%=baseUrl%>/j_spring_security_check" method="post">
				<div>
					<input type="text" class="form-control form-size"
						style="width: 100%" name="j_username" id="j_username"
						placeholder="Email / Phone number / YoDime ID">
				</div>
				<br>
				<div>
					<input type="password" class="form-control form-size"
						style="width: 100%" name="j_password" id="j_password"
						placeholder="Password">
				</div>
				<div style="padding-top: 2px; margin-top: 2.0em;" class="error">
					<%
						if (request.getAttribute("errorMessage") != null
								&& StringUtils.isNotBlank((String) request.getAttribute("errorMessage"))) {
					%>
					<%=request.getAttribute("errorMessage")%>
					<%
						}
					%>
				</div>
				<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
			</form>
			<%-- <h5 align="center">
				<a href="<%=baseUrl%>/Recovery" class="login-link">Forgot
					Password?</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a
					href="<%=baseUrl%>" class="login-link">Home</a>
			</h5> --%>

			<h5 align="center">
				<a href="<%=baseUrl%>/SignUp" class="login-link">Create Account</a>
			</h5>
		</div>
		<div>
			<h2 class="footer-style">
				<span class="footer-text-left">Copyright © <%=copyright%>
					Yodime (U) Ltd. All ​rights reserved.
				</span>
			</h2>
		</div>
	</div>
</body>
</html>