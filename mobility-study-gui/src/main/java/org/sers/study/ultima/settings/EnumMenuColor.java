package org.sers.study.ultima.settings;

import org.sers.study.annotations.AuthorComment;

/**
 * 
 */
@AuthorComment
public enum EnumMenuColor {
	SELECT("Please Select Theme ..."),
	LIGHT_MENU("Light Menu"),
	DARK_MENU("layout-menu-dark");

	/**
	 * constructor with initial specified value
	 * 
	 * @param name
	 */
	EnumMenuColor(String name) {
		this.name = name;
	}

	private String name;

	/**
	 * gets the title of the enumerated value
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}
};