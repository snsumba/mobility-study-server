package org.sers.study.ultima.settings;

import org.sers.study.annotations.AuthorComment;

/**
 * 
 */
@AuthorComment
public enum EnumThemes {
	SELECT("Please Select Theme ..."),
	INDIGO_PINK("indigo"),
	BROWN_GREEN("brown"),
	BLUE_AMBER("blue"),
	BLUE_GREY_GREEN("blue-grey"),
	DARK_BLUE("dark-blue"),
	DARK_GREEN("dark-green"),
	GREEN_YELLOW("green"),
	PURPLE_CYAN("purple"),
	PURPLE_AMBER("purple-amber"),
	TEAL_LIME("teal"),
	CYAN_AMBER("cyan"),
	GREY_DEEP_ORANGE("grey");

	/**
	 * constructor with initial specified value
	 * 
	 * @param name
	 */
	EnumThemes(String name) {
		this.name = name;
	}

	private String name;

	/**
	 * gets the title of the enumerated value
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}
};