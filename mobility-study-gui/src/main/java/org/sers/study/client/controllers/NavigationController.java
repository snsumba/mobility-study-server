package org.sers.study.client.controllers;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
@ManagedBean(name = "navigationController", eager = true)
@ApplicationScoped
public class NavigationController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String profileView = HyperLinks.PROFILE_VIEW;
	private String homePage = HyperLinks.HOMEPAGE;
	private String rolesView = HyperLinks.ROLESVIEW;
	private String rolesForm = HyperLinks.ROLESFORM;
	private String userRoles = HyperLinks.USER_ROLES;
	private String usersView = HyperLinks.USERSVIEW;
	private String userForm = HyperLinks.USERFORM;
	private String countryForm = HyperLinks.COUNTRIES_UPLOAD;
	private String countriesView = HyperLinks.COUNTRIES_VIEW;
	private String viewMemberProfile = HyperLinks.VIEW_MEMBER_PROFILE;

	private String capturedLocations = HyperLinks.VIEW_CAPTURED_LOCATIONS;
	private String mappedLocations = HyperLinks.VIEW_MAPPED_LOCATIONS;
	private String circlerMappedLocations = HyperLinks.VIEW_CIRCLER_MAPPED_LOCATIONS;
	private String durationInfo = HyperLinks.VIEW_DURATION_INFO;

	private String viewTimeDiary = HyperLinks.VIEW_TIME_DIARY;
	private String viewAppType = HyperLinks.VIEW_APP_TYPE;

	private String viewDownloadCapturedLocationsForm = HyperLinks.VIEW_DOWN_LOAD_CAPTURED_LOCATIONS;
	private String viewDownloadTimediaryForm = HyperLinks.VIEW_DOWN_LOAD_TIME_DIARY;

	private String viewTimeDiaryCategorySuggestions = HyperLinks.VIEW_TIME_DIARY_CATEGORY_SUGGESTIONS;

	/**
	 * @return the viewDownloadCapturedLocationsForm
	 */
	public String getViewDownloadCapturedLocationsForm() {
		return viewDownloadCapturedLocationsForm;
	}

	/**
	 * @return the viewDownloadTimediaryForm
	 */
	public String getViewDownloadTimediaryForm() {
		return viewDownloadTimediaryForm;
	}

	/**
	 * @return the viewTimeDiaryCategorySuggestions
	 */
	public String getViewTimeDiaryCategorySuggestions() {
		return viewTimeDiaryCategorySuggestions;
	}

	/**
	 * @return the viewTimeDiary
	 */
	public String getViewTimeDiary() {
		return viewTimeDiary;
	}

	/**
	 * @return the viewAppType
	 */
	public String getViewAppType() {
		return viewAppType;
	}

	/**
	 * @return the circlerMappedLocations
	 */
	public String getCirclerMappedLocations() {
		return circlerMappedLocations;
	}

	/**
	 * @param circlerMappedLocations
	 *            the circlerMappedLocations to set
	 */
	public void setCirclerMappedLocations(String circlerMappedLocations) {
		this.circlerMappedLocations = circlerMappedLocations;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the durationInfo
	 */
	public String getDurationInfo() {
		return durationInfo;
	}

	/**
	 * @return the capturedLocations
	 */
	public String getCapturedLocations() {
		return capturedLocations;
	}

	/**
	 * @return the mappedLocations
	 */
	public String getMappedLocations() {
		return mappedLocations;
	}

	/**
	 * @return the profileView
	 */
	public String getProfileView() {
		return profileView;
	}

	/**
	 * @return the homePage
	 */
	public String getHomePage() {
		return homePage;
	}

	/**
	 * @return the rolesView
	 */
	public String getRolesView() {
		return rolesView;
	}

	/**
	 * @return the rolesForm
	 */
	public String getRolesForm() {
		return rolesForm;
	}

	/**
	 * @return the userRoles
	 */
	public String getUserRoles() {
		return userRoles;
	}

	/**
	 * @return the usersView
	 */
	public String getUsersView() {
		return usersView;
	}

	/**
	 * @return the userForm
	 */
	public String getUserForm() {
		return userForm;
	}

	/**
	 * @return the countryForm
	 */
	public String getCountryForm() {
		return countryForm;
	}

	/**
	 * @return the countriesView
	 */
	public String getCountriesView() {
		return countriesView;
	}

	/**
	 * @return the viewMemberProfile
	 */
	public String getViewMemberProfile() {
		return viewMemberProfile;
	}

}
