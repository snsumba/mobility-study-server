package org.sers.study.client.setup.landingpage;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.general.TimeHandler;
import org.sers.study.model.constants.Gender;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.constants.UserStatus;
import org.sers.study.model.security.Country;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.CountryService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@ViewScoped
public class RegistrationForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Gender> listOfGenders;
	private List<Country> listOfCountries;
	private static final String SUCCESS_URL = "/registrationSuccessful";
	private Country country;
	private User user;
	private String firstName;
	private String lastName;
	private BigInteger phoneNumber;
	private String emailAddress;
	private String username;
	private String clearTextPassword;

	@PostConstruct
	public void init() {
		listOfGenders = new ArrayList<Gender>();
		listOfGenders.addAll(Arrays.asList(Gender.values()));
		listOfCountries = ApplicationContextProvider.getApplicationContext().getBean(CountryService.class)
				.getCountries();
	}

	public void saveAccount() throws Exception {
		try {
			this.user = new User();
			this.user.setFirstName(this.firstName);
			this.user.setLastName(this.lastName);
			this.user.setUsername(this.username);
			this.user.setUsername(this.firstName + new TimeHandler().getUsernameLastNumbers());
			this.user.setEmailAddress(this.emailAddress);
			this.user.setPhoneNumber(String.valueOf(this.phoneNumber));
			this.user.setClearTextPassword(this.clearTextPassword);
			this.user.setCountry(this.country);
			this.user.setGender(Gender.UNKNOWN);
			this.user.setRecordStatus(RecordStatus.ACTIVE);
			this.user.setActivateStatus(OperationStatus.FALSE.getName());
			this.user.setDeactivateStatus(OperationStatus.TRUE.getName());
			this.user.setEditStatus(OperationStatus.TRUE.getName());
			this.user.setStatus(UserStatus.ACTIVE);

			User savedUser = ApplicationContextProvider.getApplicationContext().getBean(UserService.class)
					.saveUser(user);
			if (savedUser != null) {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
				ec.redirect(ec.getRequestContextPath() + SUCCESS_URL);
			} else {
				UiUtils.showMessageBox("Registration Failed", "Action Failed", RequestContext.getCurrentInstance());
			}
		} catch (Exception e) {
			e.printStackTrace();
			UiUtils.showMessageBox(e.getMessage(), "Action Failed", RequestContext.getCurrentInstance());
		}
	}

	public void resetForm() {
		this.user = new User();
		this.firstName = "";
		this.lastName = "";
		this.phoneNumber = null;
		this.emailAddress = "";
		this.username = "";
		this.clearTextPassword = "";
		this.country = new Country();
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the phoneNumber
	 */
	public BigInteger getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the clearTextPassword
	 */
	public String getClearTextPassword() {
		return clearTextPassword;
	}

	/**
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(BigInteger phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param clearTextPassword
	 *            the clearTextPassword to set
	 */
	public void setClearTextPassword(String clearTextPassword) {
		this.clearTextPassword = clearTextPassword;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the listOfGenders
	 */
	public List<Gender> getListOfGenders() {
		return listOfGenders;
	}

	/**
	 * @return the listOfCountries
	 */
	public List<Country> getListOfCountries() {
		return listOfCountries;
	}

	/**
	 * @param listOfGenders
	 *            the listOfGenders to set
	 */
	public void setListOfGenders(List<Gender> listOfGenders) {
		this.listOfGenders = listOfGenders;
	}

	/**
	 * @param listOfCountries
	 *            the listOfCountries to set
	 */
	public void setListOfCountries(List<Country> listOfCountries) {
		this.listOfCountries = listOfCountries;
	}

}