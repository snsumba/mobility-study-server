package org.sers.study.client.setup.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.sers.study.client.controllers.HyperLinks;
import org.sers.study.client.views.presenters.WebForm;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.Gender;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.constants.UserStatus;
import org.sers.study.model.security.Country;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.CountryService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(name = "userForm", eager = true)
@SessionScoped
public class UserForm extends WebForm<User> {

	private static final long serialVersionUID = 1L;

	private List<Gender> listOfGenders;
	private List<Country> listOfCountries;

	@PostConstruct
	public void init() {
		listOfGenders = new ArrayList<Gender>();
		listOfGenders.addAll(Arrays.asList(Gender.values()));
		listOfCountries = ApplicationContextProvider.getApplicationContext().getBean(CountryService.class)
				.getCountries();
	}

	@Override
	public String getViewUrl() {
		return HyperLinks.USERSVIEW;
	}

	@Override
	public void persist() throws Exception {
		super.model.setRecordStatus(RecordStatus.ACTIVE);
		super.model.setActivateStatus(OperationStatus.FALSE.getName());
		super.model.setDeactivateStatus(OperationStatus.TRUE.getName());
		super.model.setEditStatus(OperationStatus.TRUE.getName());
		super.model.setStatus(UserStatus.ACTIVE);
		ApplicationContextProvider.getApplicationContext().getBean(UserService.class).saveUser(super.model);
	}

	@Override
	public void resetModal() {
		super.resetModal();
		super.model = new User();
	}

	@Override
	public void setFormProperties() {
		super.setFormProperties();
	}

	/**
	 * @return the listOfGenders
	 */
	public List<Gender> getListOfGenders() {
		return listOfGenders;
	}

	/**
	 * @return the listOfCountries
	 */
	public List<Country> getListOfCountries() {
		return listOfCountries;
	}

	/**
	 * @param listOfGenders
	 *            the listOfGenders to set
	 */
	public void setListOfGenders(List<Gender> listOfGenders) {
		this.listOfGenders = listOfGenders;
	}

	/**
	 * @param listOfCountries
	 *            the listOfCountries to set
	 */
	public void setListOfCountries(List<Country> listOfCountries) {
		this.listOfCountries = listOfCountries;
	}

}