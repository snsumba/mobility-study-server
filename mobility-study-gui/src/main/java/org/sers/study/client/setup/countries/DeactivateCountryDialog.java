/**
 * 
 */
package org.sers.study.client.setup.countries;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.sers.study.client.views.presenters.DialogForm;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.security.Country;
import org.sers.study.server.core.service.CountryService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class DeactivateCountryDialog extends DialogForm<Country> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5663587060380874864L;
	private static final String DIALOG_NAME = "DeactivateCountryDialog";

	public DeactivateCountryDialog() {
		super(DIALOG_NAME, 340, 200);

	}

	@Override
	public void persist() throws Exception {
		super.model.setActivateStatus(OperationStatus.TRUE.getName());
		super.model.setEditStatus(OperationStatus.FALSE.getName());
		super.model.setDeactivateStatus(OperationStatus.FALSE.getName());
		super.model.setViewStatus(OperationStatus.FALSE.getName());
		super.model.setRecordStatus(RecordStatus.DELETED);
		ApplicationContextProvider.getBean(CountryService.class).saveCountry(super.model);
		hide();
	}

	@Override
	public void resetModal() {
		super.resetModal();
		super.model = new Country();
	}

}
