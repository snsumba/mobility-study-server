package org.sers.study.client.setup.locations;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.client.views.presenters.PaginatedTable;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.CapturedLocationService;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean
@ViewScoped
public class CapturedLocationsView extends PaginatedTable<CapturedLocation> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8413343075645160075L;
	private CapturedLocationService capturedLocationService;
	private User loggedInUser;

	@PostConstruct
	public void init() {
		loggedInUser = SharedAppData.getLoggedInUser();
		capturedLocationService = ApplicationContextProvider.getApplicationContext()
				.getBean(CapturedLocationService.class);
	}

	@Override
	public void reloadFromDB(int offset, int limit, Map<String, Object> filters) throws Exception {
		try {
			if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR)) {
				super.setTotalRecords(capturedLocationService
						.countCapturedLocations((filters != null && !filters.isEmpty() ? filters : null)));
				super.setDataModels(capturedLocationService.getCapturedLocations(offset, limit,
						(filters != null && !filters.isEmpty() ? filters : null)));
			} else {
				super.setTotalRecords(capturedLocationService.countCapturedLocations(loggedInUser,
						(filters != null && !filters.isEmpty() ? filters : null)));
				super.setDataModels(capturedLocationService.getCapturedLocations(loggedInUser, offset, limit,
						(filters != null && !filters.isEmpty() ? filters : null)));
			}
		} catch (Exception e) {
			UiUtils.showErrorMessage("Error: " + e.getMessage(), RequestContext.getCurrentInstance());
		}
	}
}
