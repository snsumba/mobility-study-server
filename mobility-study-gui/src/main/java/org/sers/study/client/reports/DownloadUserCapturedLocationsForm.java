
/**
 *Jul 5, 2017
 *org.sers.study.client.reports
 *mobility-study-gui
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.client.reports;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.StreamedContent;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.utils.ExcellFileCreator;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.exception.OperationFailedException;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.CapturedLocationService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class DownloadUserCapturedLocationsForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private StreamedContent file;
	private User loggedInUser;
	private User selectedUser = new User();
	private List<User> listOfUsers;
	private Date startDate = new Date();
	private Date endDate = new Date();

	public DownloadUserCapturedLocationsForm() {
		loggedInUser = SharedAppData.getLoggedInUser();
		try {
			listOfUsers = ApplicationContextProvider.getBean(UserService.class).getUsers();
		} catch (OperationFailedException e) {
			e.printStackTrace();
		}
	}

	public StreamedContent getFile() {
		try {
			if (UiUtils.isRangeAcceptable(startDate, endDate, UiUtils.MAXIMUM_NUMBER_OF_REPORT_DAYS)) {
				if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR)) {
					List<CapturedLocation> capturedLocations = ApplicationContextProvider
							.getBean(CapturedLocationService.class)
							.getCapturedLocations(selectedUser, startDate, endDate);
					System.out.println("capturedLocations : " + capturedLocations.size());
					file = new ExcellFileCreator().getCapturedLocationsExcellDoc(capturedLocations, selectedUser);
				}
				// resetModal();
				return file;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void resetModal() {
		this.selectedUser = new User();
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the loggedInUser
	 */
	public User getLoggedInUser() {
		return loggedInUser;
	}

	/**
	 * @return the selectedUser
	 */
	public User getSelectedUser() {
		return selectedUser;
	}

	/**
	 * @return the listOfUsers
	 */
	public List<User> getListOfUsers() {
		return listOfUsers;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(StreamedContent file) {
		this.file = file;
	}

	/**
	 * @param loggedInUser
	 *            the loggedInUser to set
	 */
	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	/**
	 * @param selectedUser
	 *            the selectedUser to set
	 */
	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	/**
	 * @param listOfUsers
	 *            the listOfUsers to set
	 */
	public void setListOfUsers(List<User> listOfUsers) {
		this.listOfUsers = listOfUsers;
	}

}
