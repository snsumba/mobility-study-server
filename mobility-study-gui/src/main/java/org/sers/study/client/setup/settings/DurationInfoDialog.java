/**
 * 
 */
package org.sers.study.client.setup.settings;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.views.presenters.DialogForm;
import org.sers.study.model.DurationInfo;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.server.core.service.DurationInfoService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class DurationInfoDialog extends DialogForm<DurationInfo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5663587060380874864L;
	private static final String DIALOG_NAME = "DurationInfoDialog";

	public DurationInfoDialog() {
		super(DIALOG_NAME, 340, 200);

	}

	@Override
	public void persist() throws Exception {
		super.model.setActivateStatus(OperationStatus.FALSE.getName());
		super.model.setEditStatus(OperationStatus.TRUE.getName());
		super.model.setDeactivateStatus(OperationStatus.TRUE.getName());
		super.model.setViewStatus(OperationStatus.FALSE.getName());
		super.model.setRecordStatus(RecordStatus.ACTIVE);
		
		ApplicationContextProvider.getBean(DurationInfoService.class).saveDurationInfo(super.model);
	}

	@Override
	public void resetModal() {
		super.resetModal();
		super.model = new DurationInfo();
	}

}
