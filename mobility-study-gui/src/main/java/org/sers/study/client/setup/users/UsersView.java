package org.sers.study.client.setup.users;

import java.io.IOException;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;
import org.sers.study.client.controllers.HyperLinks;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.client.views.presenters.PaginatedTable;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.exception.SessionExpiredException;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean
@SessionScoped
public class UsersView extends PaginatedTable<User> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserService userService;

	@PostConstruct
	public void init() {
		try {
			super.enforceSecurity(HyperLinks.USERSVIEW, true, new String[] { PermissionConstants.PERM_ADMINISTRATOR });
			userService = ApplicationContextProvider.getApplicationContext().getBean(UserService.class);
		} catch (SessionExpiredException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void reloadFromDB(int offset, int limit, Map<String, Object> filters) throws Exception {
		try {
			super.setTotalRecords(userService.countUsers((filters != null && !filters.isEmpty() ? filters : null)));
			super.setDataModels(
					userService.getUsers(offset, limit, (filters != null && !filters.isEmpty() ? filters : null)));
		} catch (Exception e) {
			UiUtils.showErrorMessage("Error: " + e.getMessage(), RequestContext.getCurrentInstance());
		}
	}
}