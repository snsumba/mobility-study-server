package org.sers.study.client.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.MethodType;

@AuthorComment
@FacesConverter("methodTypeConverter")
public class MethodTypeConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {

		if (arg2.equalsIgnoreCase(MethodType.GPS_DETERMINED.getName())) {
			return MethodType.GPS_DETERMINED;
		}

		if (arg2.equalsIgnoreCase(MethodType.MANUAL_INPUT.getName())) {
			return MethodType.MANUAL_INPUT;
		}

		if (arg2.equalsIgnoreCase(MethodType.UNKNOWN.getName())) {
			return MethodType.UNKNOWN;
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object methodType) {
		if (methodType != null)
			return ((MethodType) methodType).getName();
		return "--Select MethodType--";
	}
}
