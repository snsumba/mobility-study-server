package org.sers.study.client.views.presenters;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
public interface WebViewResolver {

	/**
	 * @return the viewUrl
	 */
	String getViewUrl();
}
