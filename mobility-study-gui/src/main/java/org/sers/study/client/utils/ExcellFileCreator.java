package org.sers.study.client.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.TimeDiary;
import org.sers.study.model.security.User;

public class ExcellFileCreator {
	private boolean display = Boolean.FALSE;

	private XSSFCellStyle applyStyle(XSSFWorkbook workbook, short hssfColor, HorizontalAlignment horizontalAlignment,
			short indexedColors, boolean bold) {
		XSSFFont font = workbook.createFont();
		font.setFontName("Times New Romans");
		font.setColor(hssfColor);
		font.setBold(bold);

		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setAlignment(horizontalAlignment);
		style.setLocked(true);
		style.setWrapText(true);
		return style;
	}

	static XSSFCellStyle shadeAlt(XSSFWorkbook workbook) {
		XSSFCellStyle cellStyle1 = workbook.createCellStyle();
		cellStyle1.setFillForegroundColor(HSSFColor.TAN.index);
		cellStyle1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		return cellStyle1;
	}

	private void addImageToExcellFile(XSSFWorkbook workbook, XSSFSheet spreadsheet, int columnNumber) {
		if (display) {
			try {
				// FileInputStream obtains input bytes from the image file
				InputStream inputStream = new InputStream() {

					@Override
					public int read() throws IOException {
						return 0;
					}
				};

				// Get the contents of an InputStream as a byte[].
				byte[] bytes = IOUtils.toByteArray(inputStream);
				// Adds a picture to the workbook
				int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
				// close the input stream
				inputStream.close();
				// Returns an object that handles instantiating concrete classes
				CreationHelper helper = workbook.getCreationHelper();
				// Creates the top-level drawing patriarch.
				Drawing drawing = spreadsheet.createDrawingPatriarch();
				// Create an anchor that is attached to the worksheet
				ClientAnchor anchor = helper.createClientAnchor();
				// set top-left corner for the image

				// anchor.setCol1(new Float((columnNumber *
				// 0.6666666666666666666666666666)).intValue());
				anchor.setCol1(4);
				anchor.setRow1(1); // Row 2
				anchor.setCol2(5); // Column G
				anchor.setRow2(3); // Row 4
				// Creates a picture
				Picture picture = drawing.createPicture(anchor, pictureIdx);
				// Reset the image to the original size
				picture.resize();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private StreamedContent saveAndApplySecurityPin(XSSFWorkbook workbook, String securityPin, String fileName) {
		ByteArrayOutputStream baos;
		ByteArrayOutputStream finalOutputStream;
		boolean securityPinEnabled = Boolean.FALSE;

		try {
			// The code below saves the Workbook object created earlier to an
			// Excel file on your file system
			baos = new ByteArrayOutputStream();
			workbook.write(baos);
			baos.close();
			finalOutputStream = baos;
			if (securityPinEnabled) {

			}
			return new DefaultStreamedContent(new ByteArrayInputStream(finalOutputStream.toByteArray()),
					"application/vnd.openxmlformats-officedocument.spreadsheetml.sharedstrings+xml application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
					fileName);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;

	}

	private void fileHeader(String reportTitle, XSSFWorkbook workbook, XSSFSheet spreadsheet, int maxColumnNo) {
		XSSFCell cell = null;

		XSSFRow reportTitleRow = spreadsheet.createRow(1);
		cell = reportTitleRow.createCell(0);
		cell.setCellValue(reportTitle);
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.CENTER,
				IndexedColors.WHITE.index, Boolean.FALSE));
		cell = reportTitleRow.createCell(6);

		XSSFRow spaceRow = spreadsheet.createRow(2);
		cell = spaceRow.createCell(0);
		cell = spaceRow.createCell(6);

		/**
		 * first row (0-based), last row (0-based),first column (0-based),last
		 * column (0-based)
		 */
		spreadsheet.addMergedRegion(new CellRangeAddress(0, 0, 0, maxColumnNo));
		spreadsheet.addMergedRegion(new CellRangeAddress(1, 1, 0, maxColumnNo));
		spreadsheet.addMergedRegion(new CellRangeAddress(2, 2, 0, maxColumnNo));

	}

	public StreamedContent getCapturedLocationsExcellDoc(List<CapturedLocation> capturedLocations, User user) {
		String fileName = user.getFullName() + " capture_locations" + System.currentTimeMillis()
				+ FileExtensions.XLSX_FILE_EXTEMSION;
		XSSFWorkbook workbook = new XSSFWorkbook();

		XSSFSheet spreadsheet = workbook.createSheet("Captured locations");

		spreadsheet.setColumnWidth(0, 1400);
		spreadsheet.setColumnWidth(1, 6000);
		spreadsheet.setColumnWidth(2, 11000);
		spreadsheet.setColumnWidth(3, 11000);
		spreadsheet.setColumnWidth(4, 7000);
		spreadsheet.setColumnWidth(5, 7000);
		spreadsheet.setColumnWidth(6, 7000);
		spreadsheet.setColumnWidth(7, 7000);

		// spreadsheet.protectSheet(null);
		// spreadsheet.enableLocking();

		addImageToExcellFile(workbook, spreadsheet, 9);
		fileHeader("Captured locations for user : " + user.getFullName(), workbook, spreadsheet, 9);

		XSSFCell cell;
		XSSFRow row = spreadsheet.createRow(4);

		cell = row.createCell(0);
		cell.setCellValue("No.".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(1);
		cell.setCellValue("Date".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(2);
		cell.setCellValue("User".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(3);
		cell.setCellValue("Place name".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(4);
		cell.setCellValue("Latitude".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(5);
		cell.setCellValue("Longitude".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(6);
		cell.setCellValue("Accuracy".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(7);
		cell.setCellValue("Method Type".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		int i = 5;
		int counter = 1;

		for (CapturedLocation txn : capturedLocations) {
			row = spreadsheet.createRow(i);
			cell = row.createCell(0);
			cell.setCellValue(((Integer) (counter++)).toString());
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.CENTER,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(1);
			cell.setCellValue((txn.getAppRegistrationDate() != null ? txn.getAppRegistrationDate()
					: String.valueOf(txn.getDateChanged())));
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(2);
			cell.setCellValue((txn.getUser() != null ? txn.getUser().getFullName() : "n.a"));
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(3);
			cell.setCellValue((txn.getPlaceName() != null ? txn.getPlaceName() : "n.a"));
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(4);
			cell.setCellValue(txn.getLatitude());
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(5);
			cell.setCellValue(txn.getLongitude());
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(6);
			cell.setCellValue(txn.getAccuracy());
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.CENTER,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(7);
			cell.setCellValue(txn.getMethodType().getName());
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			i++;
		}
		return saveAndApplySecurityPin(workbook, null, fileName);
	}

	public StreamedContent getTimeDiariesExcellDoc(List<TimeDiary> timeDiaries, User user) {
		String fileName = user.getFullName() + " time_diary" + System.currentTimeMillis()
				+ FileExtensions.XLSX_FILE_EXTEMSION;
		XSSFWorkbook workbook = new XSSFWorkbook();

		XSSFSheet spreadsheet = workbook.createSheet("Time diary");

		spreadsheet.setColumnWidth(0, 1400);
		spreadsheet.setColumnWidth(1, 6000);
		spreadsheet.setColumnWidth(2, 6000);
		spreadsheet.setColumnWidth(3, 6000);
		spreadsheet.setColumnWidth(4, 12000);
		spreadsheet.setColumnWidth(5, 12000);
		
		addImageToExcellFile(workbook, spreadsheet, 9);
		fileHeader("Time Diary for user : " + user.getFullName(), workbook, spreadsheet, 9);

		XSSFCell cell;
		XSSFRow row = spreadsheet.createRow(4);

		cell = row.createCell(0);
		cell.setCellValue("No.".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(1);
		cell.setCellValue("Date".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));
		
		cell = row.createCell(2);
		cell.setCellValue("Start Time".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(3);
		cell.setCellValue("End Time".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(4);
		cell.setCellValue("User".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		cell = row.createCell(5);
		cell.setCellValue("Activity".toUpperCase());
		cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
				IndexedColors.WHITE.index, Boolean.TRUE));

		int i = 5;
		int counter = 1;

		for (TimeDiary txn : timeDiaries) {
			row = spreadsheet.createRow(i);
			cell = row.createCell(0);
			cell.setCellValue(((Integer) (counter++)).toString());
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.CENTER,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(1);
			cell.setCellValue((txn.getRegistrationDate() != null ? txn.getRegistrationDate()
					: String.valueOf(txn.getDateChanged())));
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(2);
			cell.setCellValue((txn.getStartTime() != null ? txn.getStartTime() : String.valueOf("Not Provided")));
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(3);
			cell.setCellValue((txn.getEndTime() != null ? txn.getEndTime() : String.valueOf("Not Provided")));
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			
			cell = row.createCell(4);
			cell.setCellValue((txn.getUser() != null ? txn.getUser().getFullName() : "n.a"));
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));

			cell = row.createCell(5);
			cell.setCellValue(
					(txn.getCategorySuggestion() != null ? txn.getCategorySuggestion().getSuggestion() : "n.a"));
			cell.setCellStyle(applyStyle(workbook, HSSFColor.BLACK.index, HorizontalAlignment.LEFT,
					IndexedColors.WHITE.index, Boolean.FALSE));
			
			i++;
		}
		return saveAndApplySecurityPin(workbook, null, fileName);
	}

}
