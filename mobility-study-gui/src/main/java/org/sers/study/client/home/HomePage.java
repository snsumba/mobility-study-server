package org.sers.study.client.home;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean
@RequestScoped
public class HomePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String loggedinUser;
	private String logoutUrl = "ServiceLogout";
	private int pendingTransactions = 0;
	

	@PostConstruct
	public void init() {
		if (SharedAppData.getLoggedInUser() != null) {
			loggedinUser = SharedAppData.getLoggedInUser().getFullName();
		} else
			loggedinUser = "Hacker. Your gonna have a very bad experience. Remember, what we do in reality echoes in eternity!";
	}

	/**
	 * @return the loggedinUser
	 */
	public String getLoggedinUser() {
		return loggedinUser;
	}

	/**
	 * @return the pendingTransactions
	 */
	public int getPendingTransactions() {
		return pendingTransactions;
	}

	/**
	 * @param pendingTransactions the pendingTransactions to set
	 */
	public void setPendingTransactions(int pendingTransactions) {
		this.pendingTransactions = pendingTransactions;
	}

	/**
	 * @param loggedinUser
	 *            the loggedinUser to set
	 */
	public void setLoggedinUser(String loggedinUser) {
		this.loggedinUser = loggedinUser;
	}

	/**
	 * @return the logoutUrl
	 */
	public String getLogoutUrl() {
		return logoutUrl;
	}

	/**
	 * @param logoutUrl
	 *            the logoutUrl to set
	 */
	public void setLogoutUrl(String logoutUrl) {
		this.logoutUrl = logoutUrl;
	}
}
