package org.sers.study.client.setup.timediary;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.client.views.presenters.PaginatedTable;
import org.sers.study.model.TimeDiary;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.TimeDiaryService;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean
@ViewScoped
public class TimeDiariesView extends PaginatedTable<TimeDiary> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8413343075645160075L;
	private TimeDiaryService timeDiaryService;
	private User loggedInUser;

	@PostConstruct
	public void init() {
		loggedInUser = SharedAppData.getLoggedInUser();
		timeDiaryService = ApplicationContextProvider.getApplicationContext().getBean(TimeDiaryService.class);
	}

	@Override
	public void reloadFromDB(int offset, int limit, Map<String, Object> filters) throws Exception {
		try {
			if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR)) {
				super.setTotalRecords(
						timeDiaryService.countTimeDiaries((filters != null && !filters.isEmpty() ? filters : null)));
				super.setDataModels(timeDiaryService.getTimeDiaries(offset, limit,
						(filters != null && !filters.isEmpty() ? filters : null)));
			} else {
				super.setTotalRecords(timeDiaryService.countTimeDiaries(loggedInUser,
						(filters != null && !filters.isEmpty() ? filters : null)));
				super.setDataModels(timeDiaryService.getTimeDiaries(loggedInUser, offset, limit,
						(filters != null && !filters.isEmpty() ? filters : null)));
			}
		} catch (Exception e) {
			UiUtils.showErrorMessage("Error: " + e.getMessage(), RequestContext.getCurrentInstance());
		}
	}

	/**
	 * @return the timeDiaryService
	 */
	public TimeDiaryService getTimeDiaryService() {
		return timeDiaryService;
	}

	/**
	 * @param timeDiaryService
	 *            the timeDiaryService to set
	 */
	public void setTimeDiaryService(TimeDiaryService timeDiaryService) {
		this.timeDiaryService = timeDiaryService;
	}
}
