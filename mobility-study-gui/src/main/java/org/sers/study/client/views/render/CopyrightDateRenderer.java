package org.sers.study.client.views.render;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.server.core.service.impl.DateUtils;

@AuthorComment
@ManagedBean
@RequestScoped
public class CopyrightDateRenderer implements Serializable {

	private static final long serialVersionUID = -4697780490043450138L;
	private String copyright;

	public CopyrightDateRenderer() {
		copyright = String.valueOf(DateUtils.currentYear());
	}

	/**
	 * @return the copyright
	 */
	public String getCopyright() {
		return copyright;
	}

	/**
	 * @param copyright
	 *            the copyright to set
	 */
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

}
