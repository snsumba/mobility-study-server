package org.sers.study.client.views.render;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.model.exception.SessionExpiredException;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean
@RequestScoped
public class ComponentRenderer implements Serializable {

	private static final long serialVersionUID = 9025578123139327306L;
	private boolean renderDashEditUserButton = false;
	private boolean administrator = false;
	private String maximumReportDays = "You can download a report of 1 day or select a maximum of "
			+ UiUtils.MAXIMUM_NUMBER_OF_REPORT_DAYS + " days";
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the administrator
	 * @throws SessionExpiredException
	 */
	public boolean isAdministrator() throws SessionExpiredException {
		User loggedInUser = SharedAppData.getLoggedInUser();
		if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR))
			administrator = true;
		return administrator;
	}

	/**
	 * @return the maximumReportDays
	 */
	public String getMaximumReportDays() {
		return maximumReportDays;
	}

	/**
	 * @param renderDashEditUserButton the renderDashEditUserButton to set
	 */
	public void setRenderDashEditUserButton(boolean renderDashEditUserButton) {
		this.renderDashEditUserButton = renderDashEditUserButton;
	}

	/**
	 * @param administrator the administrator to set
	 */
	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	/**
	 * @param maximumReportDays the maximumReportDays to set
	 */
	public void setMaximumReportDays(String maximumReportDays) {
		this.maximumReportDays = maximumReportDays;
	}

	/**
	 * @return the renderDashEditUserButton
	 * @throws SessionExpiredException
	 */
	public boolean isRenderDashEditUserButton() throws SessionExpiredException {
		User user = SharedAppData.getLoggedInUser();
		if (user.getUsername().equals(User.DEFAULT_ADMIN))
			renderDashEditUserButton = false;
		else
			renderDashEditUserButton = true;
		return renderDashEditUserButton;
	}

}