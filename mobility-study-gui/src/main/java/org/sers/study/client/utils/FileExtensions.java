package org.sers.study.client.utils;

import java.io.Serializable;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
public class FileExtensions implements Serializable {

	private static final long serialVersionUID = 2906867537824174686L;

	public static final String XML_FILE_EXTEMSION = ".xml";
	public static final String PDF_FILE_EXTEMSION = ".pdf";
	public static final String PNG_FILE_EXTEMSION = ".png";
	public static final String XLSX_FILE_EXTEMSION = ".xlsx";
	public static final String CSV_FILE_EXTEMSION = ".csv";
}
