/**
 * 
 */
package org.sers.study.client.setup.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.views.presenters.DialogForm;
import org.sers.study.model.AppType;
import org.sers.study.model.constants.MethodType;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.server.core.service.AppTypeService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class AppTypeDialog extends DialogForm<AppType> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5663587060380874864L;
	private static final String DIALOG_NAME = "AppTypeDialog";

	private List<OperationStatus> listOfOperationStatus;
	private List<MethodType> listOfMethodTypes;

	public AppTypeDialog() {
		super(DIALOG_NAME, 400, 340);
		listOfOperationStatus = new ArrayList<OperationStatus>();
		listOfOperationStatus.addAll(Arrays.asList(OperationStatus.values()));

		listOfMethodTypes = new ArrayList<MethodType>();
		listOfMethodTypes.addAll(Arrays.asList(MethodType.values()));
	}

	@Override
	public void persist() throws Exception {
		super.model.setActivateStatus(OperationStatus.FALSE.getName());
		super.model.setEditStatus(OperationStatus.TRUE.getName());
		super.model.setDeactivateStatus(OperationStatus.TRUE.getName());
		super.model.setViewStatus(OperationStatus.FALSE.getName());
		super.model.setRecordStatus(RecordStatus.ACTIVE);

		ApplicationContextProvider.getBean(AppTypeService.class).saveAppType(super.model);
	}

	@Override
	public void resetModal() {
		super.resetModal();
		super.model = new AppType();
	}

	/**
	 * @return the listOfOperationStatus
	 */
	public List<OperationStatus> getListOfOperationStatus() {
		return listOfOperationStatus;
	}

	/**
	 * @return the listOfMethodTypes
	 */
	public List<MethodType> getListOfMethodTypes() {
		return listOfMethodTypes;
	}

	/**
	 * @param listOfOperationStatus
	 *            the listOfOperationStatus to set
	 */
	public void setListOfOperationStatus(List<OperationStatus> listOfOperationStatus) {
		this.listOfOperationStatus = listOfOperationStatus;
	}

	/**
	 * @param listOfMethodTypes
	 *            the listOfMethodTypes to set
	 */
	public void setListOfMethodTypes(List<MethodType> listOfMethodTypes) {
		this.listOfMethodTypes = listOfMethodTypes;
	}

}
