package org.sers.study.client.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import javax.faces.application.FacesMessage;

import org.primefaces.context.RequestContext;
import org.sers.study.annotations.AuthorComment;

@AuthorComment
public class UiUtils {

	public static final String MSG_TITLE = "Mobility Study";
	public static final int MAX_RESULTS_PER_PAGE = 10;
	public static final int SEARCH_FIELD_SIZE = 30;
	public static final String SEARCH_PLACE_HOLDER = "Search all fields";
	public static final String DATA_EMPTY_MESSAGE = "No records found with given criteria";
	public static final String GLOBAL_FILTER_ID = "globalFilter";
	public static final String INPUT_TEXT_PERCENTAGE_SIZE = "width:100%";
	public static final String INPUT_TEXT_PERCENTAGE_SIZE_FORM = "width:90%";
	public static final String SELECT_MANY_PANEL_STYLE_WIDTH = "width:90%";
	public static final String FORM_SELECT_MANY_PANEL_STYLE_WIDTH = "width:30%";
	public static final int MAXIMUM_NUMBER_OF_REPORT_DAYS = 7;
	public static final String SEARCH_BUTTON_COLOR = "cyan-btn flat";
	public static final String DOWNLOAD_BUTTON_COLOR = "blue-grey-btn flat";
	public static final String OPERATION_BUTTON_COLOR = "green-btn flat";
	public static final String TOGGLER_BUTTON_COLOR = "pink-btn flat";
	public static final String FORM_BUTTON_COLOR = "cyan-btn flat";
	public static final String FORM_ICON = "fa fa-check";
	public static final String TOGGLER_ICON = "fa fa-th";
	public static final String REPORT_BUTTON_TEXT = "Report";
	public static final String PERCENTAGE_SIGN = "%";
	public static final String UGANDA_SHILLINGS_SIGN = "/=";
	public static final String PAGINATION_PAGE_VIEW = "top";
	public static final String AUTHOR = "Sers (U) Ltd";
	public static final String FILTER_MODE = "contains";
	public static final String AUTHOR_EMAIL = "umarbravo70@gmail.com / kyeyunedeo@gmail.com";
	public static final String AUTHOR_CONTACT = "+256703819876 / +256774669561";

	/**
	 * 
	 * @param errorMessage
	 * @param requestContext
	 * @throws Exception
	 */
	public static void showErrorMessage(String errorMessage, RequestContext requestContext) throws Exception {
		if (requestContext == null) {
			System.out.println("Request context is null");
		} else {
			System.out.println("Request context is never null");
		}
		requestContext
				.showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Action Failed", errorMessage));
	}

	/**
	 * 
	 * @param message
	 * @param title
	 * @param requestContext
	 * @throws Exception
	 */
	public static void showMessageBox(String message, String title, RequestContext requestContext) {
		requestContext.showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, title, message));
	}

	public static void showMessageBox(String message, RequestContext requestContext) {
		requestContext.showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, UiUtils.MSG_TITLE, message));
	}

	public static void showSuccessMessageBox(String message, RequestContext requestContext) throws Exception {
		requestContext.showMessageInDialog(new FacesMessage(message));
	}

	public static Date getFirstDateOfThisMonth() {
		String[] dateTokens = new SimpleDateFormat("dd-MM-yyyy").format(new Date()).split("-");
		return new GregorianCalendar(Integer.parseInt(dateTokens[2]), Integer.parseInt(dateTokens[1]) - 1, 01)
				.getTime();
	}

	public static Date getLastDateOfThisMonth() {
		String[] dateTokens = new SimpleDateFormat("dd-MM-yyyy").format(new Date()).split("-");
		return new GregorianCalendar(Integer.parseInt(dateTokens[2]), Integer.parseInt(dateTokens[1]) - 1, 31)
				.getTime();
	}

	public static String formatProvidedDate(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return format.format(date);
	}
	
	public static boolean isRangeAcceptable(Date startDate, Date endDate, int range) {
		return UiUtils.isRangeAcceptable(UiUtils.getReportDateFormat(startDate), UiUtils.getReportDateFormat(endDate),
				range);
	}

	public static String getReportDateFormat(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String reportDate = dateFormat.format(date);
		System.out.println("reportDate      : " + reportDate);
		return reportDate;
	}

	private static boolean isRangeAcceptable(String providedStartDate, String providedEndDate, int range) {
		boolean rangeAcceptable = Boolean.FALSE;
		long daysDifference = 0;

		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date startDate = dateFormat.parse(providedStartDate);
			Date endDate = dateFormat.parse(providedEndDate);

			String newStartDate = dateFormat.format(startDate);
			String newEndDate = dateFormat.format(endDate);
			System.out.println("newStartDate    : " + newStartDate);
			System.out.println("newEndDate      : " + newEndDate);

			long diff = endDate.getTime() - startDate.getTime();
			daysDifference = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

			if (daysDifference <= range)
				rangeAcceptable = true;
			else
				rangeAcceptable = false;
		} catch (Exception e) {
			e.printStackTrace();
			rangeAcceptable = false;
		}
		System.out.println("Range           : " + range);
		System.out.println("Days            : " + daysDifference);
		System.out.println("status          : " + rangeAcceptable);
		return rangeAcceptable;

	}
}
