package org.sers.study.client.setup.users;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.sers.study.client.controllers.HyperLinks;
import org.sers.study.client.utils.UiUtils;
import org.springframework.beans.BeansException;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.Gender;
import org.sers.study.model.exception.OperationFailedException;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Country;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.CountryService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean(eager = true)
@RequestScoped
public class ProfileView implements Serializable {

	private static final long serialVersionUID = 1L;
	private User user;
	private List<Country> listOfCountries;
	private List<Gender> listOfGenders;

	@PostConstruct
	private void init() {
		this.user = SharedAppData.getLoggedInUser();
		listOfGenders = new ArrayList<Gender>();
		listOfGenders.addAll(Arrays.asList(Gender.values()));
		listOfCountries = ApplicationContextProvider.getApplicationContext().getBean(CountryService.class)
				.getCountries();
	}

	public ProfileView() {
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	public void saveUser() throws Exception {
		if (this.user == null)
			this.user = new User();
		try {
			User savedUser = ApplicationContextProvider.getBean(UserService.class).saveUser(this.user);
			if (savedUser != null) {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
				ec.redirect(ec.getRequestContextPath() + HyperLinks.HOMEPAGE);
			}

		} catch (ValidationFailedException exception) {
			UiUtils.showErrorMessage("Details not updated: " + exception.getMessage(),
					RequestContext.getCurrentInstance());
		}
	}

	public void deleteUser(User user) throws BeansException, OperationFailedException {
		ApplicationContextProvider.getApplicationContext().getBean(UserService.class).deleteUser(user);
	}

	/**
	 * @return the listOfCountries
	 */
	public List<Country> getListOfCountries() {
		return listOfCountries;
	}

	/**
	 * @return the listOfGenders
	 */
	public List<Gender> getListOfGenders() {
		return listOfGenders;
	}

	/**
	 * @param listOfCountries
	 *            the listOfCountries to set
	 */
	public void setListOfCountries(List<Country> listOfCountries) {
		this.listOfCountries = listOfCountries;
	}

	/**
	 * @param listOfGenders
	 *            the listOfGenders to set
	 */
	public void setListOfGenders(List<Gender> listOfGenders) {
		this.listOfGenders = listOfGenders;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
}