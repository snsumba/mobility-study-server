package org.sers.study.client.controller.upload;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.primefaces.component.fileupload.FileUploadRenderer;
import org.sers.study.annotations.AuthorComment;

/**
 * @see http://stackoverflow.com/questions/19262356/file-upload-doesnt-work-with-ajax-in-primefaces-4-0-jsf-2-2-x-javax-servlet-s
 * 
 */
@AuthorComment
public class CustomFileUploadHandler  extends FileUploadRenderer {

	@Override
	public void decode(FacesContext context, UIComponent component) {
		if (context.getExternalContext().getRequestContentType().toLowerCase()
				.startsWith("multipart/")) {
			super.decode(context, component);
		}
	}
}
