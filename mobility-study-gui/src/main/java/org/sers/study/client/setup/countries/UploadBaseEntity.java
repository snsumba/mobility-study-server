package org.sers.study.client.setup.countries;

import org.apache.poi.ss.usermodel.Cell;
import org.primefaces.context.RequestContext;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.client.views.presenters.WebForm;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.security.Country;

@AuthorComment
public abstract class UploadBaseEntity extends WebForm<Country> {

	public void validateCell(int desiredCellType, Cell cell, String errorMessage, boolean allowBlank,
			Object defaultValue) throws Exception {
		if (!allowBlank) {
			if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
				UiUtils.showErrorMessage(
						"Cell on Row: " + (cell.getRow().getRowNum() + 1) + " and Column: "
								+ getCellLetter(cell.getColumnIndex()) + " cannot be empty",
						RequestContext.getCurrentInstance());

			if (((cell.getCellType() == Cell.CELL_TYPE_STRING)))
				try {
					if (cell.getStringCellValue().isEmpty()) {
						UiUtils.showErrorMessage(
								"Cell on Row: " + (cell.getRow().getRowNum() + 1) + " and Column: "
										+ getCellLetter(cell.getColumnIndex()) + " cannot be empty",
								RequestContext.getCurrentInstance());
					}
				} catch (Exception exe) {
					if (cell.getStringCellValue().isEmpty()) {
						UiUtils.showErrorMessage(
								"Cell on Row: " + (cell.getRow().getRowNum() + 1) + " and Column: "
										+ getCellLetter(cell.getColumnIndex()) + " requires string values only",
								RequestContext.getCurrentInstance());
					}
				}
			return;
		} else if (allowBlank && (cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
			if (Cell.CELL_TYPE_NUMERIC == desiredCellType) {
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				cell.setCellValue(new Integer((defaultValue.toString())));
			} else {
				cell.setCellValue(defaultValue.toString());
			}
			return;
		} else if (allowBlank && (cell.getCellType() == Cell.CELL_TYPE_STRING) && cell.getStringCellValue().isEmpty()) {
			if (Cell.CELL_TYPE_NUMERIC == desiredCellType) {
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
				cell.setCellValue(new Integer((defaultValue.toString())));
			} else {
				cell.setCellValue(defaultValue.toString());
			}
			return;
		}

		if (desiredCellType == -1)
			return;

		if ((cell.getCellType() != Cell.CELL_TYPE_FORMULA) && (cell.getCellType() != desiredCellType)) {
			UiUtils.showErrorMessage(
					"Cell on Row: " + (cell.getRow().getRowNum() + 1) + " and Column: "
							+ getCellLetter(cell.getColumnIndex()) + " " + errorMessage,
					RequestContext.getCurrentInstance());
		}

		if (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().trim().isEmpty()) {
			UiUtils.showErrorMessage(
					"Cell on Row: " + (cell.getRow().getRowNum() + 1) + " and Column: "
							+ getCellLetter(cell.getColumnIndex()) + " cannot be empty",
					RequestContext.getCurrentInstance());
		}
	}

	public String getValueFromCell(Cell cell) {
		String temp_value = "";
		if (cell != null) {
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_NUMERIC:
				temp_value = "" + cell.getNumericCellValue();
				return temp_value;
			case Cell.CELL_TYPE_STRING:
				temp_value = "" + cell.getStringCellValue();
				return temp_value;
			}
			return null;
		} else
			return null;

	}

	private String getCellLetter(int cellIndex) {
		if (cellIndex == 0)
			return "A";

		if (cellIndex == 1)
			return "B";

		if (cellIndex == 2)
			return "C";

		if (cellIndex == 3)
			return "D";

		if (cellIndex == 4)
			return "E";

		if (cellIndex == 5)
			return "F";

		if (cellIndex == 6)
			return "G";

		if (cellIndex == 7)
			return "H";

		if (cellIndex == 8)
			return "I";

		if (cellIndex == 9)
			return "J";

		if (cellIndex == 10)
			return "K";

		if (cellIndex == 11)
			return "L";

		if (cellIndex == 12)
			return "M";

		return cellIndex + 1 + "";
	}
}
