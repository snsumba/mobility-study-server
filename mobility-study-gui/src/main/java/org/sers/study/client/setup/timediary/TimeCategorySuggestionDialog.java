/**
 * 
 */
package org.sers.study.client.setup.timediary;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.views.presenters.DialogForm;
import org.sers.study.model.TimeCategorySuggestion;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.server.core.service.TimeCategorySuggestionService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class TimeCategorySuggestionDialog extends DialogForm<TimeCategorySuggestion> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5663587060380874864L;
	private static final String DIALOG_NAME = "TimeCategorySuggestionDialog";
	
	public TimeCategorySuggestionDialog() {
		super(DIALOG_NAME, 400, 240);
	}

	@Override
	public void persist() throws Exception {
		super.model.setActivateStatus(OperationStatus.FALSE.getName());
		super.model.setEditStatus(OperationStatus.TRUE.getName());
		super.model.setDeactivateStatus(OperationStatus.TRUE.getName());
		super.model.setViewStatus(OperationStatus.FALSE.getName());
		super.model.setRecordStatus(RecordStatus.ACTIVE);
		ApplicationContextProvider.getBean(TimeCategorySuggestionService.class).saveTimeCategorySuggestion(super.model);
	}

	@Override
	public void resetModal() {
		super.resetModal();
		super.model = new TimeCategorySuggestion();
	}

}
