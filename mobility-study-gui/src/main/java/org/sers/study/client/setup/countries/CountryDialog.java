/**
 * 
 */
package org.sers.study.client.setup.countries;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.sers.study.client.views.presenters.DialogForm;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.security.Country;
import org.sers.study.server.core.service.CountryService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class CountryDialog extends DialogForm<Country> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5663587060380874864L;
	private static final String DIALOG_NAME = "CountryDialog";

	public CountryDialog() {
		super(DIALOG_NAME, 340, 200);

	}

	@Override
	public void persist() throws Exception {
		super.model.setActivateStatus(OperationStatus.FALSE.getName());
		super.model.setEditStatus(OperationStatus.TRUE.getName());
		super.model.setDeactivateStatus(OperationStatus.TRUE.getName());
		super.model.setViewStatus(OperationStatus.FALSE.getName());
		super.model.setRecordStatus(RecordStatus.ACTIVE);
		
		ApplicationContextProvider.getBean(CountryService.class).saveCountry(super.model);
	}

	@Override
	public void resetModal() {
		super.resetModal();
		super.model = new Country();
	}

}
