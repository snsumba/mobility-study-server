package org.sers.study.client.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.OperationStatus;

@AuthorComment
@FacesConverter("operationStatusConverter")
public class OperationStatusConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {

		if (arg2.equalsIgnoreCase(OperationStatus.TRUE.getName())) {
			return OperationStatus.TRUE;
		}

		if (arg2.equalsIgnoreCase(OperationStatus.FALSE.getName())) {
			return OperationStatus.FALSE;
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object operationStatus) {
		if (operationStatus != null)
			return ((OperationStatus) operationStatus).getName();
		return "--Select OperationStatus--";
	}
}
