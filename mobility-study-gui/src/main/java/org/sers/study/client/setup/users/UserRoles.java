package org.sers.study.client.setup.users;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;
import org.sers.study.client.controllers.HyperLinks;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.client.views.presenters.WebForm;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Role;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.RoleService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class UserRoles extends WebForm<User> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9125832609150241976L;
	private DualListModel<Role> roles;

	@PostConstruct
	public void init() {
		reloadSourceAndTarget();
	}

	private void reloadSourceAndTarget() {
		List<Role> rolesSource = ApplicationContextProvider.getBean(RoleService.class).getRoles();
		List<Role> rolesTarget = new ArrayList<Role>();
		roles = new DualListModel<Role>(rolesSource, rolesTarget);
	}

	private void reconcileRoles() {
		List<Role> rolesSource = ApplicationContextProvider.getBean(RoleService.class).getRoles();
		List<Role> rolesTarget = new ArrayList<Role>(super.model.getRoles());

		for (Role existingRole : rolesTarget) {

			try {
				for (Role databaseRole : rolesSource) {
					if (existingRole.getId().equalsIgnoreCase(databaseRole.getId())) {
						rolesSource.remove(databaseRole);
					}
				}
			} catch (Exception exe) {

			}
		}

		roles = new DualListModel<Role>(rolesSource, rolesTarget);
	}

	/*
	 * public String saveUser() throws Exception { if
	 * (roles.getTarget().isEmpty()) ClientUiUtils.showErrorMessage(
	 * "Select Atleast A Role to continue",
	 * RequestContext.getCurrentInstance());
	 * 
	 * super.model.setRoles(new HashSet<Role>(roles.getTarget()));
	 * 
	 * try {
	 * ApplicationContextProvider.getBean(UserService.class).saveUser(super.
	 * model); } catch (ValidationFailedException e) {
	 * ClientUiUtils.showErrorMessage("Error Occured: " + e.getMessage(),
	 * RequestContext.getCurrentInstance()); }
	 * RequestContext.getCurrentInstance().closeDialog(null); return
	 * HyperLinks.USERSVIEW; }
	 */

	@Override
	public void resetModal() {
		super.resetModal();
		reconcileRoles();
		super.model = new User();
	}

	@Override
	public void setFormProperties() {
		super.setFormProperties();
		reconcileRoles();
	}

	@Override
	public String getViewUrl() {
		return HyperLinks.USERSVIEW;
	}

	@Override
	public void persist() throws Exception {
		if (roles.getTarget().isEmpty())
			UiUtils.showErrorMessage("Select Atleast A Role to continue", RequestContext.getCurrentInstance());
		try {
			super.model.setRoles(new HashSet<Role>(roles.getTarget()));
			ApplicationContextProvider.getBean(UserService.class).saveUser(super.model);
		} catch (ValidationFailedException e) {
			UiUtils.showErrorMessage("Error Occured: " + e.getMessage(), RequestContext.getCurrentInstance());
		}

	}

	/**
	 * @return the roles
	 */
	public DualListModel<Role> getRoles() {
		return roles;
	}

	/**
	 * @param roles
	 *            the roles to set
	 */
	public void setRoles(DualListModel<Role> roles) {
		this.roles = roles;
	}

}