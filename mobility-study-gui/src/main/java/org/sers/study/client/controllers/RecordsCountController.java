package org.sers.study.client.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.utils.UiUtils;

@AuthorComment
@ManagedBean
@ApplicationScoped
public class RecordsCountController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String author;
	private String authorEmail;
	private String authorContact;
	private int maxumumRecordsPerPage;
	private int searchFieldSize;
	private String globalFilterId;
	private String searchPlaceHolder;
	private String dataEmptyMessage;

	private String downloadButtonColor;
	private String searchButtonColor;
	private String operationButtonColor;
	private String formButtonColor;
	private String formIcon;
	private String reportButtonText;
	private String togglerIcon;
	private String togglerColor;
	private String filterMode;
	private String paginatorPosition;
	private String inputTextPercentageSize;
	private String inputTextPercentageSizeForm;
	private String panelWidthPercentageSize;
	private String formPanelWidthPercentageSize;


	@PostConstruct
	public void init() {
		inputTextPercentageSize = UiUtils.INPUT_TEXT_PERCENTAGE_SIZE;
		inputTextPercentageSizeForm = UiUtils.INPUT_TEXT_PERCENTAGE_SIZE_FORM;
		panelWidthPercentageSize = UiUtils.SELECT_MANY_PANEL_STYLE_WIDTH;
		formPanelWidthPercentageSize = UiUtils.FORM_SELECT_MANY_PANEL_STYLE_WIDTH;
		paginatorPosition = UiUtils.PAGINATION_PAGE_VIEW;
		searchFieldSize = UiUtils.SEARCH_FIELD_SIZE;
		maxumumRecordsPerPage = UiUtils.MAX_RESULTS_PER_PAGE;
		globalFilterId = UiUtils.GLOBAL_FILTER_ID;
		searchPlaceHolder = UiUtils.SEARCH_PLACE_HOLDER;
		dataEmptyMessage = UiUtils.DATA_EMPTY_MESSAGE;

		downloadButtonColor = UiUtils.DOWNLOAD_BUTTON_COLOR;
		searchButtonColor = UiUtils.SEARCH_BUTTON_COLOR;
		operationButtonColor = UiUtils.OPERATION_BUTTON_COLOR;
		formButtonColor = UiUtils.FORM_BUTTON_COLOR;
		formIcon = UiUtils.FORM_ICON;
		togglerIcon = UiUtils.TOGGLER_ICON;
		togglerColor = UiUtils.TOGGLER_BUTTON_COLOR;
		reportButtonText = UiUtils.REPORT_BUTTON_TEXT;
		author = UiUtils.AUTHOR;
		authorContact = UiUtils.AUTHOR_CONTACT;
		authorEmail = UiUtils.AUTHOR_EMAIL;
		filterMode = UiUtils.FILTER_MODE;
	}

	/**
	 * @return the inputTextPercentageSize
	 */
	public String getInputTextPercentageSize() {
		return inputTextPercentageSize;
	}

	/**
	 * @return the inputTextPercentageSizeForm
	 */
	public String getInputTextPercentageSizeForm() {
		return inputTextPercentageSizeForm;
	}

	/**
	 * @return the panelWidthPercentageSize
	 */
	public String getPanelWidthPercentageSize() {
		return panelWidthPercentageSize;
	}

	/**
	 * @return the formPanelWidthPercentageSize
	 */
	public String getFormPanelWidthPercentageSize() {
		return formPanelWidthPercentageSize;
	}

	/**
	 * @param inputTextPercentageSize the inputTextPercentageSize to set
	 */
	public void setInputTextPercentageSize(String inputTextPercentageSize) {
		this.inputTextPercentageSize = inputTextPercentageSize;
	}

	/**
	 * @param inputTextPercentageSizeForm the inputTextPercentageSizeForm to set
	 */
	public void setInputTextPercentageSizeForm(String inputTextPercentageSizeForm) {
		this.inputTextPercentageSizeForm = inputTextPercentageSizeForm;
	}

	/**
	 * @param panelWidthPercentageSize the panelWidthPercentageSize to set
	 */
	public void setPanelWidthPercentageSize(String panelWidthPercentageSize) {
		this.panelWidthPercentageSize = panelWidthPercentageSize;
	}

	/**
	 * @param formPanelWidthPercentageSize the formPanelWidthPercentageSize to set
	 */
	public void setFormPanelWidthPercentageSize(String formPanelWidthPercentageSize) {
		this.formPanelWidthPercentageSize = formPanelWidthPercentageSize;
	}

	/**
	 * @return the paginatorPosition
	 */
	public String getPaginatorPosition() {
		return paginatorPosition;
	}

	/**
	 * @param paginatorPosition
	 *            the paginatorPosition to set
	 */
	public void setPaginatorPosition(String paginatorPosition) {
		this.paginatorPosition = paginatorPosition;
	}

	/**
	 * @return the togglerColor
	 */
	public String getTogglerColor() {
		return togglerColor;
	}

	/**
	 * @param togglerColor
	 *            the togglerColor to set
	 */
	public void setTogglerColor(String togglerColor) {
		this.togglerColor = togglerColor;
	}

	/**
	 * @return the togglerIcon
	 */
	public String getTogglerIcon() {
		return togglerIcon;
	}

	/**
	 * @param togglerIcon
	 *            the togglerIcon to set
	 */
	public void setTogglerIcon(String togglerIcon) {
		this.togglerIcon = togglerIcon;
	}

	/**
	 * @return the reportButtonText
	 */
	public String getReportButtonText() {
		return reportButtonText;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @return the authorEmail
	 */
	public String getAuthorEmail() {
		return authorEmail;
	}

	/**
	 * @return the authorContact
	 */
	public String getAuthorContact() {
		return authorContact;
	}

	/**
	 * @return the filterMode
	 */
	public String getFilterMode() {
		return filterMode;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @param authorEmail
	 *            the authorEmail to set
	 */
	public void setAuthorEmail(String authorEmail) {
		this.authorEmail = authorEmail;
	}

	/**
	 * @param authorContact
	 *            the authorContact to set
	 */
	public void setAuthorContact(String authorContact) {
		this.authorContact = authorContact;
	}

	/**
	 * @param filterMode
	 *            the filterMode to set
	 */
	public void setFilterMode(String filterMode) {
		this.filterMode = filterMode;
	}

	/**
	 * @param reportButtonText
	 *            the reportButtonText to set
	 */
	public void setReportButtonText(String reportButtonText) {
		this.reportButtonText = reportButtonText;
	}

	/**
	 * @return the maxumumRecordsPerPage
	 */
	public int getMaxumumRecordsPerPage() {
		return maxumumRecordsPerPage;
	}

	/**
	 * @return the globalFilterId
	 */
	public String getGlobalFilterId() {
		return globalFilterId;
	}

	/**
	 * @return the searchPlaceHolder
	 */
	public String getSearchPlaceHolder() {
		return searchPlaceHolder;
	}

	/**
	 * @return the dataEmptyMessage
	 */
	public String getDataEmptyMessage() {
		return dataEmptyMessage;
	}

	/**
	 * @return the downloadButtonColor
	 */
	public String getDownloadButtonColor() {
		return downloadButtonColor;
	}

	/**
	 * @return the formButtonColor
	 */
	public String getFormButtonColor() {
		return formButtonColor;
	}

	/**
	 * @param formButtonColor
	 *            the formButtonColor to set
	 */
	public void setFormButtonColor(String formButtonColor) {
		this.formButtonColor = formButtonColor;
	}

	/**
	 * @return the searchButtonColor
	 */
	public String getSearchButtonColor() {
		return searchButtonColor;
	}

	/**
	 * @return the operationButtonColor
	 */
	public String getOperationButtonColor() {
		return operationButtonColor;
	}

	/**
	 * @return the formIcon
	 */
	public String getFormIcon() {
		return formIcon;
	}

	/**
	 * @return the searchFieldSize
	 */
	public int getSearchFieldSize() {
		return searchFieldSize;
	}

	/**
	 * @param searchFieldSize
	 *            the searchFieldSize to set
	 */
	public void setSearchFieldSize(int searchFieldSize) {
		this.searchFieldSize = searchFieldSize;
	}

	/**
	 * @param formIcon
	 *            the formIcon to set
	 */
	public void setFormIcon(String formIcon) {
		this.formIcon = formIcon;
	}

	/**
	 * @param downloadButtonColor
	 *            the downloadButtonColor to set
	 */
	public void setDownloadButtonColor(String downloadButtonColor) {
		this.downloadButtonColor = downloadButtonColor;
	}

	/**
	 * @param searchButtonColor
	 *            the searchButtonColor to set
	 */
	public void setSearchButtonColor(String searchButtonColor) {
		this.searchButtonColor = searchButtonColor;
	}

	/**
	 * @param operationButtonColor
	 *            the operationButtonColor to set
	 */
	public void setOperationButtonColor(String operationButtonColor) {
		this.operationButtonColor = operationButtonColor;
	}

	/**
	 * @param dataEmptyMessage
	 *            the dataEmptyMessage to set
	 */
	public void setDataEmptyMessage(String dataEmptyMessage) {
		this.dataEmptyMessage = dataEmptyMessage;
	}

	/**
	 * @param searchPlaceHolder
	 *            the searchPlaceHolder to set
	 */
	public void setSearchPlaceHolder(String searchPlaceHolder) {
		this.searchPlaceHolder = searchPlaceHolder;
	}

	/**
	 * @param maxumumRecordsPerPage
	 *            the maxumumRecordsPerPage to set
	 */
	public void setMaxumumRecordsPerPage(int maxumumRecordsPerPage) {
		this.maxumumRecordsPerPage = maxumumRecordsPerPage;
	}

	/**
	 * @param globalFilterId
	 *            the globalFilterId to set
	 */
	public void setGlobalFilterId(String globalFilterId) {
		this.globalFilterId = globalFilterId;
	}

}
