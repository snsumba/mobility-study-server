package org.sers.study.client.setup.locations;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.CapturedLocationService;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean(eager = true)
@ViewScoped
public class MappedLocationsView implements Serializable {

	private static final long serialVersionUID = 1L;
	private MapModel simpleModel;
	private Marker marker;
	private User loggedInUser;
	private String selectedLocation = "";
	private static final String LOCATION_TITLE = "Location";
	private static final String CLIENT_LOCATION_TITLE = "User Location";
	private List<CapturedLocation> capturedLocations = null;
	private String searchValue = null;
	private static final double DEFAULT_LONGITUDE = new Double("32.615756");// 32.615426
	private static final double DEFAULT_LATITUDE = new Double("0.3481094");// 0.348490
	private double latitude;
	private double longitude;

	@PostConstruct
	private void init() {
		this.loggedInUser = SharedAppData.getLoggedInUser();
		simpleModel = new DefaultMapModel();
		selectedLocation = "<html><body bgcolor='#E6E6FA'><table>"
				+ "<tr><td width='120'>Default</td><td width='300'>Location</td></tr>"
				+ "<tr><td width='120'>Place Name </td><td width='300'> Plot No 115 Muteesa II Rd, Ntinda Kampala(U)</td></tr>"
				+ "<tr><td width='120'>Latitude</td><td width='300'>0.348490</td></tr>"
				+ "<tr><td width='120'>Longitude</td><td width='300'>32.615426</td></tr>"
				+ "<tr><td width='120'>Accuracy</td><td width='300'>000000</td></tr>" + "</table></body></html>";
		if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR)) {
			if (searchValue != null) {
				if (searchValue.trim().length() > 0) {
					System.out.println("searchValue : " + searchValue);
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getAllCapturedLocations(searchValue);
				} else {
					System.out.println("searchValue is still empty");
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getCapturedLocations();
				}
			} else {
				System.out.println("searchValue is empty");
				capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
						.getCapturedLocations();
			}

			if (capturedLocations.size() > 0) {
				for (CapturedLocation userLocation : capturedLocations) {
					LatLng userCordinates = new LatLng(
							new Double((userLocation.getLatitude() != null ? userLocation.getLatitude() : "32.615426")),
							new Double(
									(userLocation.getLongitude() != null ? userLocation.getLongitude() : "0.348490")));

					selectedLocation = "<html><body bgcolor='#E6E6FA'><table>"
							+ "<tr><td width='120'>First Name</td><td width='300'>"
							+ userLocation.getUser().getFirstName() + "</td></tr>"
							+ "<tr><td width='120'>Last Name</td><td width='300'>"
							+ userLocation.getUser().getLastName() + "</td></tr>"
							+ "<tr><td width='120'>Phone Number</td><td width='300'>"
							+ userLocation.getUser().getPhoneNumber() + "</td></tr>"
							+ "<tr><td width='120'>Place Name </td><td width='300'>" + userLocation.getPlaceName()
							+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
							+ userLocation.getLatitude() + "</td></tr>"
							+ "<tr><td width='120'>Longitude</td><td width='300'>" + userLocation.getLongitude()
							+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
							+ userLocation.getLatitude() + "</td></tr>"
							+ "<tr><td width='120'>Accuracy</td><td width='300'>" + userLocation.getAccuracy()
							+ "</td></tr>" + "</table></body></html>";
					latitude = new Double(
							(userLocation.getLatitude() != null ? userLocation.getLatitude() : "0.348490"));
					longitude = new Double(
							(userLocation.getLongitude() != null ? userLocation.getLongitude() : "32.615426"));
					Marker selectedMarker = new Marker(userCordinates, MappedLocationsView.CLIENT_LOCATION_TITLE);
					selectedMarker.setData(selectedLocation);
					simpleModel.addOverlay(selectedMarker);
				}
			} else {
				LatLng yodimeCordinates = new LatLng(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
				longitude = new Double(DEFAULT_LONGITUDE);
				latitude = new Double(DEFAULT_LATITUDE);

				Marker selectedMarker = new Marker(yodimeCordinates, MappedLocationsView.LOCATION_TITLE);
				selectedMarker.setData(selectedLocation);
				simpleModel.addOverlay(selectedMarker);
			}
		} else {
			if (searchValue != null) {
				if (searchValue.trim().length() > 0) {
					System.out.println("searchValue : " + searchValue);
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getAllCapturedLocations(loggedInUser, searchValue);
				} else {
					System.out.println("searchValue is still empty");
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getCapturedLocations(loggedInUser);
				}
			} else {
				System.out.println("searchValue is empty");
				capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
						.getCapturedLocations(loggedInUser);
			}

			if (capturedLocations.size() > 0) {
				for (CapturedLocation userLocation : capturedLocations) {
					LatLng userCordinates = new LatLng(
							new Double((userLocation.getLatitude() != null ? userLocation.getLatitude() : "32.615426")),
							new Double(
									(userLocation.getLongitude() != null ? userLocation.getLongitude() : "0.348490")));

					selectedLocation = "<html><body bgcolor='#E6E6FA'><table>"
							+ "<tr><td width='120'>First Name</td><td width='300'>"
							+ userLocation.getUser().getFirstName() + "</td></tr>"
							+ "<tr><td width='120'>Last Name</td><td width='300'>"
							+ userLocation.getUser().getLastName() + "</td></tr>"
							+ "<tr><td width='120'>Phone Number</td><td width='300'>"
							+ userLocation.getUser().getPhoneNumber() + "</td></tr>"
							+ "<tr><td width='120'>Place Name </td><td width='300'>" + userLocation.getPlaceName()
							+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
							+ userLocation.getLatitude() + "</td></tr>"
							+ "<tr><td width='120'>Longitude</td><td width='300'>" + userLocation.getLongitude()
							+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
							+ userLocation.getLatitude() + "</td></tr>"
							+ "<tr><td width='120'>Accuracy</td><td width='300'>" + userLocation.getAccuracy()
							+ "</td></tr>" + "</table></body></html>";
					latitude = new Double(
							(userLocation.getLatitude() != null ? userLocation.getLatitude() : "0.348490"));
					longitude = new Double(
							(userLocation.getLongitude() != null ? userLocation.getLongitude() : "32.615426"));
					Marker selectedMarker = new Marker(userCordinates, MappedLocationsView.CLIENT_LOCATION_TITLE);
					selectedMarker.setData(selectedLocation);
					simpleModel.addOverlay(selectedMarker);
				}
			} else {
				LatLng yodimeCordinates = new LatLng(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
				longitude = new Double(DEFAULT_LONGITUDE);
				latitude = new Double(DEFAULT_LATITUDE);

				Marker selectedMarker = new Marker(yodimeCordinates, MappedLocationsView.LOCATION_TITLE);
				selectedMarker.setData(selectedLocation);
				simpleModel.addOverlay(selectedMarker);
			}
		}
	}

	public void searchLocations() throws Exception {
		this.loggedInUser = SharedAppData.getLoggedInUser();
		simpleModel = new DefaultMapModel();
		selectedLocation = "<html><body bgcolor='#E6E6FA'><table>"
				+ "<tr><td width='120'>Default</td><td width='300'>Location</td></tr>"
				+ "<tr><td width='120'>Place Name </td><td width='300'> Plot No 115 Muteesa II Rd, Ntinda Kampala(U)</td></tr>"
				+ "<tr><td width='120'>Latitude</td><td width='300'>0.348490</td></tr>"
				+ "<tr><td width='120'>Longitude</td><td width='300'>32.615426</td></tr>" + "</table></body></html>";
		if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR)) {
			if (searchValue != null) {
				if (searchValue.trim().length() > 0) {
					System.out.println("searchValue : " + searchValue);
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getAllCapturedLocations(searchValue);
				} else {
					System.out.println("searchValue is still empty");
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getCapturedLocations();
				}
			} else {
				System.out.println("searchValue is empty");
				capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
						.getCapturedLocations();
			}

			if (capturedLocations.size() > 0) {
				for (CapturedLocation userLocation : capturedLocations) {
					LatLng userCordinates = new LatLng(
							new Double((userLocation.getLatitude() != null ? userLocation.getLatitude() : "32.615426")),
							new Double(
									(userLocation.getLongitude() != null ? userLocation.getLongitude() : "0.348490")));

					selectedLocation = "<html><body bgcolor='#E6E6FA'><table>"
							+ "<tr><td width='120'>First Name</td><td width='300'>"
							+ userLocation.getUser().getFirstName() + "</td></tr>"
							+ "<tr><td width='120'>Last Name</td><td width='300'>"
							+ userLocation.getUser().getLastName() + "</td></tr>"
							+ "<tr><td width='120'>Phone Number</td><td width='300'>"
							+ userLocation.getUser().getPhoneNumber() + "</td></tr>"
							+ "<tr><td width='120'>Yo-Dime Id </td><td width='300'>"
							+ "<tr><td width='120'>Place Name </td><td width='300'>" + userLocation.getPlaceName()
							+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
							+ userLocation.getLatitude() + "</td></tr>"
							+ "<tr><td width='120'>Longitude</td><td width='300'>" + userLocation.getLongitude()
							+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
							+ userLocation.getLatitude() + "</td></tr>"
							+ "<tr><td width='120'>Accuracy</td><td width='300'>" + userLocation.getAccuracy()
							+ "</td></tr>" + "</table></body></html>";
					latitude = new Double(
							(userLocation.getLatitude() != null ? userLocation.getLatitude() : "0.348490"));
					longitude = new Double(
							(userLocation.getLongitude() != null ? userLocation.getLongitude() : "32.615426"));

					Marker selectedMarker = new Marker(userCordinates, MappedLocationsView.CLIENT_LOCATION_TITLE);
					selectedMarker.setData(selectedLocation);
					simpleModel.addOverlay(selectedMarker);
				}
			} else {
				LatLng yodimeCordinates = new LatLng(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
				longitude = new Double(DEFAULT_LONGITUDE);
				latitude = new Double(DEFAULT_LATITUDE);
				Marker selectedMarker = new Marker(yodimeCordinates, MappedLocationsView.LOCATION_TITLE);
				selectedMarker.setData(selectedLocation);
				simpleModel.addOverlay(selectedMarker);
			}
		} else {
			if (searchValue != null) {
				if (searchValue.trim().length() > 0) {
					System.out.println("searchValue : " + searchValue);
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getAllCapturedLocations(loggedInUser, searchValue);
				} else {
					System.out.println("searchValue is still empty");
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getCapturedLocations(loggedInUser);
				}
			} else {
				System.out.println("searchValue is empty");
				capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
						.getCapturedLocations(loggedInUser);
			}

			if (capturedLocations.size() > 0) {
				for (CapturedLocation userLocation : capturedLocations) {
					LatLng userCordinates = new LatLng(
							new Double((userLocation.getLatitude() != null ? userLocation.getLatitude() : "32.615426")),
							new Double(
									(userLocation.getLongitude() != null ? userLocation.getLongitude() : "0.348490")));

					selectedLocation = "<html><body bgcolor='#E6E6FA'><table>"
							+ "<tr><td width='120'>First Name</td><td width='300'>"
							+ userLocation.getUser().getFirstName() + "</td></tr>"
							+ "<tr><td width='120'>Last Name</td><td width='300'>"
							+ userLocation.getUser().getLastName() + "</td></tr>"
							+ "<tr><td width='120'>Phone Number</td><td width='300'>"
							+ userLocation.getUser().getPhoneNumber() + "</td></tr>"
							+ "<tr><td width='120'>Yo-Dime Id </td><td width='300'>"
							+ "<tr><td width='120'>Place Name </td><td width='300'>" + userLocation.getPlaceName()
							+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
							+ userLocation.getLatitude() + "</td></tr>"
							+ "<tr><td width='120'>Longitude</td><td width='300'>" + userLocation.getLongitude()
							+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
							+ userLocation.getLatitude() + "</td></tr>"
							+ "<tr><td width='120'>Accuracy</td><td width='300'>" + userLocation.getAccuracy()
							+ "</td></tr>" + "</table></body></html>";
					latitude = new Double(
							(userLocation.getLatitude() != null ? userLocation.getLatitude() : "0.348490"));
					longitude = new Double(
							(userLocation.getLongitude() != null ? userLocation.getLongitude() : "32.615426"));

					Marker selectedMarker = new Marker(userCordinates, MappedLocationsView.CLIENT_LOCATION_TITLE);
					selectedMarker.setData(selectedLocation);
					simpleModel.addOverlay(selectedMarker);
				}
			} else {
				LatLng yodimeCordinates = new LatLng(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
				longitude = new Double(DEFAULT_LONGITUDE);
				latitude = new Double(DEFAULT_LATITUDE);
				Marker selectedMarker = new Marker(yodimeCordinates, MappedLocationsView.LOCATION_TITLE);
				selectedMarker.setData(selectedLocation);
				simpleModel.addOverlay(selectedMarker);
			}
		}
	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public void onMarkerSelect(OverlaySelectEvent event) {
		marker = (Marker) event.getOverlay();
		UiUtils.showMessageBox(marker.getData().toString(), marker.getTitle(), RequestContext.getCurrentInstance());
	}

	public Marker getMarker() {
		return marker;
	}

	/**
	 * @return the loggedInUser
	 */
	public User getLoggedInUser() {
		return loggedInUser;
	}

	/**
	 * @return the selectedLocation
	 */
	public String getSelectedLocation() {
		return selectedLocation;
	}

	/**
	 * @return the capturedLocations
	 */
	public List<CapturedLocation> getCapturedLocations() {
		return capturedLocations;
	}

	/**
	 * @return the searchValue
	 */
	public String getSearchValue() {
		return searchValue;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @param selectedLocation
	 *            the selectedLocation to set
	 */
	public void setSelectedLocation(String selectedLocation) {
		this.selectedLocation = selectedLocation;
	}

	/**
	 * @param capturedLocations
	 *            the capturedLocations to set
	 */
	public void setCapturedLocations(List<CapturedLocation> capturedLocations) {
		this.capturedLocations = capturedLocations;
	}

	/**
	 * @param searchValue
	 *            the searchValue to set
	 */
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	/**
	 * @param simpleModel
	 *            the simpleModel to set
	 */
	public void setSimpleModel(MapModel simpleModel) {
		this.simpleModel = simpleModel;
	}

	/**
	 * @param marker
	 *            the marker to set
	 */
	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	/**
	 * @param loggedInUser
	 *            the loggedInUser to set
	 */
	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

}