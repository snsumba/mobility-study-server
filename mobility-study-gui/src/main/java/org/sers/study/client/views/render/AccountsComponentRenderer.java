package org.sers.study.client.views.render;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.exception.SessionExpiredException;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean
@SessionScoped
public class AccountsComponentRenderer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9025578123139327306L;
	private boolean folioAccount;
	

	/**
	 * @return the folioAccount
	 * @throws SessionExpiredException
	 */
	public boolean isFolioAccount() throws SessionExpiredException {
		User loggedInUser = SharedAppData.getLoggedInUser();
		if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR))
			folioAccount = true;
		return folioAccount;
	}

}