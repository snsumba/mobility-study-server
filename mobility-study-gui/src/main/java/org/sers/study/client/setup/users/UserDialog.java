/**
 * 
 */
package org.sers.study.client.setup.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.sers.study.client.views.presenters.DialogForm;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.Gender;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class UserDialog extends DialogForm<User> {

	private static final long serialVersionUID = 5663587060380874864L;
	private static final String DIALOG_NAME = "UserDialog";
	private List<Gender> listOfGenders;

	public UserDialog() {
		super(DIALOG_NAME, 620, 390);
		listOfGenders = new ArrayList<Gender>();
		listOfGenders.addAll(Arrays.asList(Gender.values()));
	}

	@Override
	public void persist() throws Exception {
		ApplicationContextProvider.getBean(UserService.class).saveUser(super.model);
	}

	@Override
	public void resetModal() {
		super.resetModal();
		super.model = new User();
	}

	/**
	 * @return the listOfGenders
	 */
	public List<Gender> getListOfGenders() {
		return listOfGenders;
	}

	/**
	 * @param listOfGenders
	 *            the listOfGenders to set
	 */
	public void setListOfGenders(List<Gender> listOfGenders) {
		this.listOfGenders = listOfGenders;
	}
}
