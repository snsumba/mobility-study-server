package org.sers.study.client.setup.locations;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.Circle;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.primefaces.model.map.Overlay;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.exception.SessionExpiredException;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.CapturedLocationService;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.server.shared.SharedAppData;
import org.springframework.beans.BeansException;

@ManagedBean
@ViewScoped
public class CirclerMapView implements Serializable {
	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<CapturedLocation> capturedLocations;
	private List<CapturedLocation> filteredCapturedLocations;
	private CapturedLocation capturedLocation;
	private MapModel circleModel;
	private static final String[] DEFAULT_COLOURS = { "00ffff", "008080", "0000ff", "fff00", "00ff00", "ff00ff",
			"ff0000" };
	private static final int ARRAY_SIZE = 7;
	private Random rand;
	private double radius = 100;

	private User loggedInUser;
	private String selectedLocation = "";
	private String searchValue = null;
	private double latitude;
	private double longitude;
	private static final String CLIENT_LOCATION_TITLE = "Location Information";

	@PostConstruct
	public void init() {
		loggedInUser = SharedAppData.getLoggedInUser();
		if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR)) {
			if (searchValue != null) {
				if (searchValue.trim().length() > 0) {
					System.out.println("searchValue : " + searchValue);
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getAllCapturedLocations(searchValue);
				} else {
					System.out.println("searchValue is still empty");
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getCapturedLocations();
				}
			} else {
				System.out.println("searchValue is empty");
				capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
						.getCapturedLocations();
			}
		} else {
			if (searchValue != null) {
				if (searchValue.trim().length() > 0) {
					System.out.println("searchValue : " + searchValue);
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getAllCapturedLocations(loggedInUser, searchValue);
				} else {
					System.out.println("searchValue is still empty");
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getCapturedLocations(loggedInUser);
				}
			} else {
				System.out.println("searchValue is empty");
				capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
						.getCapturedLocations(loggedInUser);
			}
		}
		filteredCapturedLocations = new ArrayList<CapturedLocation>(capturedLocations);
		circleModel = new DefaultMapModel();
		rand = new Random();
		for (CapturedLocation capturedLocation : filteredCapturedLocations) {
			if (capturedLocation != null) {
				LatLng coord1 = new LatLng(new Double(capturedLocation.getLatitude()),
						new Double(capturedLocation.getLongitude()));
				Circle circle1 = new Circle(coord1, radius);

				selectedLocation = "<html><body bgcolor='#E6E6FA'><table>"
						+ "<tr><td width='120'>First Name</td><td width='300'>"
						+ capturedLocation.getUser().getFirstName() + "</td></tr>"
						+ "<tr><td width='120'>Last Name</td><td width='300'>"
						+ capturedLocation.getUser().getLastName() + "</td></tr>"
						+ "<tr><td width='120'>Phone Number</td><td width='300'>"
						+ capturedLocation.getUser().getPhoneNumber() + "</td></tr>"
						+ "<tr><td width='120'>Place Name </td><td width='300'>" + capturedLocation.getPlaceName()
						+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
						+ capturedLocation.getLatitude() + "</td></tr>"
						+ "<tr><td width='120'>Longitude</td><td width='300'>" + capturedLocation.getLongitude()
						+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
						+ capturedLocation.getLatitude() + "</td></tr>"
						+ "<tr><td width='120'>Accuracy</td><td width='300'>" + capturedLocation.getAccuracy()
						+ "</td></tr>" + "<tr><td width='120'>Redius</td><td width='300'>" + radius
						+ " metres</td></tr>" + "</table></body></html>";

				circle1.setData(selectedLocation);
				String random = getRandomColor();
				circle1.setStrokeColor("#" + random);
				circle1.setFillColor("#" + random);
				circle1.setStrokeOpacity(0.7);
				circle1.setFillOpacity(0.7);
				circleModel.addOverlay(circle1);

				Marker selectedMarker = new Marker(coord1, CirclerMapView.CLIENT_LOCATION_TITLE);
				selectedMarker.setData(selectedLocation);
				circleModel.addOverlay(selectedMarker);

				latitude = new Double(
						(capturedLocation.getLatitude() != null ? capturedLocation.getLatitude() : "0.348490"));
				longitude = new Double(
						(capturedLocation.getLongitude() != null ? capturedLocation.getLongitude() : "32.615426"));
			}
		}
	}

	public void searchLocations() throws Exception {
		this.loggedInUser = SharedAppData.getLoggedInUser();

		if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR)) {
			if (searchValue != null) {
				if (searchValue.trim().length() > 0) {
					System.out.println("searchValue : " + searchValue);
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getAllCapturedLocations(searchValue);
				} else {
					System.out.println("searchValue is still empty");
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getCapturedLocations();
				}
			} else {
				System.out.println("searchValue is empty");
				capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
						.getCapturedLocations();
			}
		} else {
			if (searchValue != null) {
				if (searchValue.trim().length() > 0) {
					System.out.println("searchValue : " + searchValue);
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getAllCapturedLocations(loggedInUser, searchValue);
				} else {
					System.out.println("searchValue is still empty");
					capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
							.getCapturedLocations(loggedInUser);
				}
			} else {
				System.out.println("searchValue is empty");
				capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
						.getCapturedLocations(loggedInUser);
			}
		}
		filteredCapturedLocations = new ArrayList<CapturedLocation>(capturedLocations);
		circleModel = new DefaultMapModel();
		rand = new Random();
		for (CapturedLocation capturedLocation : filteredCapturedLocations) {
			if (capturedLocation != null) {
				LatLng coord1 = new LatLng(new Double(capturedLocation.getLatitude()),
						new Double(capturedLocation.getLongitude()));
				Circle circle1 = new Circle(coord1, radius);

				selectedLocation = "<html><body bgcolor='#E6E6FA'><table>"
						+ "<tr><td width='120'>First Name</td><td width='300'>"
						+ capturedLocation.getUser().getFirstName() + "</td></tr>"
						+ "<tr><td width='120'>Last Name</td><td width='300'>"
						+ capturedLocation.getUser().getLastName() + "</td></tr>"
						+ "<tr><td width='120'>Phone Number</td><td width='300'>"
						+ capturedLocation.getUser().getPhoneNumber() + "</td></tr>"
						+ "<tr><td width='120'>Place Name </td><td width='300'>" + capturedLocation.getPlaceName()
						+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
						+ capturedLocation.getLatitude() + "</td></tr>"
						+ "<tr><td width='120'>Longitude</td><td width='300'>" + capturedLocation.getLongitude()
						+ "</td></tr>" + "<tr><td width='120'>Latitude</td><td width='300'>"
						+ capturedLocation.getLatitude() + "</td></tr>"
						+ "<tr><td width='120'>Accuracy</td><td width='300'>" + capturedLocation.getAccuracy()
						+ "</td></tr>" + "<tr><td width='120'>Redius</td><td width='300'>" + radius
						+ " metres</td></tr>" + "</table></body></html>";

				circle1.setData(selectedLocation);
				String random = getRandomColor();
				circle1.setStrokeColor("#" + random);
				circle1.setFillColor("#" + random);
				circle1.setStrokeOpacity(0.7);
				circle1.setFillOpacity(0.7);
				circleModel.addOverlay(circle1);

				Marker selectedMarker = new Marker(coord1, CirclerMapView.CLIENT_LOCATION_TITLE);
				selectedMarker.setData(selectedLocation);
				circleModel.addOverlay(selectedMarker);

				latitude = new Double(
						(capturedLocation.getLatitude() != null ? capturedLocation.getLatitude() : "0.348490"));
				longitude = new Double(
						(capturedLocation.getLongitude() != null ? capturedLocation.getLongitude() : "32.615426"));
			}
		}

	}

	private String getRandomColor() {
		return DEFAULT_COLOURS[rand.nextInt(ARRAY_SIZE)];
	}

	public MapModel getCircleModel() {
		return circleModel;
	}

	public void onCircleSelect(OverlaySelectEvent event) throws Exception {
		UiUtils.showMessageBox(((Overlay) event.getOverlay()).getData().toString(), "OSTK",
				RequestContext.getCurrentInstance());
	}

	/**
	 * @return the loggedInUser
	 */
	public User getLoggedInUser() {
		return loggedInUser;
	}

	/**
	 * @return the selectedLocation
	 */
	public String getSelectedLocation() {
		return selectedLocation;
	}

	/**
	 * @return the searchValue
	 */
	public String getSearchValue() {
		return searchValue;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @return the rand
	 */
	public Random getRand() {
		return rand;
	}

	/**
	 * @return the radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * @param loggedInUser
	 *            the loggedInUser to set
	 */
	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	/**
	 * @param selectedLocation
	 *            the selectedLocation to set
	 */
	public void setSelectedLocation(String selectedLocation) {
		this.selectedLocation = selectedLocation;
	}

	/**
	 * @param searchValue
	 *            the searchValue to set
	 */
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @param circleModel
	 *            the circleModel to set
	 */
	public void setCircleModel(MapModel circleModel) {
		this.circleModel = circleModel;
	}

	/**
	 * @param rand
	 *            the rand to set
	 */
	public void setRand(Random rand) {
		this.rand = rand;
	}

	/**
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * @return the filteredCapturedLocations
	 */
	public List<CapturedLocation> getFilteredCapturedLocations() {
		return filteredCapturedLocations;
	}

	/**
	 * @param filteredCapturedLocations
	 *            the filteredCapturedLocations to set
	 */
	public void setFilteredCapturedLocations(List<CapturedLocation> filteredCapturedLocations) {
		this.filteredCapturedLocations = filteredCapturedLocations;
	}

	/**
	 * @param capturedLocations
	 *            the capturedLocations to set
	 */
	public void setCapturedLocations(List<CapturedLocation> capturedLocations) {
		this.capturedLocations = capturedLocations;
	}

	public List<CapturedLocation> getCapturedLocations() {
		return capturedLocations;
	}

	public void refreshCapturedLocations() throws BeansException, SessionExpiredException {
		capturedLocations = ApplicationContextProvider.getApplicationContext().getBean(CapturedLocationService.class)
				.getCapturedLocations();
		filteredCapturedLocations = new ArrayList<CapturedLocation>(capturedLocations);
	}

	/**
	 * @return the capturedLocation
	 */
	public CapturedLocation getCapturedLocation() {
		return capturedLocation;
	}

	/**
	 * @param capturedLocation
	 *            the capturedLocation to set
	 */
	public void setCapturedLocation(CapturedLocation capturedLocation) {
		this.capturedLocation = capturedLocation;
	}

	public void deleteCapturedLocation() throws BeansException, ValidationFailedException, SessionExpiredException {
		if (capturedLocation != null) {
			capturedLocation.setRecordStatus(RecordStatus.DELETED);
			ApplicationContextProvider.getBean(CapturedLocationService.class).saveCapturedLocation(capturedLocation);
			refreshCapturedLocations();
		}
	}
}