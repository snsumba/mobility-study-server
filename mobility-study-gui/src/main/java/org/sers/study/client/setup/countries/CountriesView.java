package org.sers.study.client.setup.countries;

import java.io.IOException;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.sers.study.client.controllers.HyperLinks;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.client.views.presenters.PaginatedTable;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.exception.SessionExpiredException;
import org.sers.study.model.security.Country;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.server.core.service.CountryService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean
@ViewScoped
public class CountriesView extends PaginatedTable<Country> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8413343075645160075L;
	private CountryService countryService;

	@PostConstruct
	public void init() {

		try {
			super.enforceSecurity(HyperLinks.COUNTRIES_VIEW, true,
					new String[] { PermissionConstants.PERM_ADMINISTRATOR });
			countryService = ApplicationContextProvider.getApplicationContext().getBean(CountryService.class);
		} catch (SessionExpiredException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void reloadFromDB(int offset, int limit, Map<String, Object> filters) throws Exception {
		try {
			super.setTotalRecords(
					countryService.countCountries((filters != null && !filters.isEmpty() ? filters : null)));
			super.setDataModels(countryService.getCountries(offset, limit,
					(filters != null && !filters.isEmpty() ? filters : null)));
		} catch (Exception e) {
			UiUtils.showErrorMessage("Error: " + e.getMessage(), RequestContext.getCurrentInstance());
		}
	}
}
