package org.sers.study.client.views.presenters;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.sers.study.client.controllers.HyperLinks;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.exception.SessionExpiredException;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.core.security.service.impl.CustomSessionProvider;
import org.sers.study.server.core.utils.ApplicationContextProvider;

/**
 * Custom implementation of a {@link LazyDataModel}, to be extended by all
 * classes that need to load data lazily from the server.
 * 
 * @param <T>
 */
@AuthorComment
public abstract class PaginatedTable<T> extends LazyDataModel<T> implements SecurityValidator{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<String, Object> filters;
	private List<T> dataModels;
	private int maximumresultsPerpage;
	private int totalRecords;
	private boolean renderPage;
	
	/**
	 * Default constructor that sets the maximum records per server trip using
	 * {@link UiUtils}.MAX_RESULTS_PER_PAGEE
	 */
	public PaginatedTable() {
		super();
		this.maximumresultsPerpage = UiUtils.MAX_RESULTS_PER_PAGE;
	}

	
	@Override
	public void enforceSecurity(String redirectPage, boolean allowIfAdmin,
			String[] roles) throws SessionExpiredException, IOException {
		User loggedInUser = ApplicationContextProvider.getBean(
				CustomSessionProvider.class).getLoggedInUser();
		renderPage = false;
		if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR))
			if (allowIfAdmin) {
				renderPage = true;
				return;
			} else
				redirectTo(HyperLinks.HOMEPAGE);

		for (String permision : roles)
			if (loggedInUser.hasRole(permision)) {
				renderPage = true;
				return;
			}

		redirectTo(redirectPage);
	}

	@Override
	public void redirectTo(String pageUrl) throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance()
				.getExternalContext();

		ec.redirect(ec.getRequestContextPath() + pageUrl);
	}
	
	/**
	 * Constructor that specifies the total records to be fetched per server
	 * trip
	 * 
	 * @param maximumresultsPerpage
	 */
	public PaginatedTable(int maximumresultsPerpage) {
		this.maximumresultsPerpage = maximumresultsPerpage;
	}

	
	/**
	 * Loads the paginated records by calling the implementation of reloadFromDB
	 * as implemented by the subclass
	 */
	@Override
	public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		try {
			this.filters = filters;
			reloadFromDB(first, pageSize, filters);
			setRowCount(totalRecords);

			return dataModels;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Sub classes must provide implementation for this method that fetches
	 * paginated records from the server
	 * 
	 * @param offset
	 * @param limit
	 * @param filters
	 * @throws Exception
	 */
	public abstract void reloadFromDB(int offset, int limit, Map<String, Object> filters) throws Exception;

	/**
	 * Reloads the view without any filters
	 * 
	 * @throws Exception
	 */
	public void reloadFilterReset() throws Exception {
		reloadFromDB(0, maximumresultsPerpage, null);
	}

	/**
	 * Reloads the view using the filters from the previous database query This
	 * will in most cases be called.
	 * 
	 * @throws Exception
	 */
	public void reloadWithFilters() throws Exception {
		reloadFromDB(0, maximumresultsPerpage, filters);
	}

	/**
	 * @return the renderPage
	 */
	public boolean isRenderPage() {
		return renderPage;
	}


	/**
	 * @param renderPage the renderPage to set
	 */
	public void setRenderPage(boolean renderPage) {
		this.renderPage = renderPage;
	}


	/**
	 * @return the filters
	 */
	public Map<String, Object> getFilters() {
		return filters;
	}

	/**
	 * @return the maximumresultsPerpage
	 */
	public int getMaximumresultsPerpage() {
		return maximumresultsPerpage;
	}

	/**
	 * @return the totalRecords
	 */
	public int getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @return the dataModels
	 */
	public List<T> getDataModels() {
		return dataModels;
	}

	/**
	 * @param dataModels
	 *            the dataModels to set
	 */
	public void setDataModels(List<T> dataModels) {
		this.dataModels = dataModels;
	}

	/**
	 * @param filters
	 *            the filters to set
	 */
	public void setFilters(Map<String, Object> filters) {
		this.filters = filters;
	}

	/**
	 * @param maximumresultsPerpage
	 *            the maximumresultsPerpage to set
	 */
	public void setMaximumresultsPerpage(int maximumresultsPerpage) {
		this.maximumresultsPerpage = maximumresultsPerpage;
	}

	/**
	 * @param totalRecords
	 *            the totalRecords to set
	 */
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
}
