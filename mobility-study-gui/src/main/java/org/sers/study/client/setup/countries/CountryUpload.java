package org.sers.study.client.setup.countries;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.sers.study.client.controllers.HyperLinks;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.security.Country;
import org.sers.study.server.core.service.CountryService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class CountryUpload extends UploadBaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2691851163637739232L;
	private List<Country> countries = new ArrayList<Country>();
	private Row row = null;
	private boolean renderUploadView = true;

	public void populate(Iterator<Row> rowIterator) throws Exception {
		Iterator<Cell> cellIterator = row.cellIterator();

		while (cellIterator.hasNext()) {
			Cell cell = cellIterator.next();

			switch (cell.getColumnIndex()) {
			case 0:
				validateCell(Cell.CELL_TYPE_STRING, cell, "Requires Only String values for a Country Name.", false,
						null);
				System.out.println("Cell 1 Value : " + cell.getStringCellValue());
				Country country = new Country(cell.getStringCellValue());
				country.setPostalCode(256);
				countries.add(country);
				break;

			default:
				break;
			}
		}
	}

	public void populateWithPostalCode(Iterator<Row> rowIterator) throws Exception {
		Iterator<Cell> cellIterator = row.cellIterator();

		while (rowIterator.hasNext()) {
			row = rowIterator.next();
			Cell nameCell = row.getCell(0);
			validateCell(Cell.CELL_TYPE_STRING, nameCell, "Requires only string values for a Country Name.", false,
					null);
			String name = getValueFromCell(nameCell);

			Cell postalCodeCell = row.getCell(1);
			validateCell(Cell.CELL_TYPE_NUMERIC, postalCodeCell, "Requires only number values for Postal Code .", false,
					null);
			int postalCode = new Integer(
					getValueFromCell(postalCodeCell).substring(0, getValueFromCell(postalCodeCell).indexOf(".")));

			Country country = new Country(name);
			country.setPostalCode(postalCode);
			countries.add(country);

		}
	}

	private void extractXSSF(UploadedFile uploadedFile) throws Exception {
		InputStream file = uploadedFile.getInputstream();
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);

		Iterator<Row> rowIterator = sheet.iterator();

		while (rowIterator.hasNext()) {
			row = rowIterator.next();
			populateWithPostalCode(rowIterator);
		}
		file.close();
	}

	private void extractHSSF(UploadedFile uploadedFile) throws Exception {
		InputStream file = uploadedFile.getInputstream();
		HSSFWorkbook workbook = new HSSFWorkbook(file);
		HSSFSheet sheet = workbook.getSheetAt(0);

		Iterator<Row> rowIterator = sheet.iterator();

		while (rowIterator.hasNext()) {
			row = rowIterator.next();
			populateWithPostalCode(rowIterator);
		}
		file.close();
	}

	public void uploadCountries(FileUploadEvent event) throws Exception {
		try {
			UploadedFile uploadedFile = event.getFile();

			if (uploadedFile.getFileName().endsWith(".xls")) {
				extractHSSF(uploadedFile);
			} else if (uploadedFile.getFileName().endsWith(".xlsx")) {
				extractXSSF(uploadedFile);
			} else {
				UiUtils.showErrorMessage("Unknown File Format<br>Upload .xls and .xlsx formats only.",
						RequestContext.getCurrentInstance());
			}
			for (Country country : countries) {
				ApplicationContextProvider.getBean(CountryService.class).save(country);
			}
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			ec.redirect(ec.getRequestContextPath() + HyperLinks.COUNTRIES_VIEW);
		} catch (IOException e) {
			UiUtils.showMessageBox("File Upload failed", "Action Failed", RequestContext.getCurrentInstance());
		}
	}

	/**
	 * @return the row
	 */
	public Row getRow() {
		return row;
	}

	/**
	 * @param row
	 *            the row to set
	 */
	public void setRow(Row row) {
		this.row = row;
	}

	/**
	 * @return the countries
	 */
	public List<Country> getCountries() {
		return countries;
	}

	/**
	 * @param countries
	 *            the countries to set
	 */
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	/**
	 * @return the renderUploadView
	 */
	public boolean isRenderUploadView() {
		return renderUploadView;
	}

	/**
	 * @param renderUploadView
	 *            the renderUploadView to set
	 */
	public void setRenderUploadView(boolean renderUploadView) {
		this.renderUploadView = renderUploadView;
	}

	@Override
	public String getViewUrl() {
		return HyperLinks.COUNTRIES_VIEW;
	}

	@Override
	public void persist() throws Exception {
		ApplicationContextProvider.getApplicationContext().getBean(CountryService.class).save(super.getModel());
	}

	@Override
	public void resetModal() {
		if (model != null) {
			this.renderUploadView = Boolean.FALSE;
		}
		super.setModel(new Country());
	}
}
