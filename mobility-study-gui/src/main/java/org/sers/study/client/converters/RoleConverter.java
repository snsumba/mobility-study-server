package org.sers.study.client.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.security.Role;
import org.sers.study.server.core.service.RoleService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@FacesConverter("roleConverter")
public class RoleConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if (arg2 == null || arg2.isEmpty())
			return null;
		return (Role) ApplicationContextProvider.getBean(RoleService.class).getObjectById(arg2);
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if (object == null || object instanceof String)
			return null;

		return ((Role) object).getId();
	}
}