
/**
 *Jul 5, 2017
 *org.sers.study.client.reports
 *mobility-study-gui
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.client.reports;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.StreamedContent;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.controllers.HyperLinks;
import org.sers.study.client.utils.ExcellFileCreator;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.client.views.presenters.WebForm;
import org.sers.study.model.BaseEntity;
import org.sers.study.model.TimeDiary;
import org.sers.study.model.exception.OperationFailedException;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.TimeDiaryService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean(eager = true)
@SessionScoped
public class DownloadUserTimeDiaryForm extends WebForm<BaseEntity> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private StreamedContent file;
	private User loggedInUser;
	private User selectedUser;
	private List<User> listOfUsers;
	private Date startDate = new Date();
	private Date endDate = new Date();

	public DownloadUserTimeDiaryForm() {
		loggedInUser = SharedAppData.getLoggedInUser();
		try {
			listOfUsers = ApplicationContextProvider.getBean(UserService.class).getUsers();
		} catch (OperationFailedException e) {
			e.printStackTrace();
		}
	}

	public StreamedContent getFile() {
		try {
			if (UiUtils.isRangeAcceptable(startDate, endDate, UiUtils.MAXIMUM_NUMBER_OF_REPORT_DAYS)) {
				if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR)) {
					List<TimeDiary> timeDiaries = ApplicationContextProvider.getBean(TimeDiaryService.class)
							.getTimeDiaries(selectedUser, startDate, endDate);
					System.out.println("timeDiaries : " + timeDiaries.size());
					file = new ExcellFileCreator().getTimeDiariesExcellDoc(timeDiaries, selectedUser);
				}
				// resetModal();
				return file;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void resetModal() {
		super.resetModal();
		super.model = new BaseEntity();
		this.selectedUser = new User();
	}

	@Override
	public String getViewUrl() {
		return HyperLinks.VIEW_DOWN_LOAD_TIME_DIARY;
	}

	@Override
	public void persist() throws Exception {

	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the loggedInUser
	 */
	public User getLoggedInUser() {
		return loggedInUser;
	}

	/**
	 * @return the selectedUser
	 */
	public User getSelectedUser() {
		return selectedUser;
	}

	/**
	 * @return the listOfUsers
	 */
	public List<User> getListOfUsers() {
		return listOfUsers;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(StreamedContent file) {
		this.file = file;
	}

	/**
	 * @param loggedInUser
	 *            the loggedInUser to set
	 */
	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	/**
	 * @param selectedUser
	 *            the selectedUser to set
	 */
	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	/**
	 * @param listOfUsers
	 *            the listOfUsers to set
	 */
	public void setListOfUsers(List<User> listOfUsers) {
		this.listOfUsers = listOfUsers;
	}

}
