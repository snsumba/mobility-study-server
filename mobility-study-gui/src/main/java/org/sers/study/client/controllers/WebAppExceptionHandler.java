package org.sers.study.client.controllers;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.annotations.AuthorComment;

/**
 * @see http://www.mkyong.com/jsf2/access-a-managed-bean-from-event-listener-jsf/
 * @see http://www.java-samples.com/showtutorial.php?tutorialid=474
 * @see http://www.javacodegeeks.com/2012/04/5-useful-methods-jsf-developers-should.html
 * @see http://biemond.blogspot.com/2011/01/some-handy-code-for-your-managed-beans.html
 * {@link http://biemond.blogspot.com/2011/01/some-handy-code-for-your-managed-beans.html}
 *
 */
@AuthorComment
public abstract class WebAppExceptionHandler implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void invalidateSession() {
		FacesContext.getCurrentInstance().getExternalContext()
				.invalidateSession();
	}

	public void throwError500(String errorMessage) throws IOException {
		FacesContext.getCurrentInstance().getExternalContext()
				.responseSendError(500, errorMessage);
	}

	public void throwError(String errorMessage) throws Exception {
		UiUtils.showMessageBox(errorMessage, "Action Failed",
				RequestContext.getCurrentInstance());
	}

	public void redirectTo(String pageUrl, boolean invalidateSession)
			throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance()
				.getExternalContext();
		if (invalidateSession)
			ec.invalidateSession();

		ec.redirect(ec.getRequestContextPath() + pageUrl);
	}

	public void redirectTo(String pageUrl) throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance()
				.getExternalContext();

		ec.redirect(ec.getRequestContextPath() + pageUrl);
	}	
}
