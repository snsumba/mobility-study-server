package org.sers.study.client.setup.settings;

import java.io.IOException;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.controllers.HyperLinks;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.client.views.presenters.PaginatedTable;
import org.sers.study.model.DurationInfo;
import org.sers.study.model.exception.SessionExpiredException;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.server.core.service.DurationInfoService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean
@ViewScoped
public class DurationInformationsView extends PaginatedTable<DurationInfo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8413343075645160075L;
	private DurationInfoService durationInfoService;

	@PostConstruct
	public void init() {

		try {
			super.enforceSecurity(HyperLinks.VIEW_DURATION_INFO, true,
					new String[] { PermissionConstants.PERM_ADMINISTRATOR });
			durationInfoService = ApplicationContextProvider.getApplicationContext()
					.getBean(DurationInfoService.class);
		} catch (SessionExpiredException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void reloadFromDB(int offset, int limit, Map<String, Object> filters) throws Exception {
		try {
			super.setTotalRecords(durationInfoService
					.countDurationInfos((filters != null && !filters.isEmpty() ? filters : null)));
			super.setDataModels(durationInfoService.getDurationInfos(offset, limit,
					(filters != null && !filters.isEmpty() ? filters : null)));
		} catch (Exception e) {
			UiUtils.showErrorMessage("Error: " + e.getMessage(), RequestContext.getCurrentInstance());
		}
	}
}
