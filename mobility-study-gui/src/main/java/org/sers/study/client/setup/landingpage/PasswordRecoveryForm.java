package org.sers.study.client.setup.landingpage;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@AuthorComment
@ManagedBean(eager = true)
@ViewScoped
public class PasswordRecoveryForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private String expectedParam;

	public void recoverPassword() throws Exception {/*
		try {
			UserService userService = ApplicationContextProvider.getBean(UserService.class);
			System.out.println("recoverPassword ... reached ...");
			User user = userService.getUserByEmail(this.expectedParam);
			if (user == null) {
				user = userService.findUserByAgentTerminalId(this.expectedParam);
			}
			if (user == null) {
				user = userService.findUserByPhoneNumber(this.expectedParam);
			}

			if (user != null) {
				PasswordRecoveryToken passwordRecoveryToken = new PasswordRecoveryToken();
				passwordRecoveryToken.setAccessToken(String.valueOf(System.currentTimeMillis()));
				passwordRecoveryToken.setYodimeId(
						(user.getAgentTerminalId() != null ? user.getAgentTerminalId() : user.getEmailAddress()));
				passwordRecoveryToken.setMemberEmail(user.getEmailAddress());
				PasswordRecoveryToken recoveryToken = ApplicationContextProvider
						.getBean(PasswordRecoveryTokenService.class).savePasswordRecoveryToken(passwordRecoveryToken);
				if (recoveryToken != null) {
					String recoveryLink = ApiUtil
							.getExternalPaymentUrl(
									(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
											.getRequest(),
									passwordRecoveryToken.getYodimeId(), passwordRecoveryToken.getAccessToken(),
									YodimeUtils.RECOVERY_LINK_PAGE);
					if (recoveryLink != null) {
						if (new UiUtils().sendPasswordChangeEmailLink(user, recoveryLink)) {
							UiUtils.showMessageBox(
									"An email with a password recovery link has been sent to your email : "
											+ user.getEmailAddress(),
									"Action Successful", RequestContext.getCurrentInstance());
						} else {
							UiUtils.showMessageBox(
									"Failed to send password recovery link to email :: " + user.getEmailAddress()
											+ " , Please contact support for assistance",
									"Action Failed", RequestContext.getCurrentInstance());
						}
					} else
						UiUtils.showMessageBox(
								"Empty activation link, Please try again or contact support for assistance",
								"Action Failed", RequestContext.getCurrentInstance());
				} else
					UiUtils.showMessageBox(
							"Failed to generate activation link, Please try again or contact support for assistance",
							"Action Failed", RequestContext.getCurrentInstance());
			} else {
				UiUtils.showMessageBox(
						"user with Email / phone number / YoDime Id: \"" + this.expectedParam + " \"  does not exist",
						"Action Failed", RequestContext.getCurrentInstance());
			}

			this.resetForm();
		} catch (Exception exe) {
			UiUtils.showMessageBox(exe.getMessage(), "Action Failed", RequestContext.getCurrentInstance());
			exe.printStackTrace();

		}
	*/}

	public void resetForm() {
		this.expectedParam = "";
	}

	/**
	 * @return the expectedParam
	 */
	public String getExpectedParam() {
		return expectedParam;
	}

	/**
	 * @param expectedParam
	 *            the expectedParam to set
	 */
	public void setExpectedParam(String expectedParam) {
		this.expectedParam = expectedParam;
	}

}