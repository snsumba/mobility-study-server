package org.sers.study.client.views.presenters;

import java.io.IOException;
import java.io.Serializable;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.exception.SessionExpiredException;

@AuthorComment
public interface SecurityValidator extends Serializable {

	/**
	 * 
	 * @param pageUrl
	 * @throws IOException
	 */
	public void redirectTo(String pageUrl) throws IOException;

	void enforceSecurity(String redirectPage, boolean allowIfAdmin, String[] roles)
			throws SessionExpiredException, IOException;
}
