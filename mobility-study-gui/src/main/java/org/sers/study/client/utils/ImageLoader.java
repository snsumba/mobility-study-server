package org.sers.study.client.utils;

import java.net.URL;

public class ImageLoader {

	private final String LOGO_NAME = "logo.png";

	private static volatile ImageLoader _instance;

	public static ImageLoader getInstance() {
		if (_instance == null) {
			synchronized (ImageLoader.class) {
				if (_instance == null)
					_instance = new ImageLoader();
			}
		}
		return _instance;
	}

	private ImageLoader() {
	}

	public String logoImagePath() {
		String url = getClass().getResource(LOGO_NAME).toString();
		String path = getClass().getResource(LOGO_NAME).getPath().replaceAll("%20", " ").replace("%20", " ");
		System.out.println("Path :: " + path);
		System.out.println("URL  :: " + url);
		return path;
	}

	public URL getYoDimeLogo() {
		return getClass().getResource(LOGO_NAME);
	}

}
