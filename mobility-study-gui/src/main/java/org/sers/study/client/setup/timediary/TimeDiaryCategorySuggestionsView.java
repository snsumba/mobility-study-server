package org.sers.study.client.setup.timediary;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.client.views.presenters.PaginatedTable;
import org.sers.study.model.TimeCategorySuggestion;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.User;
import org.sers.study.server.core.service.TimeCategorySuggestionService;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.server.shared.SharedAppData;

@AuthorComment
@ManagedBean
@ViewScoped
public class TimeDiaryCategorySuggestionsView extends PaginatedTable<TimeCategorySuggestion> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8413343075645160075L;
	private TimeCategorySuggestionService timeCategorySuggestionService;
	private User loggedInUser;

	@PostConstruct
	public void init() {
		loggedInUser = SharedAppData.getLoggedInUser();
		timeCategorySuggestionService = ApplicationContextProvider.getApplicationContext()
				.getBean(TimeCategorySuggestionService.class);
	}

	@Override
	public void reloadFromDB(int offset, int limit, Map<String, Object> filters) throws Exception {
		try {
			if (loggedInUser.hasPermission(PermissionConstants.PERM_ADMINISTRATOR)) {
				super.setTotalRecords(timeCategorySuggestionService
						.countTimeCategorySuggestions((filters != null && !filters.isEmpty() ? filters : null)));
				super.setDataModels(timeCategorySuggestionService.getTimeCategorySuggestions(offset, limit,
						(filters != null && !filters.isEmpty() ? filters : null)));
			}
		} catch (Exception e) {
			UiUtils.showErrorMessage("Error: " + e.getMessage(), RequestContext.getCurrentInstance());
		}
	}

}
