package org.sers.study.client.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.constants.Gender;

@AuthorComment
@FacesConverter("genderConverter")
public class GenderConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {

		if (arg2.equalsIgnoreCase(Gender.MALE.getName())) {
			return Gender.MALE;
		}

		if (arg2.equalsIgnoreCase(Gender.FEMALE.getName())) {
			return Gender.FEMALE;
		}
		
		if (arg2.equalsIgnoreCase(Gender.UNKNOWN.getName())) {
			return Gender.UNKNOWN;
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object gender) {
		if (gender != null)
			return ((Gender) gender).getName();
		return "--Select Gender--";
	}
}
