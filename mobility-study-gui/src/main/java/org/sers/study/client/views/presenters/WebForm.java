package org.sers.study.client.views.presenters;

import java.io.IOException;

import org.primefaces.context.RequestContext;
import org.sers.study.client.utils.UiUtils;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.BaseEntity;

@AuthorComment
public abstract class WebForm<T extends BaseEntity> extends FormPresenter<T>
		implements WebViewResolver {

	private static final long serialVersionUID = 1L;
	private boolean redirect;
	protected boolean showMessagePopups;
	protected String mobileMessage;

	/**
	 * 
	 */
	public WebForm() {
		super();
		this.redirect = true;
		this.showMessagePopups = true;
	}

	/**
	 * 
	 * @param redirect
	 * @param showMessagePopups
	 */
	public WebForm(boolean redirect, boolean showMessagePopups) {
		super();
		this.redirect = redirect;
		this.showMessagePopups = showMessagePopups;
	}

	/**
	 * Performs the necessary redirections to the parent view after saving.
	 * 
	 * @throws IOException
	 */
	public void redirectToView() throws IOException {
		super.redirectTo(getViewUrl());
	}

	public void save() throws Exception {
		try {
			persist();
			resetModal();
			if (redirect)
				super.redirectTo(getViewUrl());
			else {
				mobileMessage = "Successfully saved record.";
				UiUtils.showMessageBox(mobileMessage, "Action Successful",
						RequestContext.getCurrentInstance());
			}
		} catch (Exception e) {
			if (showMessagePopups)
				UiUtils.showMessageBox(e.getMessage(), "Action Failed",
						RequestContext.getCurrentInstance());
			mobileMessage = e.getMessage();
			e.printStackTrace();
		}

	}

	/**
	 * Do nothing in this abstract class. To be overridden by Subclasses that
	 * wish to set any other form properties in case the set model is not null
	 * i.e editing mode.
	 */
	@Override
	public void setFormProperties() {
		super.isEditing = true;
	}

	@Override
	public void resetModal() {
		super.isEditing = false;
	}

	/**
	 * @param redirect
	 *            the redirect to set
	 */
	public void setRedirect(boolean redirect) {
		this.redirect = redirect;
	}

	/**
	 * @return the redirect
	 */
	public boolean isRedirect() {
		return redirect;
	}

	/**
	 * @return the showMessagePopups
	 */
	public boolean isShowMessagePopups() {
		return showMessagePopups;
	}

	/**
	 * @return the mobileMessage
	 */
	public String getMobileMessage() {
		return mobileMessage;
	}

	/**
	 * @param showMessagePopups
	 *            the showMessagePopups to set
	 */
	public void setShowMessagePopups(boolean showMessagePopups) {
		this.showMessagePopups = showMessagePopups;
	}

	/**
	 * @param mobileMessage
	 *            the mobileMessage to set
	 */
	public void setMobileMessage(String mobileMessage) {
		this.mobileMessage = mobileMessage;
	}
}
