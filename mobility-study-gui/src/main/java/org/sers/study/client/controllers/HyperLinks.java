package org.sers.study.client.controllers;

import org.sers.study.annotations.AuthorComment;

@AuthorComment
public class HyperLinks {

	public static final String HOMEPAGE = "/pages/home/home.xhtml?faces-redirect=true";
	public static final String USER_ROLES = "/pages/security/userRoles.xhtml?faces-redirect=true";
	public static final String PROFILE_VIEW = "/pages/security/ProfileView.xhtml?faces-redirect=true";
	public static final String ROLESVIEW = "/pages/security/rolesView.xhtml";
	public static final String ROLESFORM = "/pages/security/rolesForm.xhtml?faces-redirect=true";
	public static final String USERSVIEW = "/pages/security/UsersView.xhtml?faces-redirect=true";
	public static final String USERFORM = "/pages/security/UserForm.xhtml?faces-redirect=true";
	public static final String COUNTRIES_UPLOAD = "/pages/countries/CountryUpload.xhtml?faces-redirect=true";
	public static final String COUNTRIES_VIEW = "/pages/countries/CountriesView.xhtml?faces-redirect=true";
	public static final String VIEW_MEMBER_PROFILE = "/pages/settings/profileView.xhtml?faces-redirect=true";

	public static final String VIEW_DURATION_INFO = "/pages/duration/DurationInformationView.xhtml?faces-redirect=true";
	public static final String VIEW_CAPTURED_LOCATIONS = "/pages/locations/CapturedLocationsView.xhtml?faces-redirect=true";
	public static final String VIEW_MAPPED_LOCATIONS = "/pages/locations/MappedLocationsView.xhtml?faces-redirect=true";
	public static final String VIEW_CIRCLER_MAPPED_LOCATIONS = "/pages/locations/CirclerMapView.xhtml?faces-redirect=true";

	public static final String VIEW_TIME_DIARY = "/pages/time-diary/TimeDiariesView.xhtml?faces-redirect=true";
	public static final String VIEW_TIME_DIARY_CATEGORY_SUGGESTIONS = "/pages/time-diary/TimeDiaryCategorySuggestionsView.xhtml?faces-redirect=true";
	public static final String VIEW_APP_TYPE = "/pages/app-type/AppTypeView.xhtml?faces-redirect=true";

	public static final String VIEW_DOWN_LOAD_CAPTURED_LOCATIONS = "/pages/reports/DownloadUserCapturedLocationsForm.xhtml?faces-redirect=true";
	public static final String VIEW_DOWN_LOAD_TIME_DIARY = "/pages/reports/DownloadUserTimeDiaryForm.xhtml?faces-redirect=true";
}
