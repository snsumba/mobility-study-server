package org.sers.study.server.api.models;

import java.io.Serializable;

public class ServerUpdate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pk;
	private String userId;
	private String responseStatus;
	private String responseMessage;
	private String durationInMinutes;

	public ServerUpdate() {
	}

	public ServerUpdate(String pk, String userId, String responseStatus, String responseMessage) {
		this.pk = pk;
		this.userId = userId;
		this.responseStatus = responseStatus;
		this.responseMessage = responseMessage;
	}

	public String getDurationInMinutes() {
		return durationInMinutes;
	}

	public void setDurationInMinutes(String durationInMinutes) {
		this.durationInMinutes = durationInMinutes;
	}

	public String getPk() {
		return pk;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
}