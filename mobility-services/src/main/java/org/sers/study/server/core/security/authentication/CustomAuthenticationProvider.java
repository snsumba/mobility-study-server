package org.sers.study.server.core.security.authentication;

import java.util.Calendar;

import org.sers.study.server.core.security.CustomUser;
import org.sers.study.server.core.security.service.CustomAuthenticationService;
import org.sers.study.server.core.security.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Extends the spring framework {@link CustomAuthenticationProvider} interface
 * so as to provide a custom authentication mechanism
 * 
 * @author Umar
 * 
 */
public class CustomAuthenticationProvider implements
		org.springframework.security.authentication.AuthenticationProvider {

	private CustomUserDetailsService customUserDetailsService;

	@Autowired
	private CustomAuthenticationService customAuthenticationService;

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.springframework.security.authentication.AuthenticationProvider#
	 * authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {

		UserDetails userDetails = getUserDetailsService().loadUserByUsername(
				(String) authentication.getPrincipal());
		if (userDetails != null) {
			if (customAuthenticationService.isValidUserPassword(
					((CustomUser) userDetails).getSystemUser(),
					(String) authentication.getCredentials())) {

				if (customAuthenticationService
						.isEnabledUser(((CustomUser) userDetails)
								.getSystemUser())) {

					/*
					 * set password reset flag.
					 */
					if (((CustomUser) userDetails).getSystemUser()
							.getDateOfLastPasswordChange() != null) {
						Calendar passwordDate = Calendar.getInstance();
						passwordDate.setTime(((CustomUser) userDetails)
								.getSystemUser().getDateOfLastPasswordChange());

						// add ninety days to the password date.
						passwordDate.add(Calendar.DAY_OF_MONTH, 90);

						if (passwordDate.before(Calendar.getInstance())) {
							((CustomUser) userDetails).getSystemUser()
									.setChangePassword(true);
						} else {
							((CustomUser) userDetails).getSystemUser()
									.setChangePassword(false);
						}

					}

					return new UsernamePasswordAuthenticationToken(userDetails,
							authentication.getCredentials(),
							userDetails.getAuthorities());
				} else {
					throw new AuthenticationServiceException(
							"Your account was disabled. Check with the adminstrator.");
				}
			} else {
				throw new AuthenticationServiceException("password don't match");
			}
		} else {
			throw new AuthenticationCredentialsNotFoundException("");
		}
	}

	@Override
	public boolean supports(Class<? extends Object> supportedClass) {
		if (supportedClass
				.getName()
				.equalsIgnoreCase(
						"org.springframework.security.authentication.UsernamePasswordAuthenticationToken")) {
			return true;
		}

		return false;
	}

	/**
	 * @return the customUserDetailsService
	 */
	public CustomUserDetailsService getUserDetailsService() {
		return customUserDetailsService;
	}

	/**
	 * @param customUserDetailsService
	 *            the customUserDetailsService to set
	 */
	@Autowired
	public void setUserDetailsService(
			CustomUserDetailsService customUserDetailsService) {
		this.customUserDetailsService = customUserDetailsService;
	}

	/**
	 * @return the customAuthenticationService
	 */
	public CustomAuthenticationService getAuthenticationService() {
		return customAuthenticationService;
	}

	/**
	 * @param customAuthenticationService
	 *            the customAuthenticationService to set
	 */
	public void setAuthenticationService(
			CustomAuthenticationService customAuthenticationService) {
		this.customAuthenticationService = customAuthenticationService;
	}
}
