package org.sers.study.server.core.dao;

import org.sers.study.model.security.User;

/**
 * A Data access interface that provides methods for performing CRUD operations
 * on the {@link User} objects <br/>
 * <br/>
 * 
 * The UserDAO interface abstracts the application from a particular
 * implementation of the CRUD operations on the entities
 * 
 * 
 * 
 */
public interface UserDao extends BaseDao<User> {

}
