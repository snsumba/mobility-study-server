package org.sers.study.server.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ApplicationController {

	/**
	 * handles the service login
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = { "/ServiceLogin" })
	public ModelAndView loginHandler(ModelMap model) {
		return new ModelAndView("loginView", model);
	}

	@RequestMapping(value = { "/PasswordRecovery" })
	public ModelAndView passwordRecoveryHandler(ModelMap model) {
		return new ModelAndView("PasswordRecovery", model);
	}

	@RequestMapping(value = { "/Index" })
	public ModelAndView indexHandler(ModelMap model) {
		return new ModelAndView("Index", model);
	}

	@RequestMapping(value = { "/SignUp" })
	public ModelAndView signUpHandler(ModelMap model) {
		return new ModelAndView("SignUp", model);
	}

	@RequestMapping(value = { "/ServiceLoginFailure" })
	public ModelAndView loginFailureHandler(ModelMap model) {
		model.put(WebConstants.ERROR_MESSAGE_MODEL_PARAM, "username or password is incorrect.");
		return new ModelAndView("loginView", model);
	}

	@RequestMapping(value = "/AccessDenied")
	public ModelAndView accessDeniedHandler(ModelMap model) {
		model.put(WebConstants.ERROR_MESSAGE_MODEL_PARAM,
				"you don't have sufficient priviledges to access this resource.");
		return new ModelAndView("loginView", model);
	}

	@RequestMapping(value = "/errors/404")
	public ModelAndView error404Handler() {
		return new ModelAndView("404");
	}

	@RequestMapping(value = { "/registrationSuccessful" })
	public ModelAndView registratioSuccessful(ModelMap model) {
		return new ModelAndView("registrationSuccessfulView", model);
	}
}
