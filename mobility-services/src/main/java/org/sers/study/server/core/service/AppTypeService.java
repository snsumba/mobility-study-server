package org.sers.study.server.core.service;

import java.util.List;
import java.util.Map;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.AppType;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.exception.ValidationFailedException;

@AuthorComment
public interface AppTypeService extends GenericServiceAdapter {

	/**
	 * saves a given appType
	 * 
	 * @param appType
	 * @return
	 * @throws ValidationFailedException
	 *             thrown when the validation of the appType fails.
	 */
	AppType saveAppType(AppType appType) throws ValidationFailedException;

	/**
	 * 
	 * @param appType
	 * @return
	 * @throws ValidationFailedException
	 */
	void updateAppType(AppType appType) throws ValidationFailedException;

	/**
	 * gets a list of all appTypes in the system
	 * 
	 * @return
	 */
	List<AppType> getAppTypes();

	/**
	 * 
	 * @param offset
	 * @param limit
	 * @param filters
	 * @return
	 */
	List<AppType> getAppTypes(int offset, int limit, Map<String, Object> filters);

	/**
	 * 
	 * @param filters
	 * @return
	 */
	int countAppTypes(Map<String, Object> filters);

	/**
	 * This method is used to
	 * 
	 * @param recordId
	 * @return
	 */
	AppType findAppTypeByOperationStatus(OperationStatus operationStatus);

}
