package org.sers.study.server.core.dao;

import org.sers.study.model.AppType;

/**
 * A Data access interface that provides methods for performing CRUD operations
 * on the {@link AppType} objects <br/>
 * <br/>
 * 
 * The AppTypeDAO interface abstracts the application from a particular
 * implementation of the CRUD operations on the entities
 * 
 * 
 * 
 */
public interface AppTypeDao extends BaseDao<AppType> {

}
