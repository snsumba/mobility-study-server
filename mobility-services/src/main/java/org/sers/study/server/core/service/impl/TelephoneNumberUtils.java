package org.sers.study.server.core.service.impl;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.exception.ValidationFailedException;

/**
 * Performs necessary phone number utilities like validating syntax and
 * composing the right phone number from incomplete digits.
 * 
 */
@AuthorComment
public class TelephoneNumberUtils {

	public static final int MINIMUM_NUMBER_LENGTH = 9;

	public static String getValidTelephoneNumber(String telephone) throws ValidationFailedException {
		System.out.println("Telephone : " + telephone);

		return telephone;
	}

	public static String getValidPersonalUserUploadTelephoneNumber(String telephone, String rowNumber)
			throws ValidationFailedException {
		return telephone;
	}

	public static String getValidMerchantUserUploadTelephoneNumber(String telephone, String rowNumber)
			throws ValidationFailedException {
		return telephone;
	}
}
