
/**
 *Jun 29, 2017
 *org.sers.study.server.api.services
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.server.core.service.api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.sers.study.model.TimeCategorySuggestion;
import org.sers.study.model.TimeDiary;
import org.sers.study.model.security.User;
import org.sers.study.server.api.models.AppTimeDiary;
import org.sers.study.server.api.utils.ApiUtil;
import org.sers.study.server.api.utils.AppRecordStatus;
import org.sers.study.server.core.service.TimeCategorySuggestionService;
import org.sers.study.server.core.service.TimeDiaryService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@Path("/RecordAppTimeDiaryService")
public class RecordAppTimeDiaryService {

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response apiResponse(@Context HttpServletRequest request, @Context HttpHeaders headers,
			AppTimeDiary appTimeDiary) throws JSONException {

		System.out.println("RecordAppTimeDiaryService .... ");
		UserService userService = ApplicationContextProvider.getBean(UserService.class);
		JSONObject jsonObject = new JSONObject();
		try {
			String userId = ApiUtil.decryptData(appTimeDiary.getUserId());
			User user = userService.findUserByUserId(userId);
			if (user != null) {
				System.out
						.println("RecordAppTimeDiaryService .... : id : " + appTimeDiary.getTimeCategorySuggestionId());
				TimeCategorySuggestion timeCategorySuggestion = ApplicationContextProvider
						.getBean(TimeCategorySuggestionService.class)
						.getTimeCategorySuggestionById(ApiUtil.decryptData(appTimeDiary.getTimeCategorySuggestionId()));

				TimeDiary timeDiary = new TimeDiary(user, ApiUtil.decryptData(appTimeDiary.getRecordId()),
						timeCategorySuggestion, ApiUtil.decryptData(appTimeDiary.getDate()),
						ApiUtil.decryptData(appTimeDiary.getStartTime()),
						ApiUtil.decryptData(appTimeDiary.getEndTime()));

				TimeDiary savedTimeDiary = ApplicationContextProvider.getBean(TimeDiaryService.class)
						.saveTimeDiary(timeDiary);

				if (savedTimeDiary != null) {
					jsonObject.put("status", ApiUtil.encryptData(AppRecordStatus.SYNCED.getStatus()));
					jsonObject.put("userId", ApiUtil.encryptData(userId));
					jsonObject.put("recordId", ApiUtil.encryptData(appTimeDiary.getRecordId()));
					jsonObject.put("date", ApiUtil.encryptData(String.valueOf(savedTimeDiary.getRegistrationDate())));
					jsonObject.put("startTime", ApiUtil.encryptData(String.valueOf(savedTimeDiary.getStartTime())));
					jsonObject.put("endTime", ApiUtil.encryptData(String.valueOf(savedTimeDiary.getEndTime())));
					jsonObject.put("description", ApiUtil.encryptData((timeDiary.getCategorySuggestion() != null
							? timeDiary.getCategorySuggestion().getSuggestion() : "n.a")));
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));
				} else {
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData("Failed to record user location"));
				}
			} else {
				jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
				jsonObject.put("responseMessage", ApiUtil.encryptData("Authentication failed, wrong username"));
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
			jsonObject.put("responseMessage", ApiUtil.encryptData(e1.getMessage()));
		}

		System.out.println("jsonObject : " + jsonObject);
		return Response.status(200).entity("" + jsonObject).build();
	}

}
