package org.sers.study.server.web.controllers;

public final class WebConstants {

	/**
	 * parameter name for the error message in the model
	 */
	public static final String ERROR_MESSAGE_MODEL_PARAM = "errorMessage";

	/**
	 * parameter name for the system message in the model
	 */
	public static final String SYSTEM_MESSAGE_MODEL_PARAM = "systemMessage";

	/**
	 * the default name of the country supported by this application.
	 */
	public static final String DEFAULT_COUNTRY_NAME = "Uganda";

	/**
	 * defines the default date format.
	 */
	public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";

	/**
	 * the session key used to reference the identifier of a user
	 */
	public static final String USER_SESSION_KEY = "4DC45EDE-215B-4CA7-BBE6-7170BAD20282";

	/**
	 * the session key used to reference the identifier of a role.
	 */
	public static final String ROLE_SESSION_KEY = "F38916B5-1C99-4679-9FD0-183959638A3F";

	/**
	 * the number of items to fetch per query.
	 */
	public static final int ITEMS_PER_PAGE = 25;

}
