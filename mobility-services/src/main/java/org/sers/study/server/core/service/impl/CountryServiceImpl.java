package org.sers.study.server.core.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.sers.study.server.core.dao.CountryDao;
import org.sers.study.server.core.service.CountryService;
import org.sers.study.server.shared.SharedAppData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Country;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

/**
 * Implements the {@link CountryService} interface.
 * 
 * @author Umar
 * 
 */
@Service("countryService")
@Transactional
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryDao countryDao;

	@Override
	public Object getObjectById(String id) {
		return countryDao.searchByPropertyEqual("id", id);
	}

	public Country saveCountry(Country country) throws ValidationFailedException {
		validateCountry(country);
		return countryDao.merge(country);
	}

	private void validateCountry(Country country) throws ValidationFailedException {
		if (StringUtils.isBlank(country.getName()) && StringUtils.isEmpty(country.getName())) {
			throw new ValidationFailedException("Country name is empty.");
		}
		if (StringUtils.isBlank(new Integer(country.getPostalCode()).toString())
				&& StringUtils.isEmpty(new Integer(country.getPostalCode()).toString())) {
			throw new ValidationFailedException("Postal code is empty.");
		}

		countryAlreadyExists(country);

	}

	private void countryAlreadyExists(Country country) throws ValidationFailedException {
		Search search = new Search();
		search.addFilterEqual("name", country.getName());
		search.addFilterEqual("postalCode", country.getPostalCode());
		search.addFilterEqual("activateStatus", country.getActivateStatus());
		search.addFilterEqual("deactivateStatus", country.getDeactivateStatus());
		search.addFilterEqual("editStatus", country.getEditStatus());
		search.addFilterEqual("viewStatus", country.getViewStatus());
		if (countryDao.count(search) > 0)
			throw new ValidationFailedException(
					country.getName() + " : " + country.getPostalCode() + " Already exists");
	}

	@Override
	public void updateCountry(Country country) throws ValidationFailedException {
		validateCountry(country);
		countryDao.update(country);
	}

	private void setFilters(Search search, String key, Map<String, Object> filters) {
		search.addFilterOr(Filter.ilike("name", "%" + filters.get(key).toString() + "%"));
	}

	@Override
	public List<Country> getCountries(int offset, int limit, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("name", false, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		return countryDao.search(search);
	}

	@Override
	public int countCountries(Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("name", false, true);

		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		return countryDao.count(search);
	}

	@Override
	public void save(List<Country> countries) throws SQLException {
		for (Country country : countries) {
			this.save(country);
		}
	}

	@Override
	public boolean save(Country country) throws SQLException {
		if (countryDoesNotExist(country.getName())) {
			countryDao.save(country);
			return false;
		}
		return true;
	}

	private boolean countryDoesNotExist(String name) {
		return (countryDao.searchUniqueByPropertyEqual("name", name) == null);
	}

	@Override
	public List<Country> getCountries() {
		return countryDao.searchByRecordStatus(RecordStatus.ACTIVE);
	}

	@Override
	public Country getCountryByName(String name) {
		return countryDao.searchUniqueByPropertyEqual("name", name);
	}

}