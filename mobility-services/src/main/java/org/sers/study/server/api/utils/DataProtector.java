package org.sers.study.server.api.utils;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class DataProtector {

	private static final String ALGORITHM = "AES";
	private static final int ITERATIONS = 2;
	public static final String API_KEY = "ed9anjq1e6ohmhc6";
	public static final String API_SALT = "1u6m7idf36gedjb3461eiee46ohp067b285idssj222lgrt6lu";

	public static String encryptWithKeyAndSalt(String value, String salt, String providedKey) {
		String eValue = value;
		try {
			Key key = generateKey(providedKey);
			Cipher c = Cipher.getInstance(ALGORITHM);
			c.init(Cipher.ENCRYPT_MODE, key);
			String valueToEnc = null;
			for (int i = 0; i < ITERATIONS; i++) {
				valueToEnc = salt + eValue;
				byte[] encValue = c.doFinal(valueToEnc.getBytes());
				eValue = new BASE64Encoder().encode(encValue);
			}
			//System.out.println("encryptWithKeyAndSalt : "+eValue);
			return eValue;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String decryptWithKeyAndSalt(String value, String salt, String providedKey) {
		String dValue = null;
		try {
			Key key = generateKey(providedKey);
			Cipher c = Cipher.getInstance(ALGORITHM);
			c.init(Cipher.DECRYPT_MODE, key);
			String valueToDecrypt = value;
			for (int i = 0; i < ITERATIONS; i++) {
				byte[] decordedValue = new BASE64Decoder().decodeBuffer(valueToDecrypt);
				byte[] decValue = c.doFinal(decordedValue);
				dValue = new String(decValue).substring(salt.length());
				valueToDecrypt = dValue;
			}
			System.out.println("decryptWithKeyAndSalt : "+dValue);
			return dValue;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	private static Key generateKey(String providedKey) throws Exception {
		return new SecretKeySpec(providedKey.getBytes(), ALGORITHM);
	}

	public static void main(String[] args) throws Exception {
		try {

			String password = "umar";
			String passwordEnc = DataProtector.encryptWithKeyAndSalt(password, DataProtector.API_SALT,
					DataProtector.API_KEY);
			System.out.println("Encrypted : " + passwordEnc);

			String passwordDec = DataProtector.decryptWithKeyAndSalt(passwordEnc, DataProtector.API_SALT,
					DataProtector.API_KEY);
			System.out.println("Decrypted : " + passwordDec);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@SuppressWarnings("unused")
	private static String encryptWithOnlyKey(String value, String providedKey) {
		String eValue = value;
		try {
			Key key = generateKey(providedKey);
			Cipher c = Cipher.getInstance(ALGORITHM);
			c.init(Cipher.ENCRYPT_MODE, key);
			byte[] encValue = c.doFinal(eValue.getBytes());
			eValue = new BASE64Encoder().encode(encValue);
			return eValue;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@SuppressWarnings("unused")
	private static String decryptWithOnlyKey(String value, String providedKey) {
		String valueToDecrypt = value;
		String dValue = null;
		try {
			Key key = generateKey(providedKey);
			Cipher c = Cipher.getInstance(ALGORITHM);
			c.init(Cipher.DECRYPT_MODE, key);
			byte[] decordedValue = new BASE64Decoder().decodeBuffer(valueToDecrypt);
			byte[] decValue = c.doFinal(decordedValue);
			dValue = new String(decValue);
			valueToDecrypt = dValue;
			return dValue;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
}