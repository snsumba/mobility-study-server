
/**
 *Jun 29, 2017
 *org.sers.study.server.api.services
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.server.core.service.api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.sers.study.model.DurationInfo;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.security.User;
import org.sers.study.server.api.models.ServerProfileUpdate;
import org.sers.study.server.api.utils.ApiUtil;
import org.sers.study.server.core.service.DurationInfoService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@Path("/AppUpdateProfileService")
public class AppUpdateProfileService {

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response apiResponse(@Context HttpServletRequest request, @Context HttpHeaders headers,
			ServerProfileUpdate serverProfileUpdate) throws JSONException {

		System.out.println("AppUpdateProfileService .... ");
		UserService userService = ApplicationContextProvider.getBean(UserService.class);
		JSONObject jsonObject = new JSONObject();
		try {
			User tobeSaved = null;
			User user = userService.findUserByUserId(ApiUtil.decryptData(serverProfileUpdate.getUserId()));
			if (user != null) {

				/*
				 * if (!StringUtils.isBlank(user.getApiToken())) { if
				 * (!ApiUtil.decryptData(serverProfileUpdate.getApiToken()).
				 * equals(user.getApiToken())) { throw new
				 * ValidationFailedException(
				 * "Invalid or Expired Access Token, Please logout and login again"
				 * ); } }
				 */
				user.setFirstName(ApiUtil.decryptData(serverProfileUpdate.getFirstName()));
				user.setLastName(ApiUtil.decryptData(serverProfileUpdate.getLastName()));
				user.setLocation(ApiUtil.decryptData(serverProfileUpdate.getLocation()));
				user.setUsername(ApiUtil.decryptData(serverProfileUpdate.getUsername()));
				user.setClearTextPassword(ApiUtil.decryptData(serverProfileUpdate.getPassword()));
				user.setPhoneNumber(ApiUtil.decryptData(serverProfileUpdate.getPhoneNumber()));
				user.setGender(ApiUtil.getUserGender(ApiUtil.decryptData(serverProfileUpdate.getGender())));
				tobeSaved = user;
			} else {
				User newUser = new User();
				newUser.setFirstName(ApiUtil.decryptData(serverProfileUpdate.getFirstName()));
				newUser.setLastName(ApiUtil.decryptData(serverProfileUpdate.getLastName()));
				newUser.setLocation(ApiUtil.decryptData(serverProfileUpdate.getLocation()));
				newUser.setUsername(ApiUtil.decryptData(serverProfileUpdate.getUsername()));
				newUser.setClearTextPassword(ApiUtil.decryptData(serverProfileUpdate.getPassword()));
				newUser.setPhoneNumber(ApiUtil.decryptData(serverProfileUpdate.getPhoneNumber()));
				newUser.setGender(ApiUtil.getUserGender(ApiUtil.decryptData(serverProfileUpdate.getGender())));
				tobeSaved = newUser;
			}

			User updatedUser = userService.saveUser(tobeSaved);
			if (updatedUser != null) {

				DurationInfo durationInfo = ApplicationContextProvider.getBean(DurationInfoService.class)
						.getDurationInfoByRecordstatus(RecordStatus.ACTIVE);
				jsonObject.put("Id", ApiUtil.encryptData(user.getId()));
				jsonObject.put("userId", ApiUtil.encryptData(user.getUserId()));
				jsonObject.put("updatedUsername", ApiUtil.encryptData(updatedUser.getUsername()));
				jsonObject.put("emailAddress", ApiUtil.encryptData(updatedUser.getEmailAddress()));
				jsonObject.put("firstName", ApiUtil.encryptData(updatedUser.getFirstName()));
				jsonObject.put("lastName", ApiUtil.encryptData(updatedUser.getLastName()));
				jsonObject.put("location", ApiUtil.encryptData(updatedUser.getLocation()));
				jsonObject.put("gender", ApiUtil.encryptData(updatedUser.getGender().getName()));
				jsonObject.put("dateOfBirth", ApiUtil.encryptData(String.valueOf(updatedUser.getDateOfBirth())));
				jsonObject.put("phoneNumber", ApiUtil.encryptData(updatedUser.getPhoneNumber()));
				jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
				jsonObject.put("responseMessage", ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));
				if (durationInfo != null) {
					jsonObject.put("durationInMinutes",
							ApiUtil.encryptData(String.valueOf(durationInfo.getDelayInMinutes())));
					jsonObject.put("distanceApart",
							ApiUtil.encryptData(String.valueOf(durationInfo.getDistanceApart())));
				} else {
					jsonObject.put("durationInMinutes", ApiUtil.encryptData(ApiUtil.DEFAULT_DURATION_IN_MINUTES));
					jsonObject.put("distanceApart", ApiUtil.encryptData(ApiUtil.DEFAULT_DISTANCE_APART));
				}
			} else {
				jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
				jsonObject.put("responseMessage",
						ApiUtil.encryptData("Failed to update updatedUser, please try again"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
			jsonObject.put("responseMessage", ApiUtil.encryptData(e.getMessage()));
		}

		System.out.println("jsonObject : " + jsonObject);
		return Response.status(200).entity("" + jsonObject).build();
	}
}
