
package org.sers.study.server.core.service.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sers.study.general.RandomStringGenerator;
import org.sers.study.model.AppType;
import org.sers.study.model.DurationInfo;
import org.sers.study.model.TimeCategorySuggestion;
import org.sers.study.model.constants.MethodType;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.security.User;
import org.sers.study.server.api.models.AppAccessDetails;
import org.sers.study.server.api.utils.ApiUtil;
import org.sers.study.server.core.security.service.CustomAuthenticationService;
import org.sers.study.server.core.service.AppTypeService;
import org.sers.study.server.core.service.DurationInfoService;
import org.sers.study.server.core.service.TimeCategorySuggestionService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@Path("/AppLoginService")
public class AppLoginService {

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response apiResponse(@Context HttpServletRequest request, @Context HttpHeaders headers,
			AppAccessDetails appAccessDetails) throws JSONException {

		System.out.println("AppLoginService .... ");
		JSONObject jsonObject = new JSONObject();
		try {
			UserService userService = ApplicationContextProvider.getBean(UserService.class);
			User user = userService.findUserByUsername(ApiUtil.decryptData(appAccessDetails.getUserId()));
			if (user != null) {
				boolean isPasswordValid = ApplicationContextProvider.getBean(CustomAuthenticationService.class)
						.isValidUserPassword(user, ApiUtil.decryptData(appAccessDetails.getPassword()));
				if (isPasswordValid) {
					DurationInfo durationInfo = ApplicationContextProvider.getBean(DurationInfoService.class)
							.getDurationInfoByRecordstatus(RecordStatus.ACTIVE);
					AppType appType = ApplicationContextProvider.getBean(AppTypeService.class)
							.findAppTypeByOperationStatus(OperationStatus.TRUE);
							// String apiToken = new
							// RandomStringGenerator().generateRandomString();

					// user.setApiToken(apiToken);
					userService.saveUser(user);
					jsonObject.put("Id", ApiUtil.encryptData(user.getId()));
					// jsonObject.put("apiToken",
					// ApiUtil.encryptData(apiToken));
					jsonObject.put("userId", ApiUtil.encryptData(user.getUserId()));
					jsonObject.put("appType", ApiUtil.encryptData((appType != null ? appType.getMethodType().getName()
							: MethodType.GPS_DETERMINED.getName())));
					jsonObject.put("username", ApiUtil.encryptData(user.getUsername()));
					jsonObject.put("emailAddress", ApiUtil.encryptData(user.getEmailAddress()));
					jsonObject.put("firstName", ApiUtil.encryptData(user.getFirstName()));
					jsonObject.put("lastName", ApiUtil.encryptData(user.getLastName()));
					jsonObject.put("location", ApiUtil.encryptData(user.getLocation()));
					jsonObject.put("gender", ApiUtil.encryptData(user.getGender().getName()));
					jsonObject.put("dateOfBirth", ApiUtil.encryptData(String.valueOf(user.getDateOfBirth())));
					jsonObject.put("phoneNumber", ApiUtil.encryptData(user.getPhoneNumber()));
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));
					if (durationInfo != null) {
						jsonObject.put("durationInMinutes",
								ApiUtil.encryptData(String.valueOf(durationInfo.getDelayInMinutes())));
						jsonObject.put("distanceApart",
								ApiUtil.encryptData(String.valueOf(durationInfo.getDistanceApart())));
					} else {
						jsonObject.put("durationInMinutes", ApiUtil.encryptData(ApiUtil.DEFAULT_DURATION_IN_MINUTES));
						jsonObject.put("distanceApart", ApiUtil.encryptData(ApiUtil.DEFAULT_DISTANCE_APART));
					}

					JSONArray jsonArray = new JSONArray();
					List<TimeCategorySuggestion> timeDiaryCategoryCategories = ApplicationContextProvider
							.getBean(TimeCategorySuggestionService.class).getTimeCategorySuggestions();
					if (timeDiaryCategoryCategories != null) {
						for (TimeCategorySuggestion timeDiaryCategory : timeDiaryCategoryCategories) {
							JSONObject suggestionsJsonObject = new JSONObject();
							suggestionsJsonObject.put("suggestion",
									ApiUtil.encryptData(timeDiaryCategory.getSuggestion()));
							suggestionsJsonObject.put("suggestionId", ApiUtil.encryptData(timeDiaryCategory.getId()));
							jsonArray.put(suggestionsJsonObject);
						}
					}
					jsonObject.put("categorySuggestions", jsonArray);

					System.out.println("jsonObject : " + jsonObject);
					return Response.status(200).entity("" + jsonObject).build();
				} else {

					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData("Authentication failed, wrong password"));

					System.out.println("jsonObject : " + jsonObject);
					return Response.status(200).entity("" + jsonObject).build();
				}
			} else {
				jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
				jsonObject.put("responseMessage", ApiUtil.encryptData("Authentication failed, wrong username"));
			}

		} catch (Exception e) {
			jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
			jsonObject.put("responseMessage", ApiUtil.encryptData(e.getMessage()));
		}
		System.out.println("jsonObject : " + jsonObject);
		return Response.status(200).entity("" + jsonObject).build();
	}
}
