package org.sers.study.server.core.dao.impl;

import org.sers.study.model.TimeCategorySuggestion;
import org.sers.study.server.core.dao.TimeCategorySuggestionDao;
import org.springframework.stereotype.Repository;

/**
 * Implements the {@link TimeCategorySuggestionDao} interface to provide CRUD
 * operations for the {@link TimeCategorySuggestion} object using hibernate as
 * the ORM strategy
 * 
 * @author Umar
 * 
 */
@Repository("timeCategorySuggestionDAO")
public class TimeCategorySuggestionDAOImpl extends BaseDAOImpl<TimeCategorySuggestion>
		implements TimeCategorySuggestionDao {

}
