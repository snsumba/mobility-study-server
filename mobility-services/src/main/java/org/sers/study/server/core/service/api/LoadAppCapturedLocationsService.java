
/**
 *Jun 29, 2017
 *org.sers.study.server.api.services
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.server.core.service.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.security.User;
import org.sers.study.server.api.models.AppAccessDetails;
import org.sers.study.server.api.utils.ApiUtil;
import org.sers.study.server.api.utils.AppRecordStatus;
import org.sers.study.server.core.service.CapturedLocationService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@Path("/LoadAppCapturedLocationsService")
public class LoadAppCapturedLocationsService {

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response apiResponse(@Context HttpServletRequest request, @Context HttpHeaders headers,
			AppAccessDetails appAccessDetails) throws JSONException {

		System.out.println("LoadAppCapturedLocationsService .... ");
		UserService userService = ApplicationContextProvider.getBean(UserService.class);
		JSONArray jsonArray = new JSONArray();
		String userId = ApiUtil.decryptData(appAccessDetails.getUserId());
		User user = userService.findUserByUserId(userId);
		if (user != null) {
			List<CapturedLocation> capturedLocations = ApplicationContextProvider.getBean(CapturedLocationService.class)
					.getCapturedLocations(user);
			if (capturedLocations != null) {
				for (CapturedLocation capturedLocation : capturedLocations) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("recordId", ApiUtil.encryptData(capturedLocation.getRecordId()));
					jsonObject.put("userId", ApiUtil.encryptData(userId));
					jsonObject.put("status", ApiUtil.encryptData(AppRecordStatus.SYNCED.getStatus()));
					jsonObject.put("date", ApiUtil.encryptData(String.valueOf(capturedLocation.getDateCreated())));
					jsonObject.put("placeName", ApiUtil.encryptData(capturedLocation.getPlaceName()));
					jsonObject.put("longitude", ApiUtil.encryptData(capturedLocation.getLongitude()));
					jsonObject.put("latitude", ApiUtil.encryptData(capturedLocation.getLatitude()));
					jsonObject.put("accuracy", ApiUtil.encryptData(capturedLocation.getAccuracy()));
					jsonObject.put("methodType", ApiUtil.encryptData(capturedLocation.getMethodType().getName()));
					jsonArray.put(jsonObject);
				}
			}
		}

		System.out.println("jsonArray : " + jsonArray);
		return Response.status(200).entity("" + jsonArray).build();
	}

}
