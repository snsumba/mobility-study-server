package org.sers.study.server.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SessionFactory;
import org.sers.study.server.core.dao.BaseDao;
import org.sers.study.server.core.security.service.impl.CustomSessionProvider;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.sers.study.model.BaseEntity;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.security.User;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import com.googlecode.genericdao.dao.jpa.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;

/**
 * Abstract Base class for all Data Access interface Implementations and
 * abstracts<br/>
 * </br/>
 * 
 * This class requires the hibernate {@link SessionFactory} <br/>
 * <br/>
 * 
 * This class also extends the {@link GenericDAOImpl} which is a hibernate
 * specific implementation of the {@link GenericDAO} interface. This allows our
 * implementing classes to use generics when providing CRUD operations
 * 
 * @author Umar
 * 
 * @param <T>
 */
public abstract class BaseDAOImpl<T> extends GenericDAOImpl<T, String>implements BaseDao<T> {
	protected EntityManager entityManager;

	// private Logger log = LoggerFactory.getLogger(BaseDAOImpl.class);

	@PersistenceContext
	@Override
	public void setEntityManager(EntityManager entityManager) {
		super.setEntityManager(entityManager);
		this.entityManager = entityManager;
	}

	@Autowired
	@Override
	public void setSearchProcessor(JPASearchProcessor searchProcessor) {
		super.setSearchProcessor(searchProcessor);
	}

	@Override
	public List<T> searchByPropertyEqual(String property, Object value) {
		Search search = new Search();
		search.addFilterEqual(property, value);
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		return search(search);
	}

	@Override
	@SuppressWarnings("unchecked")
	public T searchUniqueByPropertyEqual(String property, Object value) {
		Search search = new Search();
		search.addFilterEqual(property, value);
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		return (T) searchUnique(search);
	}

	@Override
	public void delete(T entity) {
		if (entity != null) {

		}
	}

	@Override
	public void update(T entity) {
		// save(entity);
		super.save(entity);
	}

	@Override
	public void add(T entity) {
		save(entity);
	}

	@Override
	public List<T> searchByPropertyEqual(String property, Object value, RecordStatus recordStatus) {
		Search search = new Search();
		search.addFilterEqual(property, value);
		search.addFilterEqual("recordStatus", recordStatus);
		return search(search);
	}

	@Override
	@SuppressWarnings("unchecked")
	public T searchUniqueByPropertyEqual(String property, Object value, RecordStatus recordStatus) {
		Search search = new Search();
		search.addFilterEqual(property, value);
		search.addFilterEqual("recordStatus", recordStatus);
		return (T) searchUnique(search);
	}

	@Override
	public List<T> searchByRecordStatus(RecordStatus recordStatus) {
		Search search = new Search();
		search.addFilterEqual("recordStatus", recordStatus);
		return search(search);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T save(T entity) {
		// if (entity == null)
		// return null;
		// User logged =
		// ApplicationContextProvider.getBean(CustomSessionProvider.class).getLoggedInUser();
		//
		// if (logged != null) {
		// if (entity instanceof BaseEntity) {
		// BaseEntity obj = (BaseEntity) entity;
		// if (StringUtils.isBlank(obj.getId()) ||
		// StringUtils.isEmpty(obj.getId())) {
		// // Attach entity to parent account so all children can see
		// // it
		// User parent = logged.getCreatedBy();
		// if (parent != null)
		// obj.setCreatedBy(parent);
		// else
		// obj.setCreatedBy(logged);
		// }
		// obj.setChangedBy(logged);
		// return super.save((T) obj);
		// }
		// }
		// return super.save(entity);
		if (entity == null)
			return null;
		return super.save(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T merge(T entity) {
		// if (entity == null)
		// return null;
		// User logged =
		// ApplicationContextProvider.getBean(CustomSessionProvider.class).getLoggedInUser();
		//
		// if (logged != null) {
		// if (entity instanceof BaseEntity) {
		// BaseEntity obj = (BaseEntity) entity;
		// if (StringUtils.isBlank(obj.getId()) ||
		// StringUtils.isEmpty(obj.getId())) {
		// // Attach entity to parent account so all children can see
		// // it
		// User parent = logged.getCreatedBy();
		// if (parent != null)
		// obj.setCreatedBy(parent);
		// else
		// obj.setCreatedBy(logged);
		// }
		// obj.setChangedBy(logged);
		//
		// return super.merge((T) obj);
		// }
		// }
		//
		// return super.merge(entity);
		//
		if (entity == null)
			return null;
		return super.merge(entity);
	}

	@Override
	public T directSave(T entity) {
		if (entity == null)
			return null;
		return super.save(entity);
	}

	@Override
	public T directMerge(T entity) {
		if (entity == null)
			return null;
		return super.merge(entity);
	}

	@Override
	public void directUpdate(T entity) {
		super.save(entity);
	}
}
