package org.sers.study.server.core.service;

import org.sers.study.model.CapturedLocation;
import org.sers.study.model.security.Country;
import org.sers.study.model.security.User;

public interface AbstractConverter {
	/**
	 * 
	 * @param id
	 * @return
	 */
	Country getCountryById(String id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	User getUserById(String id);

	/**
	 * This method is used to
	 * 
	 * @param id
	 * @return
	 */
	CapturedLocation getCapturedLocationById(String id);

	

}
