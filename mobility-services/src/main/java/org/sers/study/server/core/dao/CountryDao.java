package org.sers.study.server.core.dao;

import org.sers.study.model.security.Country;

/**
 * A Data access interface that provides methods for performing CRUD operations
 * on the {@link Country} objects <br/>
 * <br/>
 * 
 * The CountryDAO interface abstracts the application from a particular
 * implementation of the CRUD operations on the entities
 * 
 * 
 * 
 */
public interface CountryDao extends BaseDao<Country> {

}
