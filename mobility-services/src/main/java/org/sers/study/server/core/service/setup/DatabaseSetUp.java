package org.sers.study.server.core.service.setup;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.sers.study.model.security.Permission;

public class DatabaseSetUp {

	public DatabaseSetUp(SessionFactory sessionFactory) {
		if (!defaultDatasetsAlreadyExist(sessionFactory)) {
			new SecurityConfig(sessionFactory);
			new CountrySetup(sessionFactory);
		}
	}

	/**
	 * this method determines whether permissions already exist, hence
	 * initialization is not necessary at this point
	 * 
	 * @param sessionFactory
	 * @return boolean(true/false)
	 */
	public boolean defaultDatasetsAlreadyExist(SessionFactory sessionFactory) {
		sessionFactory.getCurrentSession().beginTransaction();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Permission.class);
		try {
			if (criteria.list().isEmpty()) {
				return Boolean.FALSE;
			}
		} catch (Exception exe) {
		}
		return Boolean.TRUE;
	}
}