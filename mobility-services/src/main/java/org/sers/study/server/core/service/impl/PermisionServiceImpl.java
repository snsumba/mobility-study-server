package org.sers.study.server.core.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sers.study.server.core.dao.PermissionDao;
import org.sers.study.server.core.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Permission;

/**
 * Implementation of the {@link PermissionService} interface.
 * 
 * @author Umar
 * 
 */
@Service("permissionService")
@Transactional
public class PermisionServiceImpl implements PermissionService {

	@Autowired
	private PermissionDao permissionDao;

	@Override
	public Object getObjectById(String id) {
		return permissionDao.searchUniqueByPropertyEqual("id", id);
	}

	@Override
	public List<Permission> getPermissions() {
		List<Permission> permissions = permissionDao.searchByRecordStatus(RecordStatus.ACTIVE);
		return permissions;
	}

	@Override
	public Permission savePermision(Permission permission) throws ValidationFailedException {
		if (StringUtils.isBlank(permission.getName()) || StringUtils.isEmpty(permission.getName())) {
			throw new ValidationFailedException("the name of the permission is not supplied");
		}

		if (StringUtils.isBlank(permission.getDescription()) || StringUtils.isEmpty(permission.getDescription())) {
			throw new ValidationFailedException("the description of the permission is not supplied");
		}

		if (StringUtils.isBlank(permission.getId()) || StringUtils.isEmpty(permission.getId())) {
			Permission existingPerm = permissionDao.searchUniqueByPropertyEqual("name", permission.getName());
			if (existingPerm != null) {
				throw new ValidationFailedException("a permission with the given name already exists");
			}
		}

		return permissionDao.save(permission);
	}

	@Override
	public Permission getPermissionByName(String permissionName) {
		return permissionDao.searchUniqueByPropertyEqual("name", permissionName);
	}
}
