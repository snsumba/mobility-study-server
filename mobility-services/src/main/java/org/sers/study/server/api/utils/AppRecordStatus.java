package org.sers.study.server.api.utils;

public enum AppRecordStatus {
    SYNCED("Synced"),
    UN_SYNCED("Un Synced");

    /**
     * Constructor with initial specified value
     *
     * @param status
     */
    AppRecordStatus(String status) {
        this.status = status;
    }

    private String status;

    /**
     * gets the title of the enumerated value
     *
     * @return
     */
    public String getStatus() {
        return this.status;
    }

    @Override
    public String toString() {
        return this.status;
    }
};