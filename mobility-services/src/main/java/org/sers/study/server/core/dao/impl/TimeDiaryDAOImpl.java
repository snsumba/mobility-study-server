package org.sers.study.server.core.dao.impl;

import org.sers.study.model.TimeDiary;
import org.sers.study.server.core.dao.TimeDiaryDao;
import org.springframework.stereotype.Repository;

/**
 * Implements the {@link TimeDiaryDao} interface to provide CRUD operations for
 * the {@link TimeDiary} object using hibernate as the ORM strategy
 * 
 * @author Umar
 * 
 */
@Repository("timeDiaryDAO")
public class TimeDiaryDAOImpl extends BaseDAOImpl<TimeDiary>implements TimeDiaryDao {

}
