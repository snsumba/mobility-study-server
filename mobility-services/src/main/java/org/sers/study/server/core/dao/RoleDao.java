package org.sers.study.server.core.dao;

import org.sers.study.model.security.Role;

/**
 * A Data access interface that provides methods for performing CRUD operations
 * on the {@link Role} objects <br/>
 * <br/>
 * 
 * The RoleDAO interface abstracts the application from a particular
 * implementation of the CRUD operations on the entities
 * 
 */
public interface RoleDao extends BaseDao<Role> {

}
