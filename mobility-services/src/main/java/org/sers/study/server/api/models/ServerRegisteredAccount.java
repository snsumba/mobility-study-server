package org.sers.study.server.api.models;

public class ServerRegisteredAccount extends ServerUpdate {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String recordId;
	private String status;
	private String date;
	private String firstName;
	private String lastName;
	private String gender;
	private String location;
	private String phoneNumber;
	private String username;
	private String password;
	private String dateOfBirth;

	public ServerRegisteredAccount() {
	}

	public ServerRegisteredAccount(String pk, String userId, String responseStatus, String responseMessage,
			String recordId, String status, String date, String firstName, String lastName, String gender,
			String location, String phoneNumber, String username, String password, String dateOfBirth) {
		super(pk, userId, responseStatus, responseMessage);
		this.recordId = recordId;
		this.status = status;
		this.date = date;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.location = location;
		this.phoneNumber = phoneNumber;
		this.username = username;
		this.password = password;
		this.dateOfBirth = dateOfBirth;
	}

	public String getStringOfBirth() {
		return dateOfBirth;
	}

	public void setStringOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getString() {
		return date;
	}

	public void setString(String date) {
		this.date = date;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
