package org.sers.study.server.core.dao;

import org.sers.study.model.security.Permission;

/**
 * A Data access interface that provides methods for performing CRUD operations
 * on the {@link Permission} objects <br/>
 * <br/>
 * 
 * The PermissionDAO interface abstracts the application from a particular
 * implementation of the CRUD operations on the entities
 * 
 * 
 * 
 */
public interface PermissionDao extends BaseDao<Permission> {

}
