package org.sers.study.server.core.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sers.study.server.core.dao.RoleDao;
import org.sers.study.server.core.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.exception.OperationFailedException;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Role;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

/**
 * Implements the {@link RoleService} interface.
 * 
 * @author Umar
 * 
 */
@Service("roleService")
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Override
	public Object getObjectById(String id) {
		return roleDao.searchUniqueByPropertyEqual("id", id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Role saveRole(Role role) throws ValidationFailedException {
		if (StringUtils.isBlank(role.getName()) || StringUtils.isEmpty(role.getName())) {
			throw new ValidationFailedException("the name of the role is not supplied");
		}

		if (StringUtils.isBlank(role.getDescription()) || StringUtils.isEmpty(role.getDescription())) {
			throw new ValidationFailedException("the description of the role is not supplied");
		}

		if (StringUtils.isBlank(role.getId()) || StringUtils.isEmpty(role.getId())) {
			Role existingRole = roleDao.searchUniqueByPropertyEqual("name", role.getName());
			if (existingRole != null) {
				throw new ValidationFailedException("a role is the given name already exists");
			}
		}

		if (role.getPermissions() == null || role.getPermissions().isEmpty())
			throw new ValidationFailedException("Role is missing permissions");

		return roleDao.save(role);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mak.cis.mohr.api.service.UserService#getRoles()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Role> getRoles() {
		Search search = new Search();
		/*
		 * search.addFilterNotIn("name", Role.DEFAULT_ADMIN_ROLE);
		 * search.addFilterNotIn("name", Role.NORMAL_USER_ROLE);
		 */search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		List<Role> roles = roleDao.search(search);
		return roles;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Role> findRolesByName(String searchString) {
		Search search = new Search();
		search.addFilter(new Filter("name", searchString, Filter.OP_LIKE));
		// search.addFilterNotIn("name", Role.DEFAULT_ADMIN_ROLE);
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		List<Role> roles = roleDao.search(search);
		return roles;
	}

	@Override
	@Transactional(readOnly = true)
	public Role getRoleByName(String searchString) {
		Search search = new Search();
		// search.addFilterNotIn("name", Role.DEFAULT_ADMIN_ROLE);
		search.addFilterEqual("name", searchString);
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		return roleDao.searchUnique(search);
	}

	@Override
	public void deleteRole(Role role) throws OperationFailedException {

	}

	@Override
	public List<Role> getRoles(String query) {
		Search search = new Search();
		search.addFilterILike("name", "%" + query + "%");
		// search.addFilterNotIn("name", Role.DEFAULT_ADMIN_ROLE);
		search.setDistinct(true);
		return roleDao.search(search);
	}
}
