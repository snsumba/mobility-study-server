
/**
 *Jun 29, 2017
 *org.sers.study.server.api.services
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.server.core.service.api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.security.User;
import org.sers.study.server.api.models.ServerCapturedLocation;
import org.sers.study.server.api.utils.ApiUtil;
import org.sers.study.server.api.utils.AppRecordStatus;
import org.sers.study.server.core.service.CapturedLocationService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@Path("/AppCapturedLocationService")
public class RecordAppCapturedLocationService {

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response apiResponse(@Context HttpServletRequest request, @Context HttpHeaders headers,
			ServerCapturedLocation serverCapturedLocation) throws JSONException {

		System.out.println("AppCapturedLocationService .... ");
		UserService userService = ApplicationContextProvider.getBean(UserService.class);
		JSONObject jsonObject = new JSONObject();
		try {
			String userId = ApiUtil.decryptData(serverCapturedLocation.getUserId());
			User systemUser = userService.findUserByUserId(ApiUtil.decryptData(userId));
			if (systemUser != null) {
				CapturedLocation capturedLocation = new CapturedLocation(
						ApiUtil.decryptData(serverCapturedLocation.getDate()),
						ApiUtil.decryptData(serverCapturedLocation.getPlaceName()),
						ApiUtil.decryptData(serverCapturedLocation.getLongitude()),
						ApiUtil.decryptData(serverCapturedLocation.getLatitude()),
						ApiUtil.decryptData(serverCapturedLocation.getAccuracy()), null,
						ApiUtil.getMethodTypeUsed(ApiUtil.decryptData(serverCapturedLocation.getMethodType())),
						systemUser);
				capturedLocation.setRecordId(ApiUtil.decryptData(serverCapturedLocation.getRecordId()));

				CapturedLocation savedLocation = ApplicationContextProvider.getBean(CapturedLocationService.class)
						.saveCapturedLocation(capturedLocation);
				if (savedLocation != null) {
					jsonObject.put("status", ApiUtil.encryptData(AppRecordStatus.SYNCED.getStatus()));
					jsonObject.put("userId", ApiUtil.encryptData(userId));
					jsonObject.put("recordId", ApiUtil.encryptData(savedLocation.getRecordId()));
					jsonObject.put("date",
							ApiUtil.encryptData(String.valueOf(capturedLocation.getAppRegistrationDate())));
					jsonObject.put("placeName", ApiUtil.encryptData(capturedLocation.getPlaceName()));
					jsonObject.put("longitude", ApiUtil.encryptData(capturedLocation.getLongitude()));
					jsonObject.put("latitude", ApiUtil.encryptData(capturedLocation.getLatitude()));
					jsonObject.put("accuracy", ApiUtil.encryptData(capturedLocation.getAccuracy()));
					jsonObject.put("methodType", ApiUtil.encryptData(capturedLocation.getMethodType().getName()));
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));
				} else {
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData("Failed to record user location"));
				}
			} else {
				jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
				jsonObject.put("responseMessage", ApiUtil.encryptData("Authentication failed, wrong username"));
			}

		} catch (Exception e1) {
			e1.printStackTrace();
			jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
			jsonObject.put("responseMessage", ApiUtil.encryptData(e1.getMessage()));
		}

		System.out.println("jsonObject : " + jsonObject);
		return Response.status(200).entity("" + jsonObject).build();
	}

}
