package org.sers.study.server.core.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * This class provides access to the spring application context for classes that
 * are outside the spring application context.
 * 
 * @author kdeo
 * 
 */
@Component("applicationContextProvider")
public class ApplicationContextProvider implements ApplicationContextAware {

	private static ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		ApplicationContextProvider.applicationContext = applicationContext;
	}

	/**
	 * gets the application context
	 * 
	 * @return
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * Return the bean instance that uniquely matches the given object type, if
	 * any.
	 * 
	 * @param type
	 *            type the bean must match; can be an interface or superclass.
	 *            null is disallowed.
	 * @return an instance of the single bean matching the required type
	 */
	public static <T> T getBean(Class<T> type) {
		return ApplicationContextProvider.applicationContext.getBean(type);
	}
}
