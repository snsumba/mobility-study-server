
/**
 *Sep 13, 2017
 *org.sers.study.server.core.service.api
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.server.core.service.api;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sers.study.model.TimeCategorySuggestion;
import org.sers.study.model.TimeDiary;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.User;
import org.sers.study.server.api.models.AppTimeDiary;
import org.sers.study.server.api.models.ListAppTimeDiary;
import org.sers.study.server.api.utils.ApiUtil;
import org.sers.study.server.api.utils.AppRecordStatus;
import org.sers.study.server.core.service.TimeCategorySuggestionService;
import org.sers.study.server.core.service.TimeDiaryService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@Path("/RecordAppListOfTimeDiariesService")
public class RecordAppListOfTimeDiariesService {

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response apiResponse(@Context HttpServletRequest request, @Context HttpHeaders headers,
			ListAppTimeDiary appTimeDiary) throws JSONException {

		System.out.println("RecordAppTimeDiaryService .... ");
		UserService userService = ApplicationContextProvider.getBean(UserService.class);
		JSONObject jsonObject = new JSONObject();
		try {

			String userId = ApiUtil.decryptData(appTimeDiary.getUserId());
			User user = userService.findUserByUserId(userId);
			if (user != null) {
				/*
				 * if (!StringUtils.isBlank(user.getApiToken())) { if
				 * (!ApiUtil.decryptData(appTimeDiary.getApiToken()).equals(user
				 * .getApiToken())) { throw new ValidationFailedException(
				 * "Invalid or Expired Access Token, Please logout and login again"
				 * ); } }
				 */
				List<AppTimeDiary> appTimeDiaries = appTimeDiary.getAppTimeDiaries();
				if (appTimeDiaries != null) {
					if (appTimeDiaries.size() > 0) {
						List<AppTimeDiary> successfulAppTimeDiaries = new ArrayList<AppTimeDiary>();
						List<AppTimeDiary> failedAppTimeDiaries = new ArrayList<AppTimeDiary>();
						for (AppTimeDiary timeDiaryRecord : appTimeDiaries) {
							TimeCategorySuggestion timeCategorySuggestion = ApplicationContextProvider
									.getBean(TimeCategorySuggestionService.class).getTimeCategorySuggestionById(
											ApiUtil.decryptData(timeDiaryRecord.getTimeCategorySuggestionId()));

							TimeDiary timeDiary = new TimeDiary(user,
									ApiUtil.decryptData(timeDiaryRecord.getRecordId()), timeCategorySuggestion,
									ApiUtil.decryptData(timeDiaryRecord.getDate()),
									ApiUtil.decryptData(timeDiaryRecord.getStartTime()),
									ApiUtil.decryptData(timeDiaryRecord.getEndTime()));
							TimeDiary savedTimeDiary;
							try {
								savedTimeDiary = ApplicationContextProvider.getBean(TimeDiaryService.class)
										.saveTimeDiary(timeDiary);
								if (savedTimeDiary != null) {
									successfulAppTimeDiaries.add(timeDiaryRecord);
								} else
									failedAppTimeDiaries.add(timeDiaryRecord);
							} catch (ValidationFailedException e) {
								e.printStackTrace();
								failedAppTimeDiaries.add(timeDiaryRecord);
							}
						}

						JSONArray successJsonArray = new JSONArray();
						if (successfulAppTimeDiaries != null) {
							for (AppTimeDiary successfulAppTimeDiary : successfulAppTimeDiaries) {
								JSONObject successfulAppTimeDiaryJsonObject = new JSONObject();
								successfulAppTimeDiaryJsonObject.put("status",
										ApiUtil.encryptData(AppRecordStatus.SYNCED.getStatus()));
								successfulAppTimeDiaryJsonObject.put("userId", ApiUtil.encryptData(userId));
								successfulAppTimeDiaryJsonObject.put("recordId", successfulAppTimeDiary.getRecordId());
								successfulAppTimeDiaryJsonObject.put("date", successfulAppTimeDiary.getDate());
								successfulAppTimeDiaryJsonObject.put("startTime",
										successfulAppTimeDiary.getStartTime());
								successfulAppTimeDiaryJsonObject.put("endTime", successfulAppTimeDiary.getEndTime());
								successfulAppTimeDiaryJsonObject.put("description", ApiUtil.encryptData("n.a"));
								successfulAppTimeDiaryJsonObject.put("responseStatus",
										ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
								successfulAppTimeDiaryJsonObject.put("responseMessage",
										ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));

								successJsonArray.put(successfulAppTimeDiaryJsonObject);
							}
						}

						JSONArray failedJsonArray = new JSONArray();
						if (failedAppTimeDiaries != null) {
							for (AppTimeDiary failedAppTimeDiary : failedAppTimeDiaries) {
								JSONObject failedAppTimeDiaryJsonObject = new JSONObject();

								failedAppTimeDiaryJsonObject.put("status",
										ApiUtil.encryptData(AppRecordStatus.UN_SYNCED.getStatus()));
								failedAppTimeDiaryJsonObject.put("userId", ApiUtil.encryptData(userId));
								failedAppTimeDiaryJsonObject.put("recordId", failedAppTimeDiary.getRecordId());
								failedAppTimeDiaryJsonObject.put("date", failedAppTimeDiary.getDate());
								failedAppTimeDiaryJsonObject.put("startTime", failedAppTimeDiary.getStartTime());
								failedAppTimeDiaryJsonObject.put("endTime", failedAppTimeDiary.getEndTime());
								failedAppTimeDiaryJsonObject.put("description", ApiUtil.encryptData("n.a"));
								failedAppTimeDiaryJsonObject.put("responseStatus",
										ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
								failedAppTimeDiaryJsonObject.put("responseMessage",
										ApiUtil.encryptData("Failed to record user location"));

								failedJsonArray.put(failedAppTimeDiaryJsonObject);
							}
						}

						jsonObject.put("failedRecords", failedJsonArray);
						jsonObject.put("successfulRecords", successJsonArray);
						jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
						jsonObject.put("responseMessage", ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));

					} else {
						jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
						jsonObject.put("responseMessage", ApiUtil.encryptData("No Time diary data to sync"));
					}
				} else {
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData("No Time diary data to sync"));
				}

			} else {
				jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
				jsonObject.put("responseMessage", ApiUtil.encryptData("Authentication failed, wrong username"));
			}
		} catch (Exception e) {
			jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
			jsonObject.put("responseMessage", ApiUtil.encryptData(e.getMessage()));
		}

		System.out.println("jsonObject : " + jsonObject);
		return Response.status(200).entity("" + jsonObject).build();
	}

}
