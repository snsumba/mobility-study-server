package org.sers.study.server.core.dao.impl;

import org.sers.study.model.AppType;
import org.sers.study.server.core.dao.AppTypeDao;
import org.springframework.stereotype.Repository;

/**
 * Implements the {@link AppTypeDao} interface to provide CRUD operations for
 * the {@link AppType} object using hibernate as the ORM strategy
 * 
 * @author Umar
 * 
 */
@Repository("appTypeDAO")
public class AppTypeDAOImpl extends BaseDAOImpl<AppType>implements AppTypeDao {

}
