package org.sers.study.server.core.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Country;

/**
 * 
 * @author Umar
 * 
 */
public interface CountryService extends GenericServiceAdapter {

	/**
	 * saves a given country
	 * 
	 * @param country
	 * @return
	 * @throws ValidationFailedException
	 *             thrown when the validation of the country fails.
	 */
	Country saveCountry(Country country) throws ValidationFailedException;

	/**
	 * 
	 * @param countries
	 * @throws SQLException
	 */
	void save(List<Country> countries) throws SQLException;

	/**
	 * Adds a country to the database.
	 * 
	 * @param country
	 * @return true if a country already exists
	 * @throws SQLException
	 */
	boolean save(Country country) throws SQLException;

	/**
	 * 
	 * @param country
	 * @return
	 * @throws ValidationFailedException
	 */
	void updateCountry(Country country) throws ValidationFailedException;

	/**
	 * 
	 * @return
	 */
	List<Country> getCountries();

	/**
	 * 
	 * @param name
	 * @return
	 */
	Country getCountryByName(String name);

	/**
	 * 
	 * @param offset
	 * @param limit
	 * @param filters
	 * @return
	 */
	List<Country> getCountries(int offset, int limit, Map<String, Object> filters);

	/**
	 * 
	 * @param filters
	 * @return
	 */
	int countCountries(Map<String, Object> filters);

}
