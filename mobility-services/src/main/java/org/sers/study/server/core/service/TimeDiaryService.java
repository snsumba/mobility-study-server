package org.sers.study.server.core.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.TimeDiary;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.User;

@AuthorComment
public interface TimeDiaryService extends GenericServiceAdapter {

	/**
	 * saves a given timeDiary
	 * 
	 * @param timeDiary
	 * @return
	 * @throws ValidationFailedException
	 *             thrown when the validation of the timeDiary fails.
	 */
	TimeDiary saveTimeDiary(TimeDiary timeDiary) throws ValidationFailedException;

	/**
	 * 
	 * @param timeDiary
	 * @return
	 * @throws ValidationFailedException
	 */
	void updateTimeDiary(TimeDiary timeDiary) throws ValidationFailedException;

	/**
	 * 
	 * @param offset
	 * @param limit
	 * @param filters
	 * @return
	 */
	List<TimeDiary> getTimeDiaries(int offset, int limit, Map<String, Object> filters);

	/**
	 * 
	 * @param filters
	 * @return
	 */
	int countTimeDiaries(Map<String, Object> filters);

	/**
	 * 
	 * This method is used to
	 * 
	 * @param user
	 * @return
	 */
	TimeDiary findTimeDiaryByUser(User user);

	/**
	 * This method is used to
	 * 
	 * @param timeDiary
	 * @throws ValidationFailedException
	 */
	void deleteTimeDiary(TimeDiary timeDiary) throws ValidationFailedException;

	/**
	 * This method is used to
	 * 
	 * @return
	 */
	List<TimeDiary> getAllTimeDiaries();

	/**
	 * This method is used to
	 * 
	 * @param user
	 * @return
	 */
	List<TimeDiary> getTimeDiaries(User user, Date startDate, Date endDate);

	/**
	 * This method is used to
	 * 
	 * @param user
	 * @param offset
	 * @param limit
	 * @param filters
	 * @return
	 */
	List<TimeDiary> getTimeDiaries(User user, int offset, int limit, Map<String, Object> filters);

	/**
	 * This method is used to
	 * 
	 * @param user
	 * @param filters
	 * @return
	 */
	int countTimeDiaries(User user, Map<String, Object> filters);

	/**
	 * This method is used to
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<TimeDiary> getTimeDiaries(Date startDate, Date endDate);

	/**
	 * This method is used to 
	 * @return
	 */
	List<TimeDiary> getTimeDiaries();

	/**
	 * This method is used to 
	 * @param user
	 * @return
	 */
	List<TimeDiary> getTimeDiaries(User user);

}
