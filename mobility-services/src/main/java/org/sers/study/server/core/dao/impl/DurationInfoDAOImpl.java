package org.sers.study.server.core.dao.impl;

import org.sers.study.model.DurationInfo;
import org.sers.study.server.core.dao.DurationInfoDao;
import org.springframework.stereotype.Repository;

/**
 * Implements the {@link DurationInfoDao} interface to provide CRUD operations
 * for the {@link DurationInfo} object using hibernate as the ORM strategy
 * 
 * @author Umar
 * 
 */
@Repository("durationInfoDAO")
public class DurationInfoDAOImpl extends BaseDAOImpl<DurationInfo>implements DurationInfoDao {

}
