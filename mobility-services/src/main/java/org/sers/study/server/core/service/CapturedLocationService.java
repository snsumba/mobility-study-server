package org.sers.study.server.core.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.User;

@AuthorComment
public interface CapturedLocationService extends GenericServiceAdapter {

	/**
	 * saves a given capturedLocation
	 * 
	 * @param capturedLocation
	 * @return
	 * @throws ValidationFailedException
	 *             thrown when the validation of the capturedLocation fails.
	 */
	CapturedLocation saveCapturedLocation(CapturedLocation capturedLocation) throws ValidationFailedException;

	/**
	 * 
	 * @param capturedLocation
	 * @return
	 * @throws ValidationFailedException
	 */
	void updateCapturedLocation(CapturedLocation capturedLocation) throws ValidationFailedException;

	/**
	 * 
	 * This method is used to
	 * 
	 * @param placeName
	 * @return
	 */
	List<CapturedLocation> getCapturedLocationsByPlaceName(String placeName);

	/**
	 * 
	 * @param offset
	 * @param limit
	 * @param filters
	 * @return
	 */
	List<CapturedLocation> getCapturedLocations(int offset, int limit, Map<String, Object> filters);

	/**
	 * 
	 * @param filters
	 * @return
	 */
	int countCapturedLocations(Map<String, Object> filters);

	/**
	 * 
	 * This method is used to
	 * 
	 * @param user
	 * @return
	 */
	CapturedLocation findCapturedLocationByUser(User user);

	/**
	 * This method is used to
	 * 
	 * @param capturedLocation
	 * @throws ValidationFailedException
	 */
	void deleteCapturedLocation(CapturedLocation capturedLocation) throws ValidationFailedException;

	/**
	 * This method is used to
	 * 
	 * @return
	 */
	List<CapturedLocation> getAllCapturedLocations();

	/**
	 * This method is used to
	 * 
	 * @param value
	 * @return
	 */
	List<CapturedLocation> getAllCapturedLocations(String value);

	/**
	 * This method is used to
	 * 
	 * @param user
	 * @return
	 */
	List<CapturedLocation> getCapturedLocations(User user, Date startDate, Date endDate);

	/**
	 * This method is used to
	 * 
	 * @param user
	 * @param placeName
	 * @return
	 */
	List<CapturedLocation> getCapturedLocationsByPlaceName(User user, String placeName);

	/**
	 * This method is used to
	 * 
	 * @param user
	 * @param value
	 * @return
	 */
	List<CapturedLocation> getAllCapturedLocations(User user, String value);

	/**
	 * This method is used to
	 * 
	 * @param recordId
	 * @return
	 */
	CapturedLocation findCapturedLocationByRecordId(String recordId);

	/**
	 * This method is used to
	 * 
	 * @param user
	 * @param offset
	 * @param limit
	 * @param filters
	 * @return
	 */
	List<CapturedLocation> getCapturedLocations(User user, int offset, int limit, Map<String, Object> filters);

	/**
	 * This method is used to
	 * 
	 * @param user
	 * @param filters
	 * @return
	 */
	int countCapturedLocations(User user, Map<String, Object> filters);

	/**
	 * This method is used to
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<CapturedLocation> getCapturedLocations(Date startDate, Date endDate);

	/**
	 * This method is used to 
	 * @param user
	 * @return
	 */
	List<CapturedLocation> getCapturedLocations(User user);

	/**
	 * This method is used to 
	 * @return
	 */
	List<CapturedLocation> getCapturedLocations();

}
