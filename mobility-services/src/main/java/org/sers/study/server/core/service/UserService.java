package org.sers.study.server.core.service;

import java.util.List;
import java.util.Map;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.exception.OperationFailedException;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Permission;
import org.sers.study.model.security.Role;
import org.sers.study.model.security.User;

@AuthorComment
public interface UserService {

	/**
	 * gets users in the system with a specified query
	 * 
	 * @param query
	 * @return
	 */
	List<User> getUsers(String query);

	/**
	 * gets a user based on their username
	 * 
	 * @param username
	 *            string for login name
	 * @return User or null if no much found
	 */
	User findUserByUsername(String username);

	/**
	 * 
	 * @param permissionName
	 * @return
	 */
	Permission getPermissionByName(String permissionName);

	/**
	 * saves a given user
	 * 
	 * @param user
	 * @return
	 * @throws ValidationFailedException
	 *             thrown when the validation of the user fails.
	 */
	User saveUser(User user) throws ValidationFailedException;

	/**
	 * gets a list of all users in the system
	 * 
	 * @return
	 */
	List<User> getUsers() throws OperationFailedException;

	/**
	 * deletes a given user from the system
	 * 
	 * @param user
	 * @throws OperationFailedException
	 *             thrown when the delete operation fails
	 */
	void deleteUser(User user) throws OperationFailedException;

	/**
	 * gets roles in the system with a specified query
	 * 
	 * @param query
	 * @return
	 */
	List<Role> getRoles(String query);

	/**
	 * saves a given role
	 * 
	 * @param role
	 * @return
	 * @throws ValidationFailedException
	 *             thrown when the validation of the user fails.
	 */
	Role saveRole(Role role) throws ValidationFailedException;

	/**
	 * gets a list of all
	 * 
	 * @return
	 */
	List<Role> getRoles();

	/**
	 * deletes a given role from the system
	 * 
	 * @param role
	 * @throws OperationFailedException
	 *             thrown when the delete operation fails.
	 */
	void deleteRole(Role role) throws OperationFailedException;

	/**
	 * gets a list of all permissions
	 * 
	 * @return
	 */
	List<Permission> getPermissions();

	/**
	 * saves a given {@link Permission}
	 * 
	 * @param permission
	 * @throws ValidationFailedException
	 *             thrown when the validation of the user fails.
	 */
	Permission savePermision(Permission permission) throws ValidationFailedException;

	/**
	 * finds users whose username(s) are like the query provided. This means
	 * that if a query like 'ct' is provided, all users who begin with the ct
	 * characters are returned
	 * 
	 * @param query
	 * @return
	 */
	List<User> findUsersByUsername(String query);

	/**
	 * finds users whose username(s) are like the query provided. This means
	 * that if a query like 'ct' is provided, all users who begin with the ct
	 * characters are returned
	 * 
	 * @param query
	 * @return
	 */
	List<User> findLockedUsersByUsername(String query);

	/**
	 * gets a user with a given Id
	 * 
	 * @param userId
	 * @return
	 */
	User getUserById(String userId);

	/**
	 * @param text
	 * @return
	 */
	Role getRoleById(String text);

	/**
	 * finds roles whose name(s) are like the searchString provided. This means
	 * that if a search string like 'ct' is provided, all roles whose names
	 * begin with ct characters are returned
	 * 
	 * @param searchString
	 * @return
	 */
	List<Role> findRolesByName(String searchString);

	/**
	 * gets the permission with the given id
	 * 
	 * @param id
	 * @return
	 */
	Permission getPermissionById(String id);

	/**
	 * gets a list of users on a given page number
	 * 
	 * @param pageNo
	 * @return
	 */
	List<User> getUsers(Integer pageNo);

	/**
	 * gets a list of roles on a given page number
	 * 
	 * @param pageNo
	 * @return
	 */
	List<Role> getRolesByPage(Integer pageNo);

	/**
	 * 
	 * @return
	 */
	int getTotalNumberOfUsers();

	/**
	 * gets a list of users that contain a given query in their name
	 * 
	 * @param query
	 * @return searched user
	 */
	List<User> searchUserByLoginName(String query);

	/**
	 * gets a paged list of users that contain a given query in their name
	 * 
	 * @param query
	 * @param pageNo
	 * @return
	 */
	List<User> searchUserByLoginName(String query, Integer pageNo);

	/**
	 * gets total number of users who meet the search criteria
	 * 
	 * @param query
	 * @return
	 */
	int getNumberOfUsersInSearch(String query);

	/**
	 * gets a list of users based on a role with the given name.
	 * 
	 * @param roleName
	 * @return
	 */
	List<User> getUsersByRoleName(String roleName);

	/**
	 * gets the username with the given username
	 * 
	 * @param username
	 * @return
	 */
	User getUserByUsername(String username);

	/**
	 * changes the password for the given user. Using the given clear text
	 * password
	 * 
	 * @param password
	 *            new password for the user.
	 * @param user
	 *            the user whose password is to be changed.
	 * @throws ValidationFailedException
	 */
	void changePassword(String password, User user) throws ValidationFailedException;

	/**
	 * changes the password for the given user using the given clear text
	 * password and sends a notification to the user
	 * 
	 * @param password
	 *            new password for the user
	 * @param user
	 *            the user whose password is to be changed
	 * @param sendChangePasswordNotification
	 *            whether to send a change password notification.
	 * @throws ValidationFailedException
	 */
	User changePassword(String password, User user, boolean sendChangePasswordNotification)
			throws ValidationFailedException;

	User changePassword(String password, User user, boolean sendChangePasswordNotification,
			boolean enforcePasswordPolicy) throws ValidationFailedException;

	/**
	 * gets the list of users starting at the given offset and ending at the
	 * given limit.
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 */
	List<User> getUsers(int offset, int limit);

	/**
	 * gets the list of roles starting at the given offset and ending at the
	 * given limit.
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 */
	List<Role> getRoles(int offset, int limit);

	/**
	 * gets the total number of roles in the system.
	 * 
	 * @return
	 */
	int getTotalNumberOfRoles();

	/**
	 * gets the roles for a given user starting at the given offset and ending
	 * at the given limit.
	 * 
	 * @param user
	 * @param offset
	 * @param limit
	 * @return
	 */
	List<Role> getRoles(User user, int offset, int limit);

	/**
	 * gets the total number of roles for the given user.
	 * 
	 * @param user
	 * @return
	 */
	int getTotalNumberOfRoles(User user);

	/**
	 * 
	 * @param query
	 * @return
	 */
	int getTotalNumberOfUsers(String query);

	/**
	 * 
	 * @param query
	 * @return
	 */
	int getTotalNumberOfRoles(String query);

	Object getObjectById(String id);

	Role getRoleByRoleName(String name);

	/**
	 * @param offset
	 * @param limit
	 * @param filters
	 * @return
	 */
	List<User> getUsers(int offset, int limit, Map<String, Object> filters);

	/**
	 * @param filters
	 * @return
	 */
	int countUsers(Map<String, Object> filters);

	/**
	 * @param memberEmail
	 * @return
	 */
	User getUserByEmail(String memberEmail);

	/**
	 * 
	 * @param users
	 * @throws ValidationFailedException
	 */
	void saveAdminUser(List<User> users) throws ValidationFailedException;

	/**
	 * This method is used to
	 * 
	 * @param email
	 * @return
	 */
	User findUserByEmail(String email);

	/**
	 * This method is used to
	 * 
	 * @param phoneNumber
	 * @return
	 */
	User findUserByPhoneNumber(String phoneNumber);

	/**
	 * This method is used to
	 * 
	 * @param user
	 * @return
	 * @throws ValidationFailedException
	 */
	User saveUploadedUser(User user) throws ValidationFailedException;

	/**
	 * This method is used to
	 * 
	 * @return
	 * @throws OperationFailedException
	 */
	List<User> getDeletedUsers() throws OperationFailedException;

	/**
	 * This method is used to 
	 * @param userId
	 * @return
	 */
	User findUserByUserId(String userId);

}
