package org.sers.study.server.core.dao;

import org.sers.study.model.TimeCategorySuggestion;

/**
 * A Data access interface that provides methods for performing CRUD operations
 * on the {@link TimeCategorySuggestion} objects <br/>
 * <br/>
 * 
 * The TimeCategorySuggestionDAO interface abstracts the application from a
 * particular implementation of the CRUD operations on the entities
 * 
 * 
 * 
 */
public interface TimeCategorySuggestionDao extends BaseDao<TimeCategorySuggestion> {

}
