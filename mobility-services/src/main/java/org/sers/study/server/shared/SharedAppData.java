package org.sers.study.server.shared;

import org.sers.study.server.core.security.service.impl.CustomSessionProvider;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.model.security.User;

/**
 * 
 * @author Umar
 * 
 */
public class SharedAppData {

	public static final String DEFAULT_MAP_KEY = "02011990";
	public static final int MAX_RESULTS_PER_PAGEE = 10;
	public static final String GLOBAL_FILTER_ID = "globalFilter";
	public static final String FAILED_AT_YODIME = "Failed at Yodime";
	public static final String SUCCESSFUL_AT_YODIME = "Successful at Yodime";
	public static final String SEARCH_PLACE_HOLDER = "Search all fields";
	public static final String DATA_EMPTY_MESSAGE = "No records found with given criteria";
	public static final String SUCCESSFUL_AT_MTN = "Successful at MTN";
	public static final String SUCCESSFUL_AT_AIRTEL = "Successful at Airtel";
	public static final String SUCCESSFUL_AT_AFRICELL = "Successful at Africell";
	public static final String SUCCESSFUL_AT_SMART = "Successful at Smart";
	public static final String SUCCESSFUL_AT_PROVIDER = "Successful at ";

	/**
	 * @return the loggedInUser
	 */
	public static User getLoggedInUser() {
		return ApplicationContextProvider.getBean(CustomSessionProvider.class).getLoggedInUser();
	}
}
