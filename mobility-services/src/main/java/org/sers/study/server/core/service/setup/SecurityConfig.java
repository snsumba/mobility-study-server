package org.sers.study.server.core.service.setup;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.constants.UserStatus;
import org.sers.study.model.security.Permission;
import org.sers.study.model.security.PermissionAnnotation;
import org.sers.study.model.security.PermissionConstants;
import org.sers.study.model.security.Role;
import org.sers.study.model.security.User;

/**
 * 
 * @author Umar
 * 
 */
public class SecurityConfig {

	private SessionFactory sessionFactory;

	public SecurityConfig(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
		initializeSecurityConfigurations();
	}

	private void initializeSecurityConfigurations() {
		Set<Permission> savedPermissions = new HashSet<Permission>();
		List<Permission> systemPermission = parse(PermissionConstants.class);

		for (Permission permission : systemPermission) {
			permission.setRecordStatus(RecordStatus.ACTIVE);
			permission.setDateCreated(new Date());
			permission.setDateChanged(new Date());
			sessionFactory.getCurrentSession().beginTransaction();
			permission = (Permission) sessionFactory.getCurrentSession().merge(permission);
			sessionFactory.getCurrentSession().getTransaction().commit();
			savedPermissions.add(permission);
		}
		Set<Role> defaultAdministratorROle = createAdminRole(savedPermissions, sessionFactory);

		createNormalUserRole(new HashSet<Permission>(getWebAccessPermision()), sessionFactory);
		initializeAdminAccountAndSystemAdmin(defaultAdministratorROle, sessionFactory);
	}

	@SuppressWarnings("unchecked")
	private List<Permission> getWebAccessPermision() {
		sessionFactory.getCurrentSession().beginTransaction();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Permission.class);

		criteria.add(Restrictions.eq("name", PermissionConstants.PERM_WEB_ACCESS));

		return criteria.list();
	}

	/**
	 * this method uses reflection to get all fields annotated with
	 * {@PermissionAnnotation} and returns then as an array.
	 * 
	 * @param annotatedClass
	 * @return listOfPermissions
	 */
	private List<Permission> parse(Class<?> annotatedClass) {

		List<Permission> allPermissions = new ArrayList<Permission>();

		Field[] fields = annotatedClass.getFields();

		for (Field field : fields) {

			if (field.isAnnotationPresent(PermissionAnnotation.class)) {
				PermissionAnnotation permissionAnnotation = field.getAnnotation(PermissionAnnotation.class);

				Permission permission = new Permission();
				permission.setName(permissionAnnotation.name());
				permission.setDescription(permissionAnnotation.description());
				allPermissions.add(permission);
			}
		}
		return allPermissions;
	}

	/**
	 * this method creates a default user role and assigns it the permissions
	 * supplied. It returns the saved role in a set so as it can be used for
	 * other purposes e.g saving a default user
	 * 
	 * @param permissions
	 * @param sessionFactory
	 * @return setOfRoles
	 */
	private Set<Role> createAdminRole(Set<Permission> permissions, SessionFactory sessionFactory) {
		Role defaultAdministratorRole = new Role();
		defaultAdministratorRole.setRecordStatus(RecordStatus.ACTIVE);
		defaultAdministratorRole.setDescription(Role.DEFAULT_ADMIN_ROLE);
		defaultAdministratorRole.setName(Role.DEFAULT_ADMIN_ROLE);
		defaultAdministratorRole.setPermissions(permissions);

		sessionFactory.getCurrentSession().close();
		sessionFactory.getCurrentSession().beginTransaction();
		defaultAdministratorRole = (Role) sessionFactory.getCurrentSession().merge(defaultAdministratorRole);
		sessionFactory.getCurrentSession().getTransaction().commit();

		return new HashSet<Role>(Arrays.asList(new Role[] { defaultAdministratorRole }));
	}

	private Set<Role> createNormalUserRole(Set<Permission> permissions, SessionFactory sessionFactory) {
		Role defaultNormalUserRole = new Role();
		defaultNormalUserRole.setRecordStatus(RecordStatus.ACTIVE);
		defaultNormalUserRole.setDescription(Role.NORMAL_USER_ROLE);
		defaultNormalUserRole.setName(Role.NORMAL_USER_ROLE);
		defaultNormalUserRole.setPermissions(permissions);

		sessionFactory.getCurrentSession().close();
		sessionFactory.getCurrentSession().beginTransaction();
		defaultNormalUserRole = (Role) sessionFactory.getCurrentSession().merge(defaultNormalUserRole);
		sessionFactory.getCurrentSession().getTransaction().commit();

		return new HashSet<Role>(Arrays.asList(new Role[] { defaultNormalUserRole }));
	}

	/**
	 * Creates both the system admin and his corresponding account
	 * 
	 * @param roles
	 * @param sessionFactory
	 */
	private void initializeAdminAccountAndSystemAdmin(Set<Role> roles, SessionFactory sessionFactory) {
		User systemAdministrator = createSystemAdministrator(roles, sessionFactory);
		sessionFactory.getCurrentSession().close();
		sessionFactory.getCurrentSession().beginTransaction();
		sessionFactory.getCurrentSession().getTransaction().commit();
		updateSystemAdministrator(systemAdministrator, sessionFactory);
	}

	/**
	 * this method saved a default user for this system. It assigns him a
	 * default role which is received in a set.
	 * 
	 * @param roles
	 * @param sessionFactory
	 * @return
	 */
	private User createSystemAdministrator(Set<Role> roles, SessionFactory sessionFactory) {
		User systemAdministrator = new User();
		systemAdministrator.setRecordStatus(RecordStatus.ACTIVE);
		systemAdministrator.setPassword("8683a44a1fc724d9bb9dd8c9d32226aca3563065");
		systemAdministrator.setSalt("e2597cf74095403889c6b07b46d8af5d94b8e6");
		systemAdministrator.setStatus(UserStatus.ACTIVE);
		systemAdministrator.setFirstName(User.DEFAULT_FIRST_NAME);
		systemAdministrator.setLastName(User.DEFAULT_LAST_NAME);
		systemAdministrator.setEmailAddress(User.DEFAULT_ADMIN_EMAIL);
		systemAdministrator.setUsername(User.DEFAULT_ADMIN);
		systemAdministrator.setChangePassword(Boolean.FALSE);
		systemAdministrator.setPhoneNumber(User.DEFAULT_PHONE_NUMBER);
		systemAdministrator.setRoles(roles);
		sessionFactory.getCurrentSession().close();
		sessionFactory.getCurrentSession().beginTransaction();
		systemAdministrator = (User) sessionFactory.getCurrentSession().merge(systemAdministrator);
		sessionFactory.getCurrentSession().getTransaction().commit();
		return systemAdministrator;
	}

	User updateSystemAdministrator(User user, SessionFactory sessionFactory) {
		sessionFactory.getCurrentSession().beginTransaction();
		sessionFactory.getCurrentSession().merge(user);
		sessionFactory.getCurrentSession().getTransaction().commit();
		User savedUser = user;
		return savedUser;
	}

}