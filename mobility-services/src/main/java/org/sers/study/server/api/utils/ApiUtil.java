
/**
 *Jun 29, 2017
 *org.sers.study.server.api.utils
 *mobility-services
 *INSPIRON
 * 
 */
package org.sers.study.server.api.utils;

import org.sers.study.model.constants.Gender;
import org.sers.study.model.constants.MethodType;

public class ApiUtil {

	public static final String DEFAULT_DURATION_IN_MINUTES = "3";
	public static final String DEFAULT_DISTANCE_APART = "10";
	public static final String SUCCESS_TOKEN = "0";
	public static final String FAILURE_TOKEN = "-1";
	public static final String SUCCESSFUL_OPERATION = "Operation Successful";

	public static String encryptData(String data_to_encrypt) {
		// System.out.println("data_to_encrypt : " + data_to_encrypt);
		// return DataProtector.encryptWithKeyAndSalt(data_to_encrypt,
		// DataProtector.API_SALT, DataProtector.API_KEY);
		return data_to_encrypt;
	}

	public static String decryptData(String data_to_decrypt) {
		// System.out.println("data_to_decrypt : " + data_to_decrypt);
		// return DataProtector.decryptWithKeyAndSalt(data_to_decrypt,
		// DataProtector.API_SALT, DataProtector.API_KEY);
		return data_to_decrypt;
	}

	public static MethodType getMethodTypeUsed(String methodType) {
		if (methodType.equalsIgnoreCase(MethodType.GPS_DETERMINED.getName())) {
			return MethodType.GPS_DETERMINED;
		} else if (methodType.equalsIgnoreCase(MethodType.MANUAL_INPUT.getName())) {
			return MethodType.MANUAL_INPUT;
		} else
			return MethodType.UNKNOWN;
	}

	public static Gender getUserGender(String gender) {
		if (gender.equalsIgnoreCase(Gender.FEMALE.getName())) {
			return Gender.FEMALE;
		} else if (gender.equalsIgnoreCase(Gender.MALE.getName())) {
			return Gender.MALE;
		} else
			return Gender.UNKNOWN;
	}

}
