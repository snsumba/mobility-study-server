package org.sers.study.server.core.dao;

import org.sers.study.model.TimeDiary;

/**
 * A Data access interface that provides methods for performing CRUD operations
 * on the {@link TimeDiary} objects <br/>
 * <br/>
 * 
 * The TimeDiaryDAO interface abstracts the application from a particular
 * implementation of the CRUD operations on the entities
 * 
 * 
 * 
 */
public interface TimeDiaryDao extends BaseDao<TimeDiary> {

}
