package org.sers.study.server.core.dao.impl;

import org.sers.study.model.CapturedLocation;
import org.sers.study.server.core.dao.CapturedLocationDao;
import org.springframework.stereotype.Repository;

/**
 * Implements the {@link CapturedLocationDao} interface to provide CRUD operations for
 * the {@link CapturedLocation} object using hibernate as the ORM strategy
 * 
 * @author Umar
 * 
 */
@Repository("capturedLocationDAO")
public class CapturedLocationDAOImpl extends BaseDAOImpl<CapturedLocation> implements
		CapturedLocationDao {

}
