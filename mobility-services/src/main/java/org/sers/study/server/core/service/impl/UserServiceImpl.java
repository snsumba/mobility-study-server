package org.sers.study.server.core.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.constants.UserStatus;
import org.sers.study.model.exception.OperationFailedException;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Permission;
import org.sers.study.model.security.Role;
import org.sers.study.model.security.User;
import org.sers.study.server.core.dao.PermissionDao;
import org.sers.study.server.core.dao.RoleDao;
import org.sers.study.server.core.dao.UserDao;
import org.sers.study.server.core.security.service.impl.CustomSessionProvider;
import org.sers.study.server.core.security.util.CustomSecurityUtil;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;
import org.sers.study.server.shared.SharedAppData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

/**
 * Implements the {@link UserService} interface.
 * 
 * @author Umar
 * 
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private PermissionDao permissionDao;

	@Autowired
	private RoleDao roleDao;

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private void setFilters(Search search, String key, Map<String, Object> filters) {
		search.addFilterOr(Filter.ilike("username", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("lastName", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("firstName", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("phoneNumber", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("emailAddress", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("location", "%" + filters.get(key).toString() + "%"));
	}

	@Override
	@Transactional(readOnly = true)
	public User findUserByUsername(String username) {
		return userDao.searchUniqueByPropertyEqual("username", username);
	}

	@Override
	@Transactional(readOnly = true)
	public User findUserByUserId(String userId) {
		return userDao.searchUniqueByPropertyEqual("userId", userId);
	}

	@Override
	@Transactional(readOnly = true)
	public User findUserByPhoneNumber(String phoneNumber) {
		return userDao.searchUniqueByPropertyEqual("phoneNumber", phoneNumber);
	}

	@Override
	@Transactional(readOnly = true)
	public User findUserByEmail(String email) {
		return userDao.searchUniqueByPropertyEqual("emailAddress", email);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mak.cis.mohr.api.service.UserService#getUsers(int)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> getUsers(Integer pageNo) {
		Search search = new Search();
		search.setMaxResults(SystemConstants.MAX_NUM_PAGE_RECORD);
		search.addSort("username", false, true);
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		search.addFilterNotIn("username", User.DEFAULT_ADMIN);

		/*
		 * if the page number is less than or equal to zero, no need for paging
		 */
		if (pageNo != null && pageNo > 0) {
			search.setPage(pageNo - 1);
		} else {
			search.setPage(0);
		}

		List<User> users = userDao.search(search);
		return users;
	}

	@SuppressWarnings("unused")
	private void enforcePasswordPolicy(User user) throws ValidationFailedException {
		/*
		 * enforce password policy (at least 2 numeric characters and a minimum
		 * of 8 characters
		 */
		String regex = "^(?=.*[0-9].*[0-9])[0-9a-zA-Z]{8,}$";
		if (StringUtils.isNotBlank(user.getClearTextPassword())) {
			if (!user.getClearTextPassword().matches(regex)) {
				throw new ValidationFailedException("the password should have atleast 2 numeric characters and "
						+ "a minimum of eight characters long.");
			}
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteUser(User user) throws OperationFailedException {
		if (StringUtils.equalsIgnoreCase(user.getUsername(), User.DEFAULT_ADMIN)) {
			throw new OperationFailedException("cannot delete default administrator");
		}
		user.setRecordStatus(RecordStatus.DELETED);
		userDao.save(user);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.mak.cis.mohr.api.service.UserService#saveRole(org.mak.cis.mohr.model
	 * .Role)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Role saveRole(Role role) throws ValidationFailedException {
		if (StringUtils.isBlank(role.getName()) || StringUtils.isEmpty(role.getName())) {
			throw new ValidationFailedException("the name of the role is not supplied");
		}

		if (StringUtils.isBlank(role.getDescription()) || StringUtils.isEmpty(role.getDescription())) {
			throw new ValidationFailedException("the description of the role is not supplied");
		}

		if (StringUtils.isBlank(role.getId()) || StringUtils.isEmpty(role.getId())) {
			Role existingRole = roleDao.searchUniqueByPropertyEqual("name", role.getName());
			if (existingRole != null) {
				throw new ValidationFailedException("a role is the given name already exists");
			}
		}

		if (role.getPermissions() == null || role.getPermissions().isEmpty()) {
			throw new ValidationFailedException("Role is missing permissions");
		}

		Role saved = roleDao.save(role);
		return saved;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mak.cis.mohr.api.service.UserService#getRoles()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Role> getRoles() {
		Search search = new Search();
		CustomSessionProvider sessionProvider = ApplicationContextProvider.getBean(CustomSessionProvider.class);
		User loggeAccount = sessionProvider.getLoggedInUser();
		if (!loggeAccount.hasAdministrativePrivileges()) {
			search.addFilterNotIn("name", Arrays.asList(new String[] { Role.DEFAULT_ADMIN_ROLE }));
			search.addFilterNotIn("name", Arrays.asList(new String[] { Role.NORMAL_USER_ROLE }));
		}

		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		List<Role> roles = roleDao.search(search);
		return roles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.mak.cis.mohr.api.service.UserService#getRolesByPage(java.lang.Integer
	 * )
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Role> getRolesByPage(Integer pageNo) {
		Search search = new Search();
		search.setMaxResults(SystemConstants.MAX_NUM_PAGE_RECORD);
		search.addSort("id", false, true);
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);

		/*
		 * if the page number is less than or equal to zero, no need for paging
		 */
		if (pageNo > 0) {
			search.setPage(pageNo - 1);
		} else {
			search.setPage(0);
		}

		List<Role> roles = roleDao.search(search);
		return roles;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteRole(Role role) throws OperationFailedException {
		if (StringUtils.equalsIgnoreCase(role.getName(), Role.DEFAULT_ADMIN_ROLE)) {
			throw new OperationFailedException("cannot delete default administration role");
		}
		roleDao.remove(role);
	}

	@Override
	public List<Permission> getPermissions() {
		List<Permission> permissions = permissionDao.searchByRecordStatus(RecordStatus.ACTIVE);
		return permissions;
	}

	@Override
	@Transactional(readOnly = true)
	public List<User> findUsersByUsername(String username) {
		Search search = new Search();
		search.addFilter(new Filter("username", username, Filter.OP_LIKE));
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		search.addFilterNotIn("username", User.DEFAULT_ADMIN);
		List<User> users = userDao.search(search);

		return users;
	}

	@Override
	@Transactional(readOnly = true)
	public List<User> findLockedUsersByUsername(String username) {
		Search search = new Search();
		search.addFilter(new Filter("username", username, Filter.OP_LIKE));
		search.addFilterNotIn("username", User.DEFAULT_ADMIN);
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		List<User> users = userDao.search(search);

		return users;
	}

	@Override
	@Transactional(readOnly = true)
	public User getUserById(String userId) {
		return userDao.searchUniqueByPropertyEqual("id", userId);
	}

	@Override
	public User getUserByEmail(String memberEmail) {
		return userDao.searchUniqueByPropertyEqual("emailAddress", memberEmail);
	}

	@Override
	@Transactional(readOnly = true)
	public Role getRoleById(String id) {
		Role role = roleDao.searchUniqueByPropertyEqual("id", id);
		return role;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Role> findRolesByName(String searchString) {
		Search search = new Search();
		search.addFilter(new Filter("name", searchString, Filter.OP_LIKE));
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		List<Role> roles = roleDao.search(search);
		return roles;
	}

	@Override
	@Transactional(readOnly = true)
	public Role getRoleByRoleName(String name) {
		return roleDao.searchUniqueByPropertyEqual("name", name, RecordStatus.ACTIVE);
	}

	@Override
	public Permission getPermissionById(String id) {
		return permissionDao.searchUniqueByPropertyEqual("id", id);
	}

	@Override
	public Permission savePermision(Permission permission) throws ValidationFailedException {
		if (StringUtils.isBlank(permission.getName()) || StringUtils.isEmpty(permission.getName())) {
			throw new ValidationFailedException("the name of the permission is not supplied");
		}

		if (StringUtils.isBlank(permission.getDescription()) || StringUtils.isEmpty(permission.getDescription())) {
			throw new ValidationFailedException("the description of the permission is not supplied");
		}

		if (StringUtils.isBlank(permission.getId()) || StringUtils.isEmpty(permission.getId())) {
			Permission existingPerm = permissionDao.searchUniqueByPropertyEqual("name", permission.getName());
			if (existingPerm != null) {
				throw new ValidationFailedException("a permission with the given name already exists");
			}
		}

		return permissionDao.save(permission);
	}

	@Override
	public int getTotalNumberOfUsers() {
		Search search = new Search();
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		return userDao.count(search);
	}

	/**
	 * @return the userDAO
	 */
	public UserDao getUserDAO() {
		return userDao;
	}

	/**
	 * @param userDAO
	 *            the userDAO to set
	 */
	public void setUserDAO(UserDao userDAO) {
		this.userDao = userDAO;
	}

	/**
	 * Search for users
	 * 
	 * @see org.PermissionServiceImpl.cis.mohr.api.service.UserService#searchUserByLoginName(java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> searchUserByLoginName(String query) {
		Search search = prepareUserSearchByLoginName(query);
		List<User> users = userDao.search(search);
		return users;
	}

	/**
	 * Prepares the search to be used in searching users
	 * 
	 * @param query
	 * @return
	 */
	public static Search prepareUserSearchByLoginName(String query) {

		Search search = new Search();
		String param = new StringBuilder().append("%").append(StringEscapeUtils.escapeSql(query)).append("%")
				.toString();
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		search.addFilterLike("username", param);
		search.addSort("username", false, true);

		return search;
	}

	@Override
	public int getNumberOfUsersInSearch(String query) {
		Search search = prepareUserSearchByLoginName(query);
		return userDao.count(search);
	}

	@Override
	public List<User> searchUserByLoginName(String query, Integer pageNo) {
		Search search = prepareUserSearchByLoginName(query);
		search.setMaxResults(SystemConstants.MAX_NUM_PAGE_RECORD);

		if (pageNo != null && pageNo > 0) {
			search.setPage(pageNo - 1);
		} else {
			search.setPage(0);
		}

		List<User> users = userDao.search(search);
		return users;
	}

	@Transactional(readOnly = true)
	@Override
	public List<User> getUsersByRoleName(String roleName) {
		Search search = new Search();
		search.addFilterSome("roles", Filter.equal("name", roleName));
		return userDao.search(search);
	}

	@Transactional(readOnly = true)
	@Override
	public User getUserByUsername(String username) {
		System.out.println("getUserByUsername ....");
		User user = userDao.searchUniqueByPropertyEqual("username", username);
		if (user == null)
			user = userDao.searchUniqueByPropertyEqual("phoneNumber", username);
		if (user == null)
			user = userDao.searchUniqueByPropertyEqual("emailAddress", username);
		return user;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void changePassword(String password, User user) throws ValidationFailedException {
		changePassword(password, user, true, true);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public User changePassword(String password, User user, boolean sendChangePasswordNotification)
			throws ValidationFailedException {
		return changePassword(password, user, sendChangePasswordNotification, true);
	}

	@Transactional(readOnly = true)
	@Override
	public List<User> getUsers(int offset, int limit) {
		Search search = new Search();
		search.addSortAsc("username");
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		search.addFilterNotIn("username", User.DEFAULT_ADMIN);

		return userDao.search(search);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Role> getRoles(int offset, int limit) {
		Search search = new Search();
		search.addSortAsc("name");
		search.setFirstResult(offset);
		search.setMaxResults(limit);

		return roleDao.search(search);
	}

	@Transactional(readOnly = true)
	@Override
	public int getTotalNumberOfRoles() {
		return roleDao.count(new Search());
	}

	@Transactional(readOnly = true)
	@Override
	public List<Role> getRoles(User user, int offset, int limit) {

		Search search = new Search();
		search.addFilterSome("users", new Filter("", user));
		search.addSortAsc("name");
		search.setFirstResult(offset);
		search.setMaxResults(limit);

		return roleDao.search(search);
	}

	@Transactional(readOnly = true)
	@Override
	public int getTotalNumberOfRoles(User user) {
		Search search = new Search();
		search.addFilterSome("users", new Filter("", user));
		return roleDao.count(search);
	}

	@Override
	public List<User> getUsers(String query) {

		Search search = new Search();
		search.addFilterNotIn("username", User.DEFAULT_ADMIN);
		search.addFilterILike("firstName", "%" + query + "%");
		search.setDistinct(true);
		return userDao.search(search);
	}

	@Override
	public List<Role> getRoles(String query) {
		Search search = new Search();
		search.addFilterILike("name", "%" + query + "%");
		search.setDistinct(true);
		return roleDao.search(search);
	}

	@Override
	public int getTotalNumberOfUsers(String query) {
		Search search = new Search();
		search.addFilterILike("firstName", "%" + query + "%");
		search.setDistinct(true);
		return userDao.count(search);
	}

	@Override
	public int getTotalNumberOfRoles(String query) {
		return 0;
	}

	@Override
	public User changePassword(String password, User user, boolean sendChangePasswordNotification,
			boolean enforcePasswordPolicy) throws ValidationFailedException {
		return null;
	}

	@Override
	public Permission getPermissionByName(String permissionName) {
		return permissionDao.searchUniqueByPropertyEqual("name", permissionName);
	}

	@Override
	public Object getObjectById(String id) {
		return userDao.searchUniqueByPropertyEqual("id", id);
	}

	@Override
	public List<User> getUsers(int offset, int limit, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("username", false, true);
		search.addFilterNotIn("username", User.DEFAULT_ADMIN);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		return userDao.search(search);
	}

	@Override
	public int countUsers(Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateCreated", true, true);
		search.addSort("username", false, true);
		search.addFilterNotIn("username", User.DEFAULT_ADMIN);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		return userDao.count(search);
	}

	@Override
	public void saveAdminUser(List<User> users) throws ValidationFailedException {
		for (User adminUser : users) {
			User user = new User();
			user.setUsername(adminUser.getUsername());
			user.setStatus(UserStatus.ACTIVE);
			user.setFirstName(adminUser.getFirstName());
			user.setLastName(adminUser.getLastName());
			user.setPhoneNumber(adminUser.getPhoneNumber());
			user.setEmailAddress(adminUser.getEmailAddress());
			user.setSalt("e2597cf74095403889c6b07b46d8af5d94b8e6");
			user.setClearTextPassword(adminUser.getPassword());
			user.setActivateStatus(OperationStatus.FALSE.getName());
			user.setDeactivateStatus(OperationStatus.TRUE.getName());
			user.setEditStatus(OperationStatus.TRUE.getName());
			saveUser(user);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.mak.cis.mohr.api.service.UserService#saveUser(org.mak.cis.mohr.model
	 * .User)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public User saveUser(User user) throws ValidationFailedException {
		// User loggedInAccount =
		// ApplicationContextProvider.getBean(CustomSessionProvider.class).getLoggedInUser();

		/*
		 * if (loggedInAccount == null) {// Signing up loggedInAccount =
		 * getUserByUsername(User.DEFAULT_ADMIN); }
		 */

		// Check for Phone Number Syntax, Leave if blank
		if (StringUtils.isNotBlank(user.getPhoneNumber()) && StringUtils.isNotEmpty(user.getPhoneNumber())) {
			user.setPhoneNumber(TelephoneNumberUtils.getValidTelephoneNumber(user.getPhoneNumber()));
		}

		// check whether the id is null or empty this is to ensure that the user
		// is new
		if (StringUtils.isBlank(user.getId()) || StringUtils.isEmpty(user.getId())) {
			// check whether username exists
			log.debug(this.getClass().getName() + " - checking for existing user");
			User userByUsername = findUserByUsername(user.getUsername());

			if (userByUsername != null) {
				throw new ValidationFailedException(
						"A user with this username - " + user.getUsername() + " already exists");
			}
			
			// by default, the user is set to active
			user.setStatus(UserStatus.ACTIVE);
			// If no roles attached, attach default role
			if (user.getRoles() == null || user.getRoles().isEmpty())
				user.addRole(getRoleByRoleName(Role.NORMAL_USER_ROLE));
		} else {
			User existingUser = userDao.find(user.getId());

			if (existingUser != null) {
				user.setPassword(existingUser.getPassword());
				user.setSalt(existingUser.getSalt());
				user.setStatus(existingUser.getStatus());
			}
			User existingUserWithSimilarUsername = null;
			existingUserWithSimilarUsername = findUserByUsername(user.getUsername());

			if (existingUserWithSimilarUsername != null) {
				if (!existingUserWithSimilarUsername.getId().equalsIgnoreCase(user.getId())) {
					throw new ValidationFailedException(
							"A user with this username - " + user.getUsername() + " already exists");
				}
			}

		}
		CustomSecurityUtil.prepUserCredentials(user);

		if (StringUtils.isBlank(user.getId()) && StringUtils.isEmpty(user.getId())) {
			/*
			 * user.setCreatedBy(loggedInAccount);
			 * user.setChangedBy(loggedInAccount); return
			 * userDao.directSave(user);
			 */
			return saveUserAndAttachAccount(user);

		}

		return userDao.directMerge(user);

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public User saveUploadedUser(User user) throws ValidationFailedException {
		User loggedInAccount = ApplicationContextProvider.getBean(CustomSessionProvider.class).getLoggedInUser();

		if (loggedInAccount == null) {// Signing up
			loggedInAccount = getUserByUsername(User.DEFAULT_ADMIN);
		}

		// Check for Phone Number Syntax, Leave if blank
		if (StringUtils.isNotBlank(user.getPhoneNumber()) && StringUtils.isNotEmpty(user.getPhoneNumber())) {
			user.setPhoneNumber(TelephoneNumberUtils.getValidTelephoneNumber(user.getPhoneNumber()));
		}

		// check whether the id is null or empty this is to ensure that the user
		// is new
		if (StringUtils.isBlank(user.getId()) || StringUtils.isEmpty(user.getId())) {
			// check whether username exists
			log.debug(this.getClass().getName() + " - checking for existing user");
			User userByUsername = findUserByUsername(user.getUsername());
			if (userByUsername != null) {
				throw new ValidationFailedException(
						"A user with this username - " + user.getUsername() + " already exists");
			}

			// by default, the user is set to active
			user.setStatus(UserStatus.ACTIVE);
			// If no roles attached, attach default role
			if (user.getRoles() == null || user.getRoles().isEmpty())
				user.addRole(getRoleByRoleName(Role.NORMAL_USER_ROLE));
		} else {
			User existingUser = userDao.find(user.getId());

			if (existingUser != null) {
				user.setPassword(existingUser.getPassword());
				user.setSalt(existingUser.getSalt());
				user.setStatus(existingUser.getStatus());
			}
			User existingUserWithSimilarUsername = null;
			existingUserWithSimilarUsername = findUserByUsername(user.getUsername());

			if (existingUserWithSimilarUsername != null) {
				if (!existingUserWithSimilarUsername.getId().equalsIgnoreCase(user.getId())) {
					throw new ValidationFailedException(
							"A user with this username - " + user.getUsername() + " already exists");
				}
			}

		}
		CustomSecurityUtil.prepUserCredentials(user);

		if (StringUtils.isBlank(user.getId()) && StringUtils.isEmpty(user.getId())) {
			/*
			 * user.setCreatedBy(loggedInAccount);
			 * user.setChangedBy(loggedInAccount); return
			 * userDao.directSave(user);
			 */
			return saveUserAndAttachAccount(user);

		}

		return userDao.directMerge(user);

	}

	private User saveUserAndAttachAccount(User user) {
		/*user.setRoles(new HashSet<Role>(findRolesByName(Role.NORMAL_USER_ROLE)));
		User memberUser = userDao.directMerge(user);
		User createdBy = SharedAppData.getLoggedInUser();
		User changedBy = SharedAppData.getLoggedInUser();

		if (createdBy == null) {
			createdBy = findUserByUsername(User.DEFAULT_ADMIN);
			changedBy = findUserByUsername(User.DEFAULT_ADMIN);
		}
		memberUser.setCreatedBy(createdBy);
		memberUser.setChangedBy(changedBy);*/

		return userDao.directMerge(user);
	}

	@Override
	@Transactional(readOnly = true)
	public List<User> getUsers() throws OperationFailedException {
		Search search = new Search();
		search.addFilterNotIn("username", User.DEFAULT_ADMIN);
		CustomSessionProvider sessionProvider = ApplicationContextProvider.getBean(CustomSessionProvider.class);
		User loggeAccount = sessionProvider.getLoggedInUser();
		if (!loggeAccount.hasAdministrativePrivileges())
			search.addFilterEqual("emailAddress", loggeAccount.getEmailAddress());
		search.addFilterEqual("recordStatus", RecordStatus.ACTIVE);
		return userDao.search(search);
	}

	@Override
	@Transactional(readOnly = true)
	public List<User> getDeletedUsers() throws OperationFailedException {
		Search search = new Search();
		search.addFilterNotIn("username", User.DEFAULT_ADMIN);
		CustomSessionProvider sessionProvider = ApplicationContextProvider.getBean(CustomSessionProvider.class);
		User loggeAccount = sessionProvider.getLoggedInUser();
		if (!loggeAccount.hasAdministrativePrivileges())
			search.addFilterEqual("emailAddress", loggeAccount.getEmailAddress());
		search.addFilterEqual("recordStatus", RecordStatus.DELETED);
		return userDao.search(search);
	}

}