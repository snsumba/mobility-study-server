
/**
 *Sep 13, 2017
 *org.sers.study.server.core.service.api
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.server.core.service.api;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.User;
import org.sers.study.server.api.models.ListServerCapturedLocation;
import org.sers.study.server.api.models.ServerCapturedLocation;
import org.sers.study.server.api.utils.ApiUtil;
import org.sers.study.server.api.utils.AppRecordStatus;
import org.sers.study.server.core.service.CapturedLocationService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@Path("/RecordAppListOfCapturedLocationsService")
public class RecordAppListOfCapturedLocationsService {

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response apiResponse(@Context HttpServletRequest request, @Context HttpHeaders headers,
			ListServerCapturedLocation listServerCapturedLocation) throws JSONException {

		System.out.println("AppCapturedLocationService .... ");
		UserService userService = ApplicationContextProvider.getBean(UserService.class);
		JSONObject jsonObject = new JSONObject();

		try {

			String userId = ApiUtil.decryptData(listServerCapturedLocation.getUserId());
			User systemUser = userService.findUserByUserId(userId);
			if (systemUser != null) {
				/*if (!StringUtils.isBlank(systemUser.getApiToken())) {
					if (!ApiUtil.decryptData(listServerCapturedLocation.getApiToken())
							.equals(systemUser.getApiToken())) {
						throw new ValidationFailedException(
								"Invalid or Expired Access Token, Please logout and login again");
					}
				}*/
				List<ServerCapturedLocation> serverCapturedLocations = listServerCapturedLocation
						.getServerCapturedLocations();
				if (serverCapturedLocations != null) {
					if (serverCapturedLocations.size() > 0) {

						List<ServerCapturedLocation> failedCapturedLocations = new ArrayList<ServerCapturedLocation>();
						List<ServerCapturedLocation> successfulCapturedLocations = new ArrayList<ServerCapturedLocation>();

						CapturedLocationService capturedLocationService = ApplicationContextProvider
								.getBean(CapturedLocationService.class);
						for (ServerCapturedLocation serverCapturedLocation : serverCapturedLocations) {
							CapturedLocation capturedLocation = new CapturedLocation(
									ApiUtil.decryptData(serverCapturedLocation.getDate()),
									ApiUtil.decryptData(serverCapturedLocation.getPlaceName()),
									ApiUtil.decryptData(serverCapturedLocation.getLongitude()),
									ApiUtil.decryptData(serverCapturedLocation.getLatitude()),
									ApiUtil.decryptData(serverCapturedLocation.getAccuracy()), null,
									ApiUtil.getMethodTypeUsed(
											ApiUtil.decryptData(serverCapturedLocation.getMethodType())),
									systemUser);
							capturedLocation.setRecordId(ApiUtil.decryptData(serverCapturedLocation.getRecordId()));

							try {
								CapturedLocation savedLocation = capturedLocationService
										.saveCapturedLocation(capturedLocation);
								if (savedLocation != null) {
									successfulCapturedLocations.add(serverCapturedLocation);
								} else
									failedCapturedLocations.add(serverCapturedLocation);
							} catch (ValidationFailedException e) {
								e.printStackTrace();
								failedCapturedLocations.add(serverCapturedLocation);
							}

						}

						JSONArray successJsonArray = new JSONArray();
						if (successfulCapturedLocations != null) {
							for (ServerCapturedLocation serverCapturedLocation : successfulCapturedLocations) {
								JSONObject successfulLocationJsonObject = new JSONObject();
								successfulLocationJsonObject.put("status",
										ApiUtil.encryptData(AppRecordStatus.SYNCED.getStatus()));
								successfulLocationJsonObject.put("userId", ApiUtil.encryptData(userId));
								successfulLocationJsonObject.put("recordId", serverCapturedLocation.getRecordId());
								successfulLocationJsonObject.put("date", serverCapturedLocation.getDate());
								successfulLocationJsonObject.put("placeName", serverCapturedLocation.getPlaceName());
								successfulLocationJsonObject.put("longitude", serverCapturedLocation.getLongitude());
								successfulLocationJsonObject.put("latitude", serverCapturedLocation.getLatitude());
								successfulLocationJsonObject.put("accuracy", serverCapturedLocation.getAccuracy());
								successfulLocationJsonObject.put("methodType", ApiUtil.encryptData("n.a"));
								successfulLocationJsonObject.put("responseStatus",
										ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
								successfulLocationJsonObject.put("responseMessage",
										ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));
								successJsonArray.put(successfulLocationJsonObject);
							}
						}

						JSONArray failedJsonArray = new JSONArray();
						if (failedCapturedLocations != null) {
							for (ServerCapturedLocation serverCapturedLocation : failedCapturedLocations) {
								JSONObject failedLocationJsonObject = new JSONObject();
								failedLocationJsonObject.put("status",
										ApiUtil.encryptData(AppRecordStatus.UN_SYNCED.getStatus()));
								failedLocationJsonObject.put("userId", ApiUtil.encryptData(userId));
								failedLocationJsonObject.put("recordId", serverCapturedLocation.getRecordId());
								failedLocationJsonObject.put("date", String.valueOf(serverCapturedLocation.getDate()));
								failedLocationJsonObject.put("placeName", serverCapturedLocation.getPlaceName());
								failedLocationJsonObject.put("longitude", serverCapturedLocation.getLongitude());
								failedLocationJsonObject.put("latitude", serverCapturedLocation.getLatitude());
								failedLocationJsonObject.put("accuracy", serverCapturedLocation.getAccuracy());
								failedLocationJsonObject.put("methodType", ApiUtil.encryptData("n.a"));
								failedLocationJsonObject.put("responseStatus",
										ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
								failedLocationJsonObject.put("responseMessage",
										ApiUtil.encryptData("Failed to record user location"));
								failedJsonArray.put(failedLocationJsonObject);
							}
						}

						jsonObject.put("failedRecords", failedJsonArray);
						jsonObject.put("successfulRecords", successJsonArray);
						jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
						jsonObject.put("responseMessage", ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));

					} else {
						jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
						jsonObject.put("responseMessage", ApiUtil.encryptData("No Location data to sync"));
					}

				} else {
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData("No Location data to sync"));
				}

			} else {
				jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
				jsonObject.put("responseMessage", ApiUtil.encryptData("Authentication failed, wrong username"));
			}
		} catch (Exception e) {
			jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
			jsonObject.put("responseMessage", ApiUtil.encryptData(e.getMessage()));
		}
		System.out.println("jsonObject : " + jsonObject);
		return Response.status(200).entity("" + jsonObject).build();
	}

}
