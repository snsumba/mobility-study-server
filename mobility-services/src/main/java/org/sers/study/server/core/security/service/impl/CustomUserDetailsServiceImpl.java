package org.sers.study.server.core.security.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.sers.study.server.core.dao.PermissionDao;
import org.sers.study.server.core.dao.UserDao;
import org.sers.study.server.core.security.CustomUser;
import org.sers.study.server.core.security.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.sers.study.model.security.Permission;
import org.sers.study.model.security.User;

/**
 * This class is responsible for loading a {@link CustomUser} object for a given
 * {@link User}
 */
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {

	@Autowired
	private UserDao userDAO;

	@Autowired
	private PermissionDao permissionDAO;

	@Override
	public CustomUser getUserDetails(User userAccount) {
		CustomUser userDetails = null;
		User account;
		if (userAccount != null) {
			account = userAccount;

			List<GrantedAuthority> authorities = getUserAuthorities(account);
			if (authorities == null) {
				authorities = new ArrayList<GrantedAuthority>();
			}
			userDetails = new CustomUser(account, true, true, true, true, authorities);
		}
		return userDetails;
	}

	/**
	 * gets a list of granted authorities for a given user
	 * 
	 * @param user
	 * @return
	 */
	protected List<GrantedAuthority> getUserAuthorities(User user) {
		List<GrantedAuthority> authorities = null;
		if (user != null) {
			authorities = new ArrayList<GrantedAuthority>();
			List<Permission> permissions = null;

			if (user.hasAdministrativePrivileges()) {
				permissions = permissionDAO.findAll();
			} else {
				permissions = user.findPermissions();
			}

			if (permissions != null && permissions.size() > 0) {
				for (Permission perm : permissions) {
					GrantedAuthority ga = new GrantedAuthorityImpl(perm.getName());
					authorities.add(ga);
				}
			}
		}

		return authorities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
		try {
			User user = userDAO.searchUniqueByPropertyEqual("username", username);
			if (user == null)
				user = userDAO.searchUniqueByPropertyEqual("phoneNumber", username);
			if (user == null)
				user = userDAO.searchUniqueByPropertyEqual("agentTerminalId", username);
			if (user == null)
				user = userDAO.searchUniqueByPropertyEqual("emailAddress", username);
			
			if (user != null) {
				return getUserDetails(user);
			}
		} catch (Exception e) {

			throw new UsernameNotFoundException(e.getMessage(), e);
		}

		return null;
	}

}
