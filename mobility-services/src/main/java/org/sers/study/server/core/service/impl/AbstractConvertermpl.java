package org.sers.study.server.core.service.impl;

import org.sers.study.model.CapturedLocation;
import org.sers.study.model.security.Country;
import org.sers.study.model.security.User;
import org.sers.study.server.core.dao.CapturedLocationDao;
import org.sers.study.server.core.dao.CountryDao;
import org.sers.study.server.core.dao.UserDao;
import org.sers.study.server.core.service.AbstractConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AbstractConvertermpl implements AbstractConverter {

	@Autowired
	CountryDao countryDao;

	@Autowired
	UserDao userDao;

	@Autowired
	CapturedLocationDao capturedLocationDao;

	@Override
	public Country getCountryById(String id) {
		return countryDao.searchUniqueByPropertyEqual("id", id);
	}

	@Override
	public User getUserById(String id) {
		return userDao.searchUniqueByPropertyEqual("id", id);
	}

	@Override
	public CapturedLocation getCapturedLocationById(String id) {
		return capturedLocationDao.searchUniqueByPropertyEqual("id", id);
	}
}
