package org.sers.study.server.core.service.impl;

import java.util.List;
import java.util.Map;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.AppType;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.server.core.dao.AppTypeDao;
import org.sers.study.server.core.service.AppTypeService;
import org.sers.study.server.shared.SharedAppData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

@AuthorComment
@Service("appTypeService")
@Transactional
public class AppTypeServiceImpl implements AppTypeService {

	@Autowired
	private AppTypeDao appTypeDao;

	@Override
	public Object getObjectById(String id) {
		return appTypeDao.searchByPropertyEqual("id", id);
	}

	@Override
	public AppType saveAppType(AppType appType) throws ValidationFailedException {
		this.validateObject(appType);
		if (appType.getId() == null) {
			isAlreadyRegistered(appType);
		}
		return appTypeDao.directMerge(appType);
	}

	private void isAlreadyRegistered(AppType appType) throws ValidationFailedException {
		Search search = new Search();
		if (appTypeDao.count(search) > 0) 
			throw new ValidationFailedException("Activation status : "+appType.getOperationStatus()+" is already registered, Please edit an existing Status");
		
	}

	@Override
	public void updateAppType(AppType appType) throws ValidationFailedException {
		validateObject(appType);
		appTypeDao.directUpdate(appType);
	}

	private void validateObject(AppType appType) throws ValidationFailedException {
		if (appType.getMethodType() == null) {
			throw new ValidationFailedException("Method type not set.");
		}
		if (appType.getOperationStatus() == null) {
			throw new ValidationFailedException("Operation status not set.");
		}

	}

	private void objectAlreadyExists(AppType appType) throws ValidationFailedException {
		Search search = new Search();
		search.addFilterEqual("operationStatus", appType.getOperationStatus());

		if (appTypeDao.count(search) > 0)
			throw new ValidationFailedException(
					"Status : " + appType.getOperationStatus() + " Already registered to " + appType.getMethodType());
	}

	@Override
	public List<AppType> getAppTypes() {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		return appTypeDao.search(search);
	}

	private void setFilters(Search search, String key, Map<String, Object> filters) {
	}

	@Override
	public List<AppType> getAppTypes(int offset, int limit, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		return appTypeDao.search(search);
	}

	@Override
	public int countAppTypes(Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		return appTypeDao.count(search);
	}

	@Override
	public AppType findAppTypeByOperationStatus(OperationStatus operationStatus) {
		Search search = new Search();
		search.addFilterEqual("operationStatus", operationStatus);
		return appTypeDao.searchUnique(search);
	}

}