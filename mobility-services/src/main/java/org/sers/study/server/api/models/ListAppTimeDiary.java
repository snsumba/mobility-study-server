
/**
 *Sep 13, 2017
 *org.sers.study.server.api.models
 *mobility-services
 *INSPIRON
  */
package org.sers.study.server.api.models;

import java.io.Serializable;
import java.util.List;

public class ListAppTimeDiary implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
	private List<AppTimeDiary> appTimeDiaries;
	/*
	 * private String apiToken;
	 * 
	 *//**
		 * @return the apiToken
		 */
	/*
	 * public String getApiToken() { return apiToken; }
	 * 
	 *//**
		 * @param apiToken
		 *            the apiToken to set
		 *//*
		 * public void setApiToken(String apiToken) { this.apiToken = apiToken;
		 * }
		 */

	/**
	 * @return the appTimeDiaries
	 */
	public List<AppTimeDiary> getAppTimeDiaries() {
		return appTimeDiaries;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @param appTimeDiaries
	 *            the appTimeDiaries to set
	 */
	public void setAppTimeDiaries(List<AppTimeDiary> appTimeDiaries) {
		this.appTimeDiaries = appTimeDiaries;
	}

}
