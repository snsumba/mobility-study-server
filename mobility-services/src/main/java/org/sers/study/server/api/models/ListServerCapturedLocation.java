
/**
 *Sep 13, 2017
 *org.sers.study.server.api.models
 *mobility-services

 */
package org.sers.study.server.api.models;

import java.io.Serializable;
import java.util.List;

public class ListServerCapturedLocation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
	private List<ServerCapturedLocation> serverCapturedLocations;
	/*
	 * private String apiToken;
	 * 
	 *//**
		 * @return the apiToken
		 */
	/*
	 * public String getApiToken() { return apiToken; }
	 * 
	 *//**
		 * @param apiToken
		 *            the apiToken to set
		 *//*
		 * public void setApiToken(String apiToken) { this.apiToken = apiToken;
		 * }
		 */

	/**
	 * @return the serverCapturedLocations
	 */
	public List<ServerCapturedLocation> getServerCapturedLocations() {
		return serverCapturedLocations;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @param serverCapturedLocations
	 *            the serverCapturedLocations to set
	 */
	public void setServerCapturedLocations(List<ServerCapturedLocation> serverCapturedLocations) {
		this.serverCapturedLocations = serverCapturedLocations;
	}

}
