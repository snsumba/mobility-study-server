package org.sers.study.server.core.dao.impl;

import org.sers.study.server.core.dao.CountryDao;
import org.springframework.stereotype.Repository;
import org.sers.study.model.security.Country;

/**
 * Implements the {@link CountryDao} interface to provide CRUD operations for
 * the {@link Country} object using hibernate as the ORM strategy
 * 
 * @author Umar
 * 
 */
@Repository("countryDAO")
public class CountryDAOImpl extends BaseDAOImpl<Country> implements
		CountryDao {

}
