package org.sers.study.server.core.service.impl;

import java.util.List;
import java.util.Map;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.DurationInfo;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.server.core.dao.DurationInfoDao;
import org.sers.study.server.core.service.DurationInfoService;
import org.sers.study.server.shared.SharedAppData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

@AuthorComment
@Service("durationInfoService")
@Transactional
public class DurationInfoServiceImpl implements DurationInfoService {

	@Autowired
	private DurationInfoDao durationInfoDao;

	@Override
	public Object getObjectById(String id) {
		return durationInfoDao.searchByPropertyEqual("id", id);
	}

	@Override
	public DurationInfo saveDurationInfo(DurationInfo durationInfo) throws ValidationFailedException {
		if (durationInfo.getId() == null) {
			isAlreadyRegistered(durationInfo);
		}
		return durationInfoDao.directMerge(durationInfo);
	}

	private void isAlreadyRegistered(DurationInfo durationInfo) throws ValidationFailedException {
		Search search = new Search();
		if (durationInfoDao.count(search) > 0)
			throw new ValidationFailedException("Duration already registered, Please edit an existing duration");
	}

	@Override
	public void updateDurationInfo(DurationInfo durationInfo) throws ValidationFailedException {
		durationInfoDao.directUpdate(durationInfo);
	}

	private void setFilters(Search search, String key, Map<String, Object> filters) {
	}

	@Override
	public List<DurationInfo> getDurationInfos(int offset, int limit, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		return durationInfoDao.search(search);
	}

	@Override
	public int countDurationInfos(Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		return durationInfoDao.count(search);
	}

	@Override
	public void deleteDurationInfo(DurationInfo durationInfo) throws ValidationFailedException {
		durationInfoDao.remove(durationInfo);
		durationInfoDao.removeById(durationInfo.getId());
		System.out.println("Delete called ..." + durationInfo.getId());
	}

	@Override
	public DurationInfo getDurationInfoByRecordstatus(RecordStatus recordStatus) {
		return durationInfoDao.searchUniqueByPropertyEqual("recordStatus", recordStatus);
	}
}