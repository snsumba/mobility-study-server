package org.sers.study.server.core.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Custom Implementation of necessary Date truncation functionalities.
 * 
 * @author kdeo
 * 
 */
public class DateUtils {

	public static final Date SYSTEM_STABLE_DATE = new GregorianCalendar(2016,
			00, 01).getTime();

	public static long calculateDaysBetween(Date dateEarly, Date dateLater) {
		return (dateLater.getTime() - dateEarly.getTime())
				/ (24 * 60 * 60 * 1000);
	}

	public static Date getFirstDayOfThisYear() {
		return new GregorianCalendar(currentYear(), 0, 1).getTime();
	}

	public static Date getLastDayOfThisYear() {
		return new GregorianCalendar(currentYear(), 11, 31).getTime();
	}

	public static Date getSimpleDate(Date date, Date time) {
		String[] dateTokens = (new SimpleDateFormat("dd-MM-yyyy").format(date))
				.split("-");
		String[] timeTokens = (new SimpleDateFormat("HH-mm-ss").format(time))
				.split("-");
		return new GregorianCalendar(new Integer(dateTokens[2]), new Integer(
				dateTokens[1]) - 1, new Integer(dateTokens[0]), new Integer(
				timeTokens[0]), new Integer(timeTokens[1]), new Integer(
				timeTokens[2])).getTime();
	}

	/**
	 * Gets the current hour of the day
	 * 
	 * @return
	 */
	public static int currentHour() {
		return Integer.parseInt(new SimpleDateFormat("HH-mm-ss").format(
				new Date()).split("-")[0]);
	}

	/**
	 * Gets the current minute of the day
	 * 
	 * @return
	 */
	public static int currentMinutes() {
		return Integer.parseInt(new SimpleDateFormat("HH-mm-ss").format(
				new Date()).split("-")[1]);
	}

	/**
	 * Gets the current day
	 * 
	 * @return
	 */
	public static int currentDay() {
		return Integer.parseInt(new SimpleDateFormat("dd-MM-yyyy").format(
				new Date()).split("-")[0]);
	}

	/**
	 * Gets the current Month
	 * 
	 * @return
	 */
	public static int currentMonth() {
		return (Integer.parseInt(new SimpleDateFormat("dd-MM-yyyy").format(
				new Date()).split("-")[1])) - 1;
	}

	public static Date getFirstDateOfThisYear() {
		return new GregorianCalendar(currentYear(), 00, 01).getTime();
	}

	public static Date getFirstDateOfThisMonth() {
		return new GregorianCalendar(currentYear(), currentMonth(), 01)
				.getTime();
	}

	public static Date getLastDateOfThisMonth() {
		return new GregorianCalendar(currentYear(), currentMonth(), 31)
				.getTime();
	}

	public static Date getLastDateOfThisYear() {
		return new GregorianCalendar(currentYear(), 11, 31).getTime();
	}

	public static Date getLastDateOfThisYear(int year) {
		return new GregorianCalendar(year, 11, 31).getTime();
	}

	public static Date getDateAfterDays(int days) {
		return new GregorianCalendar(currentYear(), currentMonth(),
				currentDay() + days).getTime();
	}

	public static void main(String[] args) {
		System.out.println("'sam','deo'");
	}

	/**
	 * Gets the current Year
	 * 
	 * @return
	 */
	public static int currentYear() {
		return Integer.parseInt(new SimpleDateFormat("dd-MM-yyyy").format(
				new Date()).split("-")[2]);
	}

	public static Date getMaximumDate(Date date) {
		String[] tokens = (new SimpleDateFormat("dd:MM:yyyy").format(date))
				.split(":");

		return new GregorianCalendar(new Integer(tokens[2]), new Integer(
				tokens[1]) - 1, new Integer(tokens[0]), 23, 59, 59).getTime();
	}

	public static Date getMinimumDate(Date date) {
		String[] tokens = (new SimpleDateFormat("dd:MM:yyyy").format(date))
				.split(":");

		return new GregorianCalendar(new Integer(tokens[2]), new Integer(
				tokens[1]) - 1, new Integer(tokens[0]), 00, 00, 00).getTime();
	}

	public static Date getMinimumDate(Date date, int hoursPastMidnight) {
		String[] tokens = (new SimpleDateFormat("dd:MM:yyyy").format(date))
				.split(":");

		return new GregorianCalendar(new Integer(tokens[2]), new Integer(
				tokens[1]) - 1, new Integer(tokens[0]), hoursPastMidnight, 00,
				00).getTime();
	}

	public static List<Date> getDates(Date start, Date end, int interval) {
		if (start == null || end == null)
			throw new IllegalArgumentException(
					"Both Start date and end date must be specified");

		if (interval <= 0)
			throw new IllegalArgumentException("Invalid Interval specified, "
					+ interval);

		List<Date> datesList = new ArrayList<Date>();
		GregorianCalendar startDate = new GregorianCalendar();
		startDate.setTime(start);
		GregorianCalendar endDate = new GregorianCalendar();
		endDate.setTime(end);
		endDate.add(Calendar.DATE, 1);

		while (startDate.before(endDate)) {
			datesList.add(startDate.getTime());
			startDate.add(Calendar.DATE, interval);
		}

		return datesList;
	}
}
