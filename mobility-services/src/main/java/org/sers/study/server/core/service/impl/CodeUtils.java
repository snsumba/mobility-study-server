package org.sers.study.server.core.service.impl;

import java.util.HashMap;

/**
 * Used to generate {@link BusinessUnit} Codes from the previous Saved Codes.
 * 
 * @author kdeo
 * 
 */
public final class CodeUtils {

	private static CodeUtils instance = new CodeUtils();
	private static final int MAXIMUM_CODE_POINT = 999;
	public static final String BUSINESS_CODE = "BUSINESS_CODE";
	public static final String NUMERIC_CODE = "NUMERIC_CODE";
	public static final String CHARACTER_CODE = "CHARACTER_CODE";

	/**
	 * @return the instance
	 */
	public static CodeUtils getInstance() {
		return instance;
	}

	public final HashMap<String, String> getNextCode(String previousCode) {
		if (previousCode == null || previousCode.isEmpty())
			previousCode = "NoCode";
		HashMap<String, String> parameters = new HashMap<String, String>();
		StringBuffer characterBuffer = new StringBuffer();
		StringBuffer digitBuffer = new StringBuffer();

		for (Character character : previousCode.toCharArray()) {
			if (Character.isAlphabetic(character))
				characterBuffer.append(character);
			else
				digitBuffer.append(character);
		}

		// To cater for the first generation of new codes
		if (characterBuffer.length() <= 0 || digitBuffer.length() != 3) {
			parameters.put(BUSINESS_CODE, "A001");
			parameters.put(NUMERIC_CODE, "1");
			parameters.put(CHARACTER_CODE, "A");
			return parameters;
		}

		if (Integer.valueOf(digitBuffer.toString()) < MAXIMUM_CODE_POINT) {
			int newDigit = Integer.valueOf(digitBuffer.toString()) + 1;
			if (String.valueOf(newDigit).endsWith("0"))
				newDigit++;

			if (String.valueOf(newDigit).length() == 3) {
				parameters.put(BUSINESS_CODE, characterBuffer.toString() + newDigit);
				parameters.put(NUMERIC_CODE, String.valueOf(newDigit));
				parameters.put(CHARACTER_CODE, characterBuffer.toString());
			}

			else if (String.valueOf(newDigit).length() == 2) {
				parameters.put(BUSINESS_CODE, characterBuffer.toString() + "0" + newDigit);
				parameters.put(NUMERIC_CODE, "0" + newDigit);
				parameters.put(CHARACTER_CODE, characterBuffer.toString());
			}

			else if (String.valueOf(newDigit).length() == 1) {
				parameters.put(BUSINESS_CODE, characterBuffer.toString() + "00" + newDigit);
				parameters.put(NUMERIC_CODE, "00" + newDigit);
				parameters.put(CHARACTER_CODE, characterBuffer.toString());
			}
			return parameters;
		}

		String nextCharacterCode = getNextCharacterCode(characterBuffer.toString());
		parameters.put(BUSINESS_CODE, nextCharacterCode + "001");

		parameters.put(NUMERIC_CODE, "001");
		parameters.put(CHARACTER_CODE, nextCharacterCode);

		return parameters;
	}

	private final String getNextCharacterCode(String characterCode) {
		return characterCode.substring(0, characterCode.length() - 1)
				+ getCharaterRepresentation(characterCode.charAt(characterCode.length() - 1), 1);
	}

	private final String getCharaterRepresentation(char character, int increment) {
		if (String.valueOf(character).equalsIgnoreCase("Z"))
			return "ZA";

		return Character.toChars((((character)) + 1))[0] + "";
	}
}
