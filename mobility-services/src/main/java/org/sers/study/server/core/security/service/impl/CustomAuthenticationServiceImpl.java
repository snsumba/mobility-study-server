package org.sers.study.server.core.security.service.impl;

import org.apache.commons.lang.StringUtils;
import org.sers.study.server.core.dao.UserDao;
import org.sers.study.server.core.security.CustomUser;
import org.sers.study.server.core.security.service.CustomAuthenticationService;
import org.sers.study.server.core.security.service.CustomUserDetailsService;
import org.sers.study.server.core.security.util.CustomSecurityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.sers.study.model.constants.UserStatus;
import org.sers.study.model.security.User;


@Service("authenticationService")
public class CustomAuthenticationServiceImpl implements CustomAuthenticationService {

	@Autowired
	private UserDao userDAO;

	@Autowired
	private CustomUserDetailsService pettycashUserDetailsService;

	private Logger log = LoggerFactory.getLogger(this.getClass());

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.mak.cis.mohr.api.security.service.AuthenticationService#authenticate
	 * (java .lang.String, java.lang.String)
	 */
	@Override
	public User authenticate(String username, String password, boolean attachUserToSecurityContext) {
		User user = null;
		if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
			user = userDAO.searchUniqueByPropertyEqual("username", username);
			if (user == null)
				user = userDAO.searchUniqueByPropertyEqual("phoneNumber", username);
			if (user == null)
				user = userDAO.searchUniqueByPropertyEqual("agentTerminalId", username);
			if (user == null)
				user = userDAO.searchUniqueByPropertyEqual("emailAddress", username);

			if (user != null && isValidUserPassword(user, password)) {
				if (attachUserToSecurityContext) {
					CustomUser userDetails = pettycashUserDetailsService.getUserDetails(user);
					if (userDetails != null) {
						CustomSecurityUtil.setSecurityContext(userDetails);
					}
				} else {
					return user;
				}
			} else {
				log.error("Access denied for " + user.getUsername());
				throw new AccessDeniedException("password and username mismatch.");
			}
		}

		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mak.cis.mohr.api.security.service.AuthenticationService#
	 * isValidUserPassword (java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean isValidUserPassword(User user, String password) {

		if (user == null || StringUtils.isBlank(password) || StringUtils.isBlank(user.getSalt())) {
			// we don't accept empy passwords
			return false;
		}

		String hashedPassword = CustomSecurityUtil.encodeString(password + user.getSalt());
		if (hashedPassword.equals(user.getPassword())) {
			return true;
		} else {
			// try legacy method for backward compatibilty
			hashedPassword = CustomSecurityUtil.encodeString2(password + user.getSalt());
			if (hashedPassword.equals(user.getPassword())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @return the userDAO
	 */
	public UserDao getUserDAO() {
		return userDAO;
	}

	/**
	 * @param userDAO
	 *            the userDAO to set
	 */
	public void setUserDAO(UserDao userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public boolean isEnabledUser(User systemUser) {
		if (systemUser.getStatus().equals(UserStatus.DELETED)) {
			return false;
		}
		return true;
	}
}
