package org.sers.study.server.core.dao;

import org.sers.study.model.CapturedLocation;

/**
 * A Data access interface that provides methods for performing CRUD operations
 * on the {@link CapturedLocation} objects <br/>
 * <br/>
 * 
 * The CapturedLocationDAO interface abstracts the application from a particular
 * implementation of the CRUD operations on the entities
 * 
 * 
 * 
 */
public interface CapturedLocationDao extends BaseDao<CapturedLocation> {

}
