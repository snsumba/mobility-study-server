package org.sers.study.server.core.service;

import java.util.List;
import java.util.Map;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.DurationInfo;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.exception.ValidationFailedException;

@AuthorComment
public interface DurationInfoService extends GenericServiceAdapter {

	/**
	 * saves a given durationInfo
	 * 
	 * @param durationInfo
	 * @return
	 * @throws ValidationFailedException
	 *             thrown when the validation of the durationInfo fails.
	 */
	DurationInfo saveDurationInfo(DurationInfo durationInfo) throws ValidationFailedException;

	/**
	 * 
	 * @param durationInfo
	 * @return
	 * @throws ValidationFailedException
	 */
	void updateDurationInfo(DurationInfo durationInfo) throws ValidationFailedException;

	/**
	 * 
	 * This method is used to
	 * 
	 * @param offset
	 * @param limit
	 * @param filters
	 * @return
	 */
	List<DurationInfo> getDurationInfos(int offset, int limit, Map<String, Object> filters);

	/**
	 * 
	 * @param filters
	 * @return
	 */
	int countDurationInfos(Map<String, Object> filters);

	/**
	 * This method is used to
	 * 
	 * @param durationInfo
	 * @throws ValidationFailedException
	 */
	void deleteDurationInfo(DurationInfo durationInfo) throws ValidationFailedException;

	/**
	 * 
	 * This method is used to
	 * 
	 * @param recordStatus
	 * @return
	 */
	DurationInfo getDurationInfoByRecordstatus(RecordStatus recordStatus);

}
