package org.sers.study.server.core.security.service;

import org.sers.study.server.core.security.CustomUser;
import org.sers.study.model.security.User;


public interface CustomUserDetailsService extends org.springframework.security.core.userdetails.UserDetailsService {

	/**
	 * gets a UserDetails for a given user
	 * 
	 * @param user
	 * @return
	 */
	CustomUser getUserDetails(User user);
}
