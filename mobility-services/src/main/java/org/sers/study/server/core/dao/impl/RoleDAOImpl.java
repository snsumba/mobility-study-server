package org.sers.study.server.core.dao.impl;

import org.sers.study.server.core.dao.RoleDao;
import org.springframework.stereotype.Repository;
import org.sers.study.model.security.Role;

/**
 * Implements the {@link RoleDao} interface to provide CRUD operations for the
 * {@link Role} object using hibernate as the ORM strategy
 * 
 * @author Umar
 * 
 */
@Repository("roleDAO")
public class RoleDAOImpl extends BaseDAOImpl<Role>implements RoleDao {

}
