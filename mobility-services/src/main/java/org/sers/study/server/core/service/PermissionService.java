package org.sers.study.server.core.service;

import java.util.List;

import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Permission;

/**
 * 
 * @author Umar
 *
 */
public interface PermissionService {
	/**
	 * gets a list of all permissions
	 * 
	 * @return
	 */
	List<Permission> getPermissions();

	/**
	 * saves a given {@link Permission}
	 * 
	 * @param permission
	 * @throws ValidationFailedException
	 *             thrown when the validation of the user fails.
	 */
	Permission savePermision(Permission permission) throws ValidationFailedException;

	/**
	 * 
	 * @param permissionName
	 * @return
	 */
	Permission getPermissionByName(String permissionName);

	Object getObjectById(String id);
}
