package org.sers.study.server.core.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.CapturedLocation;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.User;
import org.sers.study.server.core.dao.CapturedLocationDao;
import org.sers.study.server.core.service.CapturedLocationService;
import org.sers.study.server.shared.SharedAppData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

@AuthorComment
@Service("capturedLocationService")
@Transactional
public class CapturedLocationServiceImpl implements CapturedLocationService {

	static final int MAX_LOCATIONS = 200;

	@Autowired
	private CapturedLocationDao capturedLocationDao;

	@Override
	public Object getObjectById(String id) {
		return capturedLocationDao.searchByPropertyEqual("id", id);
	}

	/*
	 * public boolean canSaveLocation() { int count =
	 * ApplicationContextProvider.getBean(CountryService.class)
	 * .getCountryByName(CountryConstants.TOGOLOESE_REPUBLIC).getPostalCode();
	 * return countCapturedLocations() <= (count != 0 ? count : MAX_LOCATIONS);
	 * }
	 */

	public int countCapturedLocations() {
		return capturedLocationDao.count(new Search());
	}

	@Override
	public CapturedLocation saveCapturedLocation(CapturedLocation capturedLocation) throws ValidationFailedException {
		this.validateObject(capturedLocation);
		this.objectAlreadyExists(capturedLocation);
		return capturedLocationDao.directMerge(capturedLocation);
	}

	@Override
	public void updateCapturedLocation(CapturedLocation capturedLocation) throws ValidationFailedException {
		validateObject(capturedLocation);
		capturedLocationDao.directUpdate(capturedLocation);
	}

	private void validateObject(CapturedLocation capturedLocation) throws ValidationFailedException {
		/*
		 * if (StringUtils.isBlank(capturedLocation.getId())) { if
		 * (!canSaveLocation()) throw new ValidationFailedException(
		 * "Error 879 Occured. Unfortunately you cant continue."); }
		 */
		if (StringUtils.isBlank(capturedLocation.getLatitude())
				&& StringUtils.isEmpty(capturedLocation.getLatitude())) {
			throw new ValidationFailedException("Latitude not set.");
		}
		if (StringUtils.isBlank(capturedLocation.getLongitude())
				&& StringUtils.isEmpty(capturedLocation.getLongitude())) {
			throw new ValidationFailedException("Longitude not set.");
		}
		if (StringUtils.isBlank(capturedLocation.getPlaceName())
				&& StringUtils.isEmpty(capturedLocation.getPlaceName())) {
			throw new ValidationFailedException("Place name not set.");
		}
		if (capturedLocation.getUser() == null) {
			throw new ValidationFailedException("User not set.");
		}

	}

	private void objectAlreadyExists(CapturedLocation capturedLocation) throws ValidationFailedException {
		/*
		 * Search search = new Search(); search.addFilterEqual("user",
		 * capturedLocation.getUser()); search.addFilterEqual("latitude",
		 * capturedLocation.getLatitude()); search.addFilterEqual("longitude",
		 * capturedLocation.getLongitude()); search.addFilterEqual("placeName",
		 * capturedLocation.getPlaceName());
		 * 
		 * if (capturedLocationDao.count(search) > 0) throw new
		 * ValidationFailedException(capturedLocation.getUser() + " : " +
		 * capturedLocation.getRecordStatus().getName() + " Already exists");
		 */
	}

	@Override
	public List<CapturedLocation> getCapturedLocations(User user, Date startDate, Date endDate) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		if (startDate != null)
			search.addFilterGreaterOrEqual("dateCreated", DateUtils.getMinimumDate(startDate));
		if (endDate != null)
			search.addFilterLessOrEqual("dateCreated", DateUtils.getMaximumDate(endDate));
		return capturedLocationDao.search(search);
	}

	@Override
	public List<CapturedLocation> getCapturedLocations(User user) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		return capturedLocationDao.search(search);
	}

	@Override
	public List<CapturedLocation> getCapturedLocations(Date startDate, Date endDate) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (startDate != null)
			search.addFilterGreaterOrEqual("dateCreated", DateUtils.getMinimumDate(startDate));
		if (endDate != null)
			search.addFilterLessOrEqual("dateCreated", DateUtils.getMaximumDate(endDate));
		return capturedLocationDao.search(search);
	}

	@Override
	public List<CapturedLocation> getCapturedLocations() {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		return capturedLocationDao.search(search);
	}

	private void setFilters(Search search, String key, Map<String, Object> filters) {
		search.addFilterOr(Filter.ilike("user.emailAddress", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("latitude", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("appRegistrationDate", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("accuracy", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.location", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.userId", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("longitude", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("placeName", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.phoneNumber", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.firstName", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.lastName", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.username", "%" + filters.get(key).toString() + "%"));
	}

	private void setSearchFilters(Search search, String value) {
		search.addFilterOr(Filter.ilike("user.emailAddress", "%" + value + "%"),
				Filter.ilike("latitude", "%" + value + "%"), Filter.ilike("longitude", "%" + value + "%"),
				Filter.ilike("placeName", "%" + value + "%"), Filter.ilike("user.phoneNumber", "%" + value + "%"),
				Filter.ilike("user.username", "%" + value + "%"), Filter.ilike("user.location", "%" + value + "%"),
				Filter.ilike("user.userId", "%" + value + "%"), Filter.ilike("user.firstName", "%" + value + "%"),
				Filter.ilike("user.lastName", "%" + value + "%"), Filter.ilike("accuracy", "%" + value + "%"),
				Filter.ilike("appRegistrationDate", "%" + value + "%"));
	}

	@Override
	public List<CapturedLocation> getCapturedLocations(User user, int offset, int limit, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		return capturedLocationDao.search(search);
	}

	@Override
	public int countCapturedLocations(User user, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		return capturedLocationDao.count(search);
	}

	@Override
	public List<CapturedLocation> getCapturedLocations(int offset, int limit, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		return capturedLocationDao.search(search);
	}

	@Override
	public int countCapturedLocations(Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		return capturedLocationDao.count(search);
	}

	@Override
	public CapturedLocation findCapturedLocationByUser(User user) {
		Search search = new Search();
		search.addFilterEqual("user", user);
		return capturedLocationDao.searchUnique(search);
	}

	@Override
	public CapturedLocation findCapturedLocationByRecordId(String recordId) {
		Search search = new Search();
		search.addFilterEqual("recordId", recordId);
		search.setFirstResult(0);
		search.setMaxResults(1);
		return capturedLocationDao.searchUnique(search);
	}

	@Override
	public List<CapturedLocation> getAllCapturedLocations(User user, String value) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		if (value != null) {
			if (value.trim().length() > 0) {
				this.setSearchFilters(search, value);
			}
		}
		return capturedLocationDao.search(search);
	}

	@Override
	public List<CapturedLocation> getCapturedLocationsByPlaceName(User user, String placeName) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		search.addFilterEqual("placeName", placeName);
		return capturedLocationDao.search(search);
	}

	@Override
	public List<CapturedLocation> getCapturedLocationsByPlaceName(String placeName) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("placeName", placeName);
		return capturedLocationDao.search(search);
	}

	@Override
	public List<CapturedLocation> getAllCapturedLocations() {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		return capturedLocationDao.search(search);
	}

	@Override
	public List<CapturedLocation> getAllCapturedLocations(String value) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (value != null) {
			if (value.trim().length() > 0) {
				this.setSearchFilters(search, value);
			}
		}
		return capturedLocationDao.search(search);
	}

	@Override
	public void deleteCapturedLocation(CapturedLocation capturedLocation) throws ValidationFailedException {
		capturedLocationDao.remove(capturedLocation);
		capturedLocationDao.removeById(capturedLocation.getId());
		System.out.println("Delete called ..." + capturedLocation.getId());
	}
}