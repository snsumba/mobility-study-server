package org.sers.study.server.core.service.impl;

/**
 * This class holds constants used in the Mohr system.
 * 
 * @author Umar
 * 
 */
public final class SystemConstants {

	/**
	 * default constructor. does not allow instantiation
	 */
	private SystemConstants() {
	}

	/**
	 * OS key
	 */
	public static final String OPERATING_SYSTEM_KEY = "os.name";
	/**
	 * Name of the OS running the system
	 */
	public static final String OPERATING_SYSTEM = System
			.getProperty(OPERATING_SYSTEM_KEY);
	/**
	 * OS name Windows XP
	 */
	public static final String OPERATING_SYSTEM_WINDOWS_XP = "Windows XP";
	/**
	 * OS name Windows Vista
	 */
	public static final String OPERATING_SYSTEM_WINDOWS_VISTA = "Windows Vista";
	/**
	 * os name Linux
	 */
	public static final String OPERATING_SYSTEM_LINUX = "Linux";
	/**
	 * os name FreeBSD
	 */
	public static final String OPERATING_SYSTEM_FREEBSD = "FreeBSD";
	/**
	 * os name Mac OS X
	 */
	public static final String OPERATING_SYSTEM_OSX = "Mac OS X";

	/**
	 * the maximum number of records to return when queried through pages.
	 */
	public static final int MAX_NUM_PAGE_RECORD = 25;

	/**
	 * the property key used to access the name path of the user's home
	 * directory currently running this application
	 */
	public static final String USER_HOME_DIRECTORY_KEY = "user.home";

	/**
	 * the default email address from which all emails in the system will be
	 * coming from unless one is provided for in the application settings.
	 * 
	 * @see #GLOBAL_PROPERTY_APPLICATION_EMAIL_ADDRESS
	 */
	@Deprecated()
	public static String DEFAULT_FROM_EMAIL_ADDRESS = "vinepharmacy@dcareug.com";

	/**
	 * defines the application key for the requisition type resolver jar file.
	 */
	public static final String APP_SETTING_KEY_REQUISITION_TYPE_RESOLVER_JAR = "REQUISITION_TYPE_RESOLVER_JAR";

	public static final String APP_SETTING_KEY_TAX_RATE = "TAX_RATE";

	public static final String GLOBAL_PROPERTY_APPLICATION_URL = "application.url";

	public static final String GLOBAL_PROPERTY_APPLICATION_URL_DEFAULT_VALUE = "http://192.168.1.1:8080/pettycash";

	/**
	 * Defines the application property name to access the application email
	 * address. <br/>
	 * To get the value for this property name, use
	 * {@link ApplicationContextProvider#getProperty(propertyName)}
	 */
	public static final String GLOBAL_PROPERTY_APPLICATION_EMAIL_ADDRESS = "application.emailAddress";

	public static final int GLOBAL_PROPERY_MINIMUM_PASSWORD_LENGTH = 8;
}
