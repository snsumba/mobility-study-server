package org.sers.study.server.core.security.service.impl;

import org.sers.study.server.core.dao.UserDao;
import org.sers.study.server.core.security.util.CustomSecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.sers.study.model.exception.SessionExpiredException;
import org.sers.study.model.security.User;

/**
 * Responsible to provide session details for the currently Logged in User to
 * all sections of the application
 
 */
@Service
@Scope("session")
public class CustomSessionProvider {

	private User loggedInUser;

	@Autowired
	UserDao userDao;

	/**
	 * @return the loggedInUser
	 */
	public final User getLoggedInUser() {
		if (loggedInUser == null)
			try {
				User loggedInUser = CustomSecurityUtil.getLoggedInUser();
				// loggedInUser.setDateLastLoggedIn(new Date());
				this.loggedInUser = userDao.directSave(loggedInUser);
			} catch (SessionExpiredException e) {
				// e.printStackTrace();
			}
		return loggedInUser;
	}

	public void reloadUserCredentials() {
		this.loggedInUser = userDao.searchUniqueByPropertyEqual("id", loggedInUser.getId());
	}

	/**
	 * @param loggedInUser
	 *            the loggedInUser to set
	 */
	public final void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
}
