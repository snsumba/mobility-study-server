package org.sers.study.server.core.service.setup;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.sers.study.model.constants.OperationStatus;
import org.sers.study.model.constants.RecordStatus;
import org.sers.study.model.security.Country;
import org.sers.study.model.security.CountryAnnotation;
import org.sers.study.model.security.CountryConstants;

public class CountrySetup {
	private SessionFactory sessionFactory;

	public CountrySetup(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
		initializeCountries();
	}

	private void initializeCountries() {
		List<Country> defaultCountries = parse(CountryConstants.class);

		for (Country country : defaultCountries) {
			country.setRecordStatus(RecordStatus.ACTIVE);
			country.setDateCreated(new Date());
			country.setDateChanged(new Date());
			sessionFactory.getCurrentSession().beginTransaction();
			country = (Country) sessionFactory.getCurrentSession().merge(country);
			sessionFactory.getCurrentSession().getTransaction().commit();
		}
	}

	/**
	 * Use reflection to get all fields annotated with {@CountryAnnotation } and
	 * returns then as an array.
	 * 
	 * @param annotatedClass
	 * @return
	 */
	private List<Country> parse(Class<?> annotatedClass) {

		List<Country> countries = new ArrayList<Country>();

		Field[] fields = annotatedClass.getFields();

		for (Field field : fields) {

			if (field.isAnnotationPresent(CountryAnnotation.class)) {
				CountryAnnotation countryAnnotation = field.getAnnotation(CountryAnnotation.class);

				Country country = new Country();
				country.setName(countryAnnotation.name());
				country.setPostalCode(countryAnnotation.postalCode());
				country.setActivateStatus(OperationStatus.FALSE.getName());
				country.setDeactivateStatus(OperationStatus.TRUE.getName());
				country.setEditStatus(OperationStatus.TRUE.getName());
				country.setViewStatus(OperationStatus.FALSE.getName());
				countries.add(country);
			}
		}
		return countries;
	}

}
