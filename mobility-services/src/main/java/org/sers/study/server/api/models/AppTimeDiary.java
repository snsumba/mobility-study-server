
/**
 *Jul 2, 2017
 *org.sers.study.server.api.models
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.server.api.models;

import java.io.Serializable;

public class AppTimeDiary implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
	private String date;
	private String recordId;
	private String timeCategorySuggestionId;
	private String startTime;
	private String endTime;

	/**
	 * 
	 */
	public AppTimeDiary() {
		super();
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the recordId
	 */
	public String getRecordId() {
		return recordId;
	}

	/**
	 * @return the timeCategorySuggestionId
	 */
	public String getTimeCategorySuggestionId() {
		return timeCategorySuggestionId;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @param recordId
	 *            the recordId to set
	 */
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	/**
	 * @param timeCategorySuggestionId
	 *            the timeCategorySuggestionId to set
	 */
	public void setTimeCategorySuggestionId(String timeCategorySuggestionId) {
		this.timeCategorySuggestionId = timeCategorySuggestionId;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}
