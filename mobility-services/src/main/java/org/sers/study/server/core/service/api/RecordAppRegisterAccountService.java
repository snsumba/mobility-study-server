
/**
 *Jun 29, 2017
 *org.sers.study.server.api.services
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.server.core.service.api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.sers.study.model.security.User;
import org.sers.study.server.api.models.ServerRegisteredAccount;
import org.sers.study.server.api.utils.ApiUtil;
import org.sers.study.server.api.utils.AppRecordStatus;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@Path("/AppRegisterAccountService")
public class RecordAppRegisterAccountService {

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response apiResponse(@Context HttpServletRequest request, @Context HttpHeaders headers,
			ServerRegisteredAccount serverRegisteredAccount) throws JSONException {
		System.out.println("AppRegisterAccountService .... ");
		JSONObject jsonObject = new JSONObject();
		try {
			UserService userService = ApplicationContextProvider.getBean(UserService.class);
			User user = userService.findUserByUserId(ApiUtil.decryptData(serverRegisteredAccount.getUserId()));
			if (user == null) {
				User newUser = new User();
				newUser.setFirstName(ApiUtil.decryptData(serverRegisteredAccount.getFirstName()));
				newUser.setLastName(ApiUtil.decryptData(serverRegisteredAccount.getLastName()));
				newUser.setLocation(ApiUtil.decryptData(serverRegisteredAccount.getLocation()));
				newUser.setGender(ApiUtil.getUserGender(ApiUtil.decryptData(serverRegisteredAccount.getGender())));
				newUser.setPhoneNumber(ApiUtil.decryptData(serverRegisteredAccount.getPhoneNumber()));
				newUser.setUsername(ApiUtil.decryptData(serverRegisteredAccount.getUsername()));
				newUser.setUserId(ApiUtil.decryptData(serverRegisteredAccount.getUserId()));
				newUser.setClearTextPassword(ApiUtil.decryptData(serverRegisteredAccount.getPassword()));

				User savedUser = userService.saveUser(newUser);
				if (savedUser != null) {
					jsonObject.put("userId", ApiUtil.encryptData(savedUser.getId()));
					jsonObject.put("username", ApiUtil.encryptData(savedUser.getUsername()));
					jsonObject.put("emailAddress", ApiUtil.encryptData(savedUser.getEmailAddress()));
					jsonObject.put("firstName", ApiUtil.encryptData(savedUser.getFirstName()));
					jsonObject.put("lastName", ApiUtil.encryptData(savedUser.getLastName()));
					jsonObject.put("location", ApiUtil.encryptData(savedUser.getLocation()));
					jsonObject.put("gender", ApiUtil.encryptData(savedUser.getGender().getName()));
					jsonObject.put("dateOfBirth", ApiUtil.encryptData(String.valueOf(savedUser.getDateOfBirth())));
					jsonObject.put("phoneNumber", ApiUtil.encryptData(savedUser.getPhoneNumber()));
					jsonObject.put("recordId", ApiUtil.encryptData(savedUser.getId()));
					jsonObject.put("status", ApiUtil.encryptData(AppRecordStatus.SYNCED.getStatus()));
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));
				} else {
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
					jsonObject.put("responseMessage",
							ApiUtil.encryptData("Failed to save savedUser, please try again"));
				}
			} else {
				jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
				jsonObject.put("responseMessage", ApiUtil
						.encryptData("User with Id " + serverRegisteredAccount.getUserId() + " is already registered"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.FAILURE_TOKEN));
			jsonObject.put("responseMessage", ApiUtil.encryptData(e.getMessage()));
		}

		System.out.println("jsonObject : " + jsonObject);
		return Response.status(200).entity("" + jsonObject).build();
	}

}
