package org.sers.study.server.core.service;

import java.util.List;
import java.util.Map;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.TimeCategorySuggestion;
import org.sers.study.model.exception.ValidationFailedException;

@AuthorComment
public interface TimeCategorySuggestionService extends GenericServiceAdapter {

	/**
	 * saves a given timeCategorySuggestion
	 * 
	 * @param timeCategorySuggestion
	 * @return
	 * @throws ValidationFailedException
	 *             thrown when the validation of the timeCategorySuggestion
	 *             fails.
	 */
	TimeCategorySuggestion saveTimeCategorySuggestion(TimeCategorySuggestion timeCategorySuggestion)
			throws ValidationFailedException;

	/**
	 * 
	 * @param timeCategorySuggestion
	 * @return
	 * @throws ValidationFailedException
	 */
	void updateTimeCategorySuggestion(TimeCategorySuggestion timeCategorySuggestion) throws ValidationFailedException;

	/**
	 * gets a list of all timeDiaryCategories in the system
	 * 
	 * @return
	 */
	List<TimeCategorySuggestion> getTimeCategorySuggestions();

	/**
	 * 
	 * @param offset
	 * @param limit
	 * @param filters
	 * @return
	 */
	List<TimeCategorySuggestion> getTimeCategorySuggestions(int offset, int limit, Map<String, Object> filters);

	/**
	 * 
	 * @param filters
	 * @return
	 */
	int countTimeCategorySuggestions(Map<String, Object> filters);

	/**
	 * This method is used to
	 * 
	 * @param id
	 * @return
	 */
	TimeCategorySuggestion getTimeCategorySuggestionById(String id);

}
