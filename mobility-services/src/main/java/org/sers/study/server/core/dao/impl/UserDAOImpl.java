package org.sers.study.server.core.dao.impl;

import org.sers.study.server.core.dao.UserDao;
import org.springframework.stereotype.Repository;
import org.sers.study.model.security.User;

/**
 * Implements the {@link UserDao} interface to provide CRUD operations for the
 * {@link UserAccount} object using hibernate as the ORM strategy
 * 
 * @author kdeo
 * 
 */
@Repository("userDAO")
public class UserDAOImpl extends BaseDAOImpl<User> implements UserDao {

}
