package org.sers.study.server.core.dao;

import org.sers.study.model.DurationInfo;

/**
 * A Data access interface that provides methods for performing CRUD operations
 * on the {@link DurationInfo} objects <br/>
 * <br/>
 * 
 * The DurationInfoDAO interface abstracts the application from a particular
 * implementation of the CRUD operations on the entities
 * 
 * 
 * 
 */
public interface DurationInfoDao extends BaseDao<DurationInfo> {

}
