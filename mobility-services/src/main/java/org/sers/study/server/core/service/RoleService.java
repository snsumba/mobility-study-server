package org.sers.study.server.core.service;

import java.util.List;

import org.sers.study.model.exception.OperationFailedException;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.Role;


public interface RoleService {

	/**
	 * saves a given role
	 * 
	 * @param role
	 * @return
	 * @throws ValidationFailedException
	 *             thrown when the validation of the user fails.
	 */
	Role saveRole(Role role) throws ValidationFailedException;

	/**
	 * gets a list of all
	 * 
	 * @return
	 */
	List<Role> getRoles();

	/**
	 * deletes a given role from the system
	 * 
	 * @param role
	 * @throws OperationFailedException
	 *             thrown when the delete operation fails.
	 */
	void deleteRole(Role role) throws OperationFailedException;

	/**
	 * 
	 * @param query
	 * @return
	 */
	List<Role> getRoles(String query);

	/**
	 * 
	 * @param searchString
	 * @return
	 */
	List<Role> findRolesByName(String searchString);

	/**
	 * 
	 * @param id
	 * @return
	 */
	Object getObjectById(String id);

	/**
	 * This method is used to 
	 * @param searchString
	 * @return
	 */
	Role getRoleByName(String searchString);
}
