package org.sers.study.server.core.service;

public interface GenericServiceAdapter {

	/**
	 * returns a specific object that extends base entity by its id
	 * 
	 * @param id
	 * @return
	 */
	Object getObjectById(String id);

}
