package org.sers.study.server.core.dao.impl;

import org.sers.study.server.core.dao.PermissionDao;
import org.springframework.stereotype.Repository;
import org.sers.study.model.security.Permission;

/**
 * Implements the {@link PermissionDao} interface to provide CRUD operations for
 * the {@link Permission} object using hibernate as the ORM strategy
 * 
 * @author Umar
 * 
 */
@Repository("permissionDAO")
public class PermissionDAOImpl extends BaseDAOImpl<Permission>implements PermissionDao {

}
