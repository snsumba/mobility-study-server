
/**
 *Jun 29, 2017
 *org.sers.study.server.api.services
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.sers.study.server.core.service.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sers.study.model.TimeDiary;
import org.sers.study.model.security.User;
import org.sers.study.server.api.models.AppAccessDetails;
import org.sers.study.server.api.utils.ApiUtil;
import org.sers.study.server.api.utils.AppRecordStatus;
import org.sers.study.server.core.service.TimeDiaryService;
import org.sers.study.server.core.service.UserService;
import org.sers.study.server.core.utils.ApplicationContextProvider;

@Path("/LoadAppTimeDiariesService")
public class LoadAppTimeDiariesService {

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response apiResponse(@Context HttpServletRequest request, @Context HttpHeaders headers,
			AppAccessDetails appAccessDetails) throws JSONException {

		System.out.println("LoadAppTimeDiariesService .... ");
		UserService userService = ApplicationContextProvider.getBean(UserService.class);
		JSONArray jsonArray = new JSONArray();
		String userId = ApiUtil.decryptData(appAccessDetails.getUserId());
		User user = userService.findUserByUserId(userId);
		if (user != null) {
			List<TimeDiary> timeDiarys = ApplicationContextProvider.getBean(TimeDiaryService.class)
					.getTimeDiaries(user);
			if (timeDiarys != null) {
				for (TimeDiary timeDiary : timeDiarys) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("status", ApiUtil.encryptData(AppRecordStatus.SYNCED.getStatus()));
					jsonObject.put("userId", ApiUtil.encryptData(userId));
					jsonObject.put("recordId", ApiUtil.encryptData(timeDiary.getId()));
					jsonObject.put("date", ApiUtil.encryptData(String.valueOf(timeDiary.getRegistrationDate())));
					jsonObject.put("startTime", ApiUtil.encryptData(String.valueOf(timeDiary.getStartTime())));
					jsonObject.put("endTime", ApiUtil.encryptData(String.valueOf(timeDiary.getEndTime())));
					jsonObject.put("description", ApiUtil.encryptData((timeDiary.getCategorySuggestion() != null
							? timeDiary.getCategorySuggestion().getSuggestion() : "n.a")));
					jsonObject.put("responseStatus", ApiUtil.encryptData(ApiUtil.SUCCESS_TOKEN));
					jsonObject.put("responseMessage", ApiUtil.encryptData(ApiUtil.SUCCESSFUL_OPERATION));
					jsonArray.put(jsonObject);
				}
			}
		}

		System.out.println("jsonArray : " + jsonArray);
		return Response.status(200).entity("" + jsonArray).build();
	}

}
