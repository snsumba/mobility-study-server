package org.sers.study.server.core.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.TimeCategorySuggestion;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.server.core.dao.TimeCategorySuggestionDao;
import org.sers.study.server.core.service.TimeCategorySuggestionService;
import org.sers.study.server.shared.SharedAppData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

@AuthorComment
@Service("timeCategorySuggestionService")
@Transactional
public class TimeCategorySuggestionServiceImpl implements TimeCategorySuggestionService {

	@Autowired
	private TimeCategorySuggestionDao timeCategorySuggestionDao;

	@Override
	public Object getObjectById(String id) {
		return timeCategorySuggestionDao.searchByPropertyEqual("id", id);
	}

	@Override
	public TimeCategorySuggestion saveTimeCategorySuggestion(TimeCategorySuggestion timeCategorySuggestion)
			throws ValidationFailedException {
		this.validateObject(timeCategorySuggestion);
		this.objectAlreadyExists(timeCategorySuggestion);
		return timeCategorySuggestionDao.directMerge(timeCategorySuggestion);
	}

	@Override
	public void updateTimeCategorySuggestion(TimeCategorySuggestion timeCategorySuggestion)
			throws ValidationFailedException {
		validateObject(timeCategorySuggestion);
		timeCategorySuggestionDao.directUpdate(timeCategorySuggestion);
	}

	private void validateObject(TimeCategorySuggestion timeCategorySuggestion) throws ValidationFailedException {
		if (StringUtils.isBlank(timeCategorySuggestion.getSuggestion())
				&& StringUtils.isEmpty(timeCategorySuggestion.getSuggestion())) {
			throw new ValidationFailedException("Suggestion not set.");
		}

	}

	private void objectAlreadyExists(TimeCategorySuggestion timeCategorySuggestion) throws ValidationFailedException {

		Search search = new Search();
		search.addFilterEqual("suggestion", timeCategorySuggestion.getSuggestion());
		if (timeCategorySuggestionDao.count(search) > 0)
			throw new ValidationFailedException(timeCategorySuggestion.getSuggestion() + " : "
					+ timeCategorySuggestion.getSuggestion() + " Already exists");
	}

	@Override
	public List<TimeCategorySuggestion> getTimeCategorySuggestions() {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		return timeCategorySuggestionDao.search(search);
	}

	private void setFilters(Search search, String key, Map<String, Object> filters) {
		search.addFilterOr(Filter.ilike("suggestion", "%" + filters.get(key).toString() + "%"));
	}

	@Override
	public List<TimeCategorySuggestion> getTimeCategorySuggestions(int offset, int limit, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		return timeCategorySuggestionDao.search(search);
	}

	@Override
	public int countTimeCategorySuggestions(Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		return timeCategorySuggestionDao.count(search);
	}

	@Override
	public TimeCategorySuggestion getTimeCategorySuggestionById(String id) {
		return timeCategorySuggestionDao.searchUniqueByPropertyEqual("id", id);
	}
}