package org.sers.study.server.core.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.sers.study.annotations.AuthorComment;
import org.sers.study.model.TimeDiary;
import org.sers.study.model.exception.ValidationFailedException;
import org.sers.study.model.security.User;
import org.sers.study.server.core.dao.TimeDiaryDao;
import org.sers.study.server.core.service.TimeDiaryService;
import org.sers.study.server.shared.SharedAppData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

@AuthorComment
@Service("timeDiaryService")
@Transactional
public class TimeDiaryServiceImpl implements TimeDiaryService {

	static final int MAX_LOCATIONS = 200;

	/*
	 * public boolean canSaveTimeDiary() { int count =
	 * ApplicationContextProvider.getBean(CountryService.class)
	 * .getCountryByName(CountryConstants.TOGOLOESE_REPUBLIC).getPostalCode();
	 * return countTimeDiaries() <= (count != 0 ? count : MAX_LOCATIONS); }
	 */

	public int countTimeDiaries() {
		return timeDiaryDao.count(new Search());
	}

	@Autowired
	private TimeDiaryDao timeDiaryDao;

	@Override
	public Object getObjectById(String id) {
		return timeDiaryDao.searchByPropertyEqual("id", id);
	}

	@Override
	public TimeDiary saveTimeDiary(TimeDiary timeDiary) throws ValidationFailedException {
		this.validateObject(timeDiary);
		return timeDiaryDao.directMerge(timeDiary);
	}

	@Override
	public void updateTimeDiary(TimeDiary timeDiary) throws ValidationFailedException {
		validateObject(timeDiary);
		timeDiaryDao.directUpdate(timeDiary);
	}

	private void validateObject(TimeDiary timeDiary) throws ValidationFailedException {
		/*
		 * if (StringUtils.isBlank(timeDiary.getId())) { if
		 * (!canSaveTimeDiary()) throw new ValidationFailedException(
		 * "Error 879 Occured. Unfortunately you cant continue."); }
		 */

		if (timeDiary.getUser() == null) {
			throw new ValidationFailedException("User not set.");
		}

	}

	@Override
	public List<TimeDiary> getTimeDiaries(User user, Date startDate, Date endDate) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		if (startDate != null)
			search.addFilterGreaterOrEqual("dateCreated", DateUtils.getMinimumDate(startDate));
		if (endDate != null)
			search.addFilterLessOrEqual("dateCreated", DateUtils.getMaximumDate(endDate));
		return timeDiaryDao.search(search);
	}

	@Override
	public List<TimeDiary> getTimeDiaries(Date startDate, Date endDate) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (startDate != null)
			search.addFilterGreaterOrEqual("dateCreated", DateUtils.getMinimumDate(startDate));
		if (endDate != null)
			search.addFilterLessOrEqual("dateCreated", DateUtils.getMaximumDate(endDate));
		return timeDiaryDao.search(search);
	}
	
	@Override
	public List<TimeDiary> getTimeDiaries(User user) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		return timeDiaryDao.search(search);
	}

	@Override
	public List<TimeDiary> getTimeDiaries() {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		return timeDiaryDao.search(search);
	}

	private void setFilters(Search search, String key, Map<String, Object> filters) {
		search.addFilterOr(Filter.ilike("user.emailAddress", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("registrationDate", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("startTime", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("endTime", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("categorySuggestion.suggestion", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.phoneNumber", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.username", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.location", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.firstName", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.lastName", "%" + filters.get(key).toString() + "%"),
				Filter.ilike("user.userId", "%" + filters.get(key).toString() + "%"));
	}

	@Override
	public List<TimeDiary> getTimeDiaries(User user, int offset, int limit, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		return timeDiaryDao.search(search);
	}

	@Override
	public int countTimeDiaries(User user, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		search.addFilterEqual("user", user);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		return timeDiaryDao.count(search);
	}

	@Override
	public List<TimeDiary> getTimeDiaries(int offset, int limit, Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		search.setFirstResult(offset);
		search.setMaxResults(limit);
		return timeDiaryDao.search(search);
	}

	@Override
	public int countTimeDiaries(Map<String, Object> filters) {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		if (filters != null)
			for (String key : filters.keySet()) {
				if (key.equals(SharedAppData.GLOBAL_FILTER_ID)) {
					this.setFilters(search, key, filters);
				}
			}
		return timeDiaryDao.count(search);
	}

	@Override
	public TimeDiary findTimeDiaryByUser(User user) {
		Search search = new Search();
		search.addFilterEqual("user", user);
		return timeDiaryDao.searchUnique(search);
	}

	@Override
	public List<TimeDiary> getAllTimeDiaries() {
		Search search = new Search();
		search.addSort("dateChanged", true, true);
		return timeDiaryDao.search(search);
	}

	@Override
	public void deleteTimeDiary(TimeDiary timeDiary) throws ValidationFailedException {
		timeDiaryDao.remove(timeDiary);
		timeDiaryDao.removeById(timeDiary.getId());
		System.out.println("Delete called ..." + timeDiary.getId());
	}
}